<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Country;
use App\City;

class CountriesCitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::getQuery()->delete();
        Country::getQuery()->delete();

        $faker = Factory::create('ru_RU');

        for ($i = 1; $i <= 10; $i++) {
            $country = new Country;
            $country->title = $faker->country;
            $country->save();
            for ($k = 1; $k < 30; $k++) {
                $city = new City;
                $city->title = $faker->city;
                $country->cities()->save($city);
            }
        }
    }
}
