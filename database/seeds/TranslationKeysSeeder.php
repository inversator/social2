<?php

use Illuminate\Database\Seeder;
use App\TranslationKey;
use App\TranslationKeyValue;
use App\Language;

// This seeder will be used multiple times
// So it can be called multiple times
// With this in mind we create all keys only once
// And then just update them
class TranslationKeysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createLanguage('ru');
        $this->createLanguage('en');

        $this->createKey('LOGIN', [
            'ru' => 'Вход',
            'en' => 'Login'
        ]);

        $this->createKey('LOGOUT', [
            'ru' => 'Выход',
            'en' => 'Logout'
        ]);

        $this->createKey('REGISTRATION', [
            'ru' => 'Регистрация',
            'en' => 'Registration'
        ]);

        $this->createKey('REMEMBER_ME', [
            'ru' => 'Запомнить меня',
            'en' => 'Remember me'
        ]);

        $this->createKey('FACEBOOK_LOGIN', [
            'ru' => 'Вход через facebook',
            'en' => 'Login through facebokk'
        ]);

        $this->createKey('CREATE', [
            'ru' => 'Создать',
            'en' => 'Create'
        ]);

        $this->createKey('ADD', [
            'ru' => 'Добавить',
            'en' => 'Add'
        ]);

        $this->createKey('SAVE', [
            'ru' => 'Сохранить',
            'en' => 'Save'
        ]);

        $this->createKey('PROPERTIES', [
            'ru' => 'Свойства',
            'en' => 'Properties'
        ]);

        $this->createKey('PROPERTY_NAME', [
            'ru' => 'Свойство',
            'en' => 'Property'
        ]);

        $this->createKey('PROPERTY_VALUE', [
            'ru' => 'Значение свойства',
            'en' => 'Property value'
        ]);

        $this->createKey('ADD_PROPERTY_VALUE', [
            'ru' => 'Добавить значение свойства',
            'en' => 'Add property value'
        ]);

        $this->createKey('LANGUAGE', [
            'ru' => 'Язык',
            'en' => 'Language'
        ]);
    }

    public function createLanguage(string $code)
    {
        return Language::firstOrCreate(['code' => $code]);
    }

    public function createKey(string $key, array $translations)
    {
        $translationKey = TranslationKey::firstOrCreate(['key' => $key]);

        $languages = Language::all();

        foreach ($translations as $langCode => $value) {
            $lang = $languages->where('code', $langCode)->first();
            if (!$lang) {
                throw new \InvalidArgumentException(sprintf('Language code %s is not valid', $langCode));
            }

            $translationKeyValue = $translationKey->translations->where('language_id', $lang->id)->first();
            if (!$translationKeyValue) {
                $translationKeyValue = new TranslationKeyValue;
                $translationKeyValue->language()->associate($lang);
                $translationKeyValue->translationKey()->associate($translationKey);
            }
            $translationKeyValue->translation = $value;
            $translationKeyValue->save();
        }
    }
}
