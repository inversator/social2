<?php

use Illuminate\Database\Seeder;
use App\Language;
use App\Property;
use App\PropertyTranslation;
use App\PropertyValue;
use App\PropertyValueTranslation;

class PropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langRu = Language::firstOrCreate(['code' => 'ru']);
        $langEn = Language::firstOrCreate(['code' => 'en']);

        PropertyTranslation::getQuery()->delete();
        PropertyValueTranslation::getQuery()->delete();
        PropertyValue::getQuery()->delete();
        Property::getQuery()->delete();

        $properties = [
            [
                'ru' => [
                    'name' => 'внешность',
                    'items' => ['обычная', 'необычная',]
                ],
                'en' => [
                    'name' => 'appearance',
                    'items' => ['usual', 'unusual'],
                ],
            ],
            [
                'ru' => [
                    'name' => 'телосложение',
                    'items' => ['худое', 'спортивное', 'полное'],
                ],
                'en' => [
                    'name' => 'body type',
                    'items' => ['thin', 'sport', 'puffy'],
                ],
            ],
        ];

        foreach ($properties as $prop) {
            $property = Property::create(['active' => true]);

            foreach ($prop as $langCode => $data) {

                foreach ($data['items'] as $item) {
                    $propValue = new PropertyValue;
                    $property->values()->save($propValue);

                    $propValueTranslation = new PropertyValueTranslation;
                    $propValueTranslation->language()->associate(Language::where(['code' => $langCode])->first());
                    $propValueTranslation->translation = $item;
                    $propValue->translations()->save($propValueTranslation);
                }

                $propTranslation = new PropertyTranslation;
                $propTranslation->language()->associate(Language::where(['code' => $langCode])->first());
                $propTranslation->translation = $data['name'];
                $property->translations()->save($propTranslation);
            }
        }
    }
}
