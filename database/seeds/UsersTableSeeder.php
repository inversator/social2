<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Role;
use App\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::updateOrCreate(['email' => 'admin@zolushka.com'], [
            'password' => 'admin',
            'name' => 'admin',
        ]);
        $role = Role::updateOrCreate(['name' => 'admin']);
        $permission = Permission::updateOrCreate(['name' => 'admin']);
        $role->perms()->sync([$permission->id]);
        if (!$user->hasRole($role->name)) {
            $user->attachRole($role);
        }
    }
}
