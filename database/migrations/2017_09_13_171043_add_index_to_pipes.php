<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToPipes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_pipes', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('last_open');
            $table->index('pipe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_pipes', function (Blueprint $table) {
            $table->dropIndex('user_id');
            $table->dropIndex('last_open');
            $table->dropIndex('pipe');
        });
    }
}
