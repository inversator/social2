<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('code', 2)->unique();
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('active');
        });

        Schema::create('property_values', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('property_id')->unsigned();
            $table->foreign('property_id')->references('id')->on('properties');
        });

        Schema::create('property_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages');
            $table->string('translation');
            $table->integer('property_id')->unsigned();
            $table->foreign('property_id')->references('id')->on('properties');
        });

        Schema::create('property_value_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages');
            $table->string('translation');
            $table->integer('property_value_id')->unsigned();
            $table->foreign('property_value_id')->references('id')->on('property_values');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_value_translations');
        Schema::dropIfExists('property_translations');
        Schema::dropIfExists('property_values');
        Schema::dropIfExists('properties');
        Schema::dropIfExists('languages');
    }
}
