@extends('layouts.master')


@section('content')
    <main>
    <div class="el el-6">
        <div class="el__inner">
        </div>
    </div>
    <div class="el el-7">
        <div class="el__inner">
        </div>
    </div>
    <div class="el el-8">
        <div class="el__inner">
        </div>
    </div>
    <div class="p-profile page-main">
        <div class="container">
             <div class="b-profile">
        <div class="top">
            @if($photos->isNotEmpty())
            <div class="image" style="background-image: url({{url($user->getMainPhoto('original'))}})">
                {{--<img src="img/prof-1.jpg" alt="">--}}
            </div>
                {{--<div class="image" >--}}
                    {{--<img src="{{url($user->getMainPhoto('original'))}}" alt="">--}}
                {{--</div>--}}
            @else
                <div class="image image-default">
                    <span class="no-photo"><i class="fa fa-camera"></i></span>
                </div>
            @endif
            <div class="info">
                <div class="info__panel">
                    <div class="title">{{$user->name}} {{$user->getAge()}} @choice('год|года|лет', $user->getAge(), [], 'en'), {{$user->getZodiac()}}, {{$user->city->title}}</div>
                    {{--<div class="info__status st-offline">Не в сети</div>--}}
                </div>
                <div class="edit-block">
                    {{--<i class="fa fa-pencil show-edit-block btn-edit btn-edit-description"  aria-hidden="true" ></i>--}}
                    <p class="text-about edit-block-to-hide edit-description">{{  Auth::user()->description }}</p>

                    <div class="form edit-block-to-show edit-form-hidden">
                        <form id="form-description" method="POST" action="{{url('profile/edit')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="action" value="description">
                            <textarea class="textarea" name="description">{{ Auth::user()->description }}</textarea>
                            <button type="submit" class="btn btn-send">Сохранить</button>
                        </form>
                    </div>
                </div>
                <div style="width:100%"></div>

            </div>
        </div>
        <div class="gallery js-gallery">
                <div class="photo-list__item-inner">
                    <form id="profile-photo-upload" class="profile-photo-upload" method="POST" action="{{url('profile/edit')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="action" value="upload">
                        <input type="file" id="photo" name="photo" class="hidden">
                        <span class="form-row__text-error"></span>
                    </form>
                    <label for="photo" class="add-photo-block">
                        <i class="fa fa-camera camera-icon " aria-hidden="true"></i>
                        <span class="add-photo-block__text">Добавить фото</span>
                    </label>
                </div>
            <div id="sortable" class="sortable-photos">
            @forelse($photos as $photo)
            <a id="{{$photo->id}}" data-position="{{$photo->priority}}" class='profile-photo-thumb' href="{{url('user_photos/'.$user->id.'/original/'.$photo->name)}}" style="background-image: url({{url('user_photos/'.$user->id.'/medium/'.$photo->name)}})"></a>

            @empty
            @endforelse
            </div>
        </div>
        <div class="more-info">
            <div class="more-info__row">
                @php $i=0; @endphp
            @forelse($categories as $category)

                <div class="more-info__column edit-block">
                    <div class="column-title">
                        {{ Translation::translatePropertyCategory($category) }}
                        {{--<i class="fa fa-pencil show-edit-block btn-edit"  aria-hidden="true" ></i>--}}
                    </div>

                    <div class="properties-show edit-block-to-hide">
                        @php $view = $category->view === NULL ? 'table' : $category->view; @endphp

                        @include('partials.'.$view, ['properties' => $category->properties, 'lang'=>$lang])
                    </div>
                    <form  method="POST" action="{{url('profile/edit')}}" class="properties-edit edit-block-to-show edit-form-hidden col-md-10">
                        {{csrf_field()}}
                        <input type="hidden" name="action" value="properties">
                        @forelse ($category->properties as $property)
                            @if($property->field_type != null)
                                @php

                                @endphp
                            @include("partials.formelements.$property->field_type", [
                            'property' => $property,
                            'lang' => $lang,
                            'label' => Translation::translateProperty($property),
                            'name' => $property->id,
                            ])
                            @endif
                        @empty
                        @endforelse
                        @include('partials.formelements.submit',[
                        'title' => 'SAVE'
                        ])
                    </form>
                </div>
                @php
                    $i++;

                    if($i%3 == 0){
                        echo '</div><div class="more-info__row">';
                    }
                @endphp

            @empty
            @endforelse
            </div>


        </div>
    </div>
        </div>
    </div>
    </div>
    </main>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection

@section('footer.js')
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/gliss/gliss.profile.js')}}"></script>
    <script src="{{asset('js/gliss/gliss.photoprocess.js')}}"></script>
    <script>
        gliss.profile.init();
    </script>
@endsection