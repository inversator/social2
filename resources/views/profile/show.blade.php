@extends('layouts.master')


@section('content')
    <main>
    <div class="el el-6">
        <div class="el__inner">
        </div>
    </div>
    <div class="el el-7">
        <div class="el__inner">
        </div>
    </div>
    <div class="el el-8">
        <div class="el__inner">
        </div>
    </div>
    <div class="p-profile page-main">
        <div class="container">
             <div class="b-profile">
        <div class="top">
            @if($photos->isNotEmpty())
            <div class="image" style="background-image: url({{url($user->getMainPhoto('original'))}})"></div>
            @else
                <div class="image image-default">
                    <span class="no-photo"><i class="fa fa-camera"></i></span>
                </div>
            @endif
            <div class="info">
                <div class="info__panel">
                    <div class="title">{{$user->name}} {{$user->getAge()}} @choice('год|года|лет', $user->getAge(), [], 'en'), {{$user->getZodiac()}}, {{$user->city->name}}</div>
                    <div class="info__status st-offline">@if ($user->isOnline()) <span class="text-success">В сети</span> @else Не в сети @endif</div>
                </div>
                <div class="">

                    <p class="text-about ">{{  $user->description }}</p>

                </div>
                <a href="javascript:void(0)"  onClick="return gliss.profile.quickMessage({{$user->id}})" class="btn-send">Отправить сообщение<span class="fa fa-envelope-o"></span></a>

                <ul class="info__action-panel">
                    <li><a href="javascript:void(0)" onClick="return gliss.profile.addFavorite(this, {{$user->id}})" data-favorite="{{$status == 'favorite' ? 'favorite': 0}}"><i class="fa fa-star btn-favorite {{$status == 'favorite' ? 'favorite': ''}}" ></i>Добавить в избранные</a></li>
                    <li><a href="javascript:void(0)" onClick="return gliss.profile.addBlock(this, {{$user->id}})" data-block="{{$status == 'block' ? 'block': 0}}"><i class="fa fa-exclamation-circle btn-ignore {{$status == 'block' ? 'block': ''}}"></i>Добавить в игнор</a></li>
                    <li><a href="#"><i class="fa fa-gift"></i>Подарить ей комплимент</a></li>
                </ul>
            </div>
        </div>
        <div class="gallery js-gallery">

            <div id="" class="sortable-photos">
            @forelse($photos as $photo)
            <a class='profile-photo-thumb' href="{{url('user_photos/'.$user->id.'/original/'.$photo->name)}}" style="background-image: url({{url('user_photos/'.$user->id.'/medium/'.$photo->name)}})"></a>

            @empty
            @endforelse
            </div>
        </div>
        <div class="more-info">
            <div class="more-info__row">
                @php $i=0; @endphp
            @forelse($categories as $category)

                <div class="more-info__column">
                    <div class="column-title">
                        {{ Translation::translatePropertyCategory($category) }}
                    </div>

                    <div class="">
                        @php $view = $category->view === NULL ? 'table' : $category->view; @endphp
                        @include('partials.'.$view, ['properties' => $category->properties, 'lang'=>$lang])
                    </div>

                </div>
                @php
                    $i++;

                    if($i%3 == 0){
                        echo '</div><div class="more-info__row">';
                    }
                @endphp

            @empty
            @endforelse
            </div>

        </div>
    </div>
        </div>
    </div>
    </div>
    </main>
    <div id="m_modal_form">
        <span onClick="return gliss.profile.closeModal()" id="modal_close">X</span>
        <div class="quick-message">
            <div class="quick-message-header">
                <span>Новое сообщение</span>
            </div>
            <div class="quick-message-body">
                <div class="user-info">
                <p>{{$user->name}}, {{$user->getAge()}}</p>
                </div>
                <form onsubmit="return gliss.profile.sendQuickMessage(event, this, {{$user->id}})">
                    <input type="hidden" name="action" value="send-message">
                    <textarea name="text"></textarea>
                    <button type="submit" class=" btn-send">
                        <span class="btn__text">Написать</span>
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div id="m_overlay" onClick="return gliss.profile.closeModal()"></div>



@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection

@section('footer.js')
    <script src="{{asset('js/gliss/gliss.modal.js')}}"></script>
    <script src="{{asset('js/gliss/gliss.profile.js')}}"></script>
    <script src="{{asset('js/gliss/gliss.photoprocess.js')}}"></script>

    <script>
        gliss.profile.init();
    </script>
@endsection