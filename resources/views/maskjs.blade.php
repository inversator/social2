<script src="/assets/phonevalidator/js/intlTelInput.min.js"></script>
<script src="/assets/phonevalidator/js/utils.js"></script>
<script type="text/javascript">
function inputMaskMe(el, callback, clear, errorBlock){
  $(el).intlTelInput({
    // typing digits after a valid number will be added to the extension part of the number
    allowExtensions: false,
    // automatically format the number according to the selected country
    autoFormat: true,
    // add or remove input placeholder with an example number for the selected country
    autoPlaceholder: false,
    // if there is just a dial code in the input: remove it on blur, and re-add it on focus
    autoHideDialCode: true,
    // default country
    defaultCountry: "ru",
    // don't insert international dial codes
    nationalMode: false,
    // number type to use for placeholders
    numberType: "MOBILE",
    preferredCountries: [ "ru", "ua" ],
    utilsScript: '/assets/phonevalidator/js/utils.js'
  });
  $(el).bind('keyup change', function(){
    var intlNumber = $(this).intlTelInput("getNumber");
    if ( $(this).intlTelInput("isValidNumber") ) {
      $(errorBlock).css('display', 'none');
      callback(intlNumber);
    }
    else {
      clear();
      var validator = intlTelInputUtils.validationError;
      var errorType = $(this).intlTelInput("getValidationError");
      if ( validator.TOO_SHORT == errorType ) $(errorBlock).html('Слишком короткий номер.').fadeIn('fast');
      else if ( validator.INVALID_COUNTRY_CODE == errorType ) $(errorBlock).html('Ошибка формата данных.').fadeIn('fast');
      else if ( validator.TOO_LONG == errorType ) $(errorBlock).html('Номер слишком длинный.').fadeIn('fast');
      else if( validator.NOT_A_NUMBER == errorType ) $(errorBlock).fadeOut('fast');
    }
  });
}
</script>