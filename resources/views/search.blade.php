@extends('layouts.master')

@section('content')


    <div class="quick-choice__wrap">
        <div class="container">
            <div class="quick-choice">
                <div class="quick-choice__list">
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                    <a class="quick-choice__item"><img src="img/quick-choice-1.jpg" alt=""></a>
                </div>
            </div>
        </div>
    </div>


    <form method="GET" action="{{ route("search") }}" class="form-horizontal">

        <div class="row col-md-12">

            <input type="text" class="form-control" name="height_from" placeholder="Рост от" value="{{ Request::get("height_from") }}">
            <input type="text" class="form-control" name="height_to" placeholder="Рост до"   value="{{ Request::get("height_to") }}">
            <input type="text" class="form-control" name="weight_from" placeholder="Вес от"  value="{{ Request::get("weight_from") }}">
            <input type="text" class="form-control" name="weight_to" placeholder="Вес до"    value="{{ Request::get("weight_to") }}">

        </div>

        <div class="row" id="breast_size">
            <input type="text" class="form-control" name="breast_size_from" placeholder="Грудь от" value="{{ Request::get("breast_size_from") }}">
            <input type="text" class="form-control" name="breast_size_to" placeholder="Грудь до" value="{{ Request::get("breast_size_to") }}">
        </div>
        
        @php
            $disabled = "disabled";
            if (Auth::user()->hasPremiumAccount()) {
                $disabled = "";
            }
        @endphp

        @foreach ($properties as $property)
            @php
            $key = "property[$property->id][]";
            @endphp
            <div class="form-group col-md-3">
                <label for="{{ $key }}">{{ Translation::translateProperty($property) }}</label>
                <select class="form-control" name="{{ $key }}" {{ $disabled }} multiple>
                    <option value=""></option>
                    @php
                        $selected = isset($selectedProperties[$property->id]) ? $selectedProperties[$property->id] : [];
                    @endphp
                    @foreach ($property->values as $value)
                        @php
                            $selectedValue = false;
                            if (is_array($selected)) {
                                $selectedValue = in_array($value->id, $selected) ? true : false;
                            }
                        @endphp
                        <option value="{{ $value->id }}" {{ $selectedValue ? "selected" : "" }}>{{ Translation::translatePropertyValue($value) }}</option>
                    @endforeach
                </select>
            </div>
        @endforeach
        
        <button type="submit" class="btn btn-default">Поиск</button>
    </form>
    <div class="s-options form">
        <form ethod="GET" action="{{ route("search") }}">
            <div class="container">
                <h1>Найдите свою вторую половинку</h1>
                <div class="s-options__row">
                    @php
                        $gender = Request::get("gender");
                        if (!$gender) {
                            if (Auth::user()->isFemale()) {
                                $gender = "M";
                            } else {
                                $gender = "F";
                            }
                        }
                        $maleChecked = $gender == "M" ? "checked" : "";
                        $femaleChecked = $gender == "F" ? "checked" : "";
                    @endphp
                    <div class="form-group">
                        <label>Кого вы ищете</label>
                        <select class="select-custom" placeholder="Девушку">
                            <option value="F">Девушку</option>
                            <option value="M">Парня</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Введите город</label>
                        <input type="text" class="form-control" placeholder="Город">
                    </div>
                    <div class="form-group form-group__age">
                        <label>Выберите возраст</label>
                        <input class="age-slider" type="slider">
                    </div>
                    <div class="btn-wrap">
                        <button type="submit" class="btn-link btn-link__accent">Искать</button>
                    </div>
                </div>
            </div>
            <div class="s-options__extra">
                <div class="s-options__extra-top">
                    <div class="container">
                        <div class="title"><span><i class="fa"></i></span>Дополнительные параметры</div>
                    </div>
                </div>
                <div class="container">
                    <div class="s-options__extra-wrap">
                        <div class="s-options__row">
                            <div class="form-group">
                                <label>Some</label>
                                <select class="select-custom" placeholder="Девушку">
                                    <option value="0">Some 1</option>
                                    <option value="1">Some 2</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Some</label>
                                <input type="text" class="form-control" placeholder="Some">
                            </div>
                            <div class="form-group">
                                <label>Some</label>
                                <input type="text" class="form-control" placeholder="Some">
                            </div>
                            <div class="form-group">
                                <label>Some</label>
                                <select class="select-custom" placeholder="Девушку">
                                    <option value="0">Some 1</option>
                                    <option value="1">Some 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <main>
        <div class="el el-6">
            <div class="el__inner">
            </div>
        </div>
        <div class="el el-7">
            <div class="el__inner">
            </div>
        </div>
        <div class="el el-8">
            <div class="el__inner">
            </div>
        </div>
        <div class="p-search page-main">
            <div class="container">
                <div class="catalog catalog__search">
                    <div class="catalog__filter">
                        <div class="wrap">
                            <ul class="catalog__filter-list">
                                <li class="active">
                                    <a href="javascript:void(0)">Все</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Новые</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Онлайн</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><i class="fa fa-camera"></i>С подтвержденным фото</a>
                                </li>
                            </ul>
                            <a class="link-to fa fa-question-circle" href="#"></a>
                        </div>
                        <ul class="panel-views">
                            <li class="active view-link fa fa-th">
                            </li>
                            <li class="view-link fa fa-th-list">
                            </li>
                        </ul>
                    </div>
                    <div class="catalog__list list-viev">
                        @foreach($users as $user)
                            <div class="catalog__item">
                                <img src="{{$user->getMainPhoto('medium')}}" alt="">
                                <span class="top">ТОП</span>
                                <a href="#" class="catalog-notification catalog-notification__conf">
                                    <span class="image"></span>
                                    <span class="count">12</span>
                                </a>
                                <div class="info">
                                    <div class="title">
                                        <span class="name">{{$user->name}}</span>,
                                        <span class="age">{{$user->age}}</span>
                                    </div>
                                    <div class="from">{{$user->city->title}}, {{$user->country->title}}</div>
                                    <a href="#" class="btn-send">Написать <span class="fa fa-envelope-o"></span></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="pagination-wrap">
                    {{ $users->links() }}
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-prev" href="#" aria-label="Previous">

                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a class="page-link" href="#">2</a></li>
                        <li><a class="page-link" href="#">3</a></li>
                        <li><a class="page-link" href="#">4</a></li>
                        <li><a class="page-link" href="#">5</a></li>
                        <li><a class="page-link" href="#">...</a></li>
                        <li><a class="page-link" href="#">199</a></li>
                        <li>
                            <a class="page-next" href="#" aria-label="Next">

                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>





    </main>
@endsection

@section('footer.js')
    <script>
        (function() {
            var gender = $("input[name=gender]");
            var breast = $("#breast_size");
            // hide or show breast size param depending on M/F attribute
            gender.on("change", function() {
                var value = $("input[name=gender]:checked").val();
                if (value === 'M') {
                    breast.hide();
                } else {
                    breast.show();
                }
            });
            gender.trigger("change");
        })();
    </script>
@endsection
