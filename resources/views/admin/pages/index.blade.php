@extends('layouts.admin')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Страницы
                </h3>
                {!! Breadcrumbs::generate(['Админ панель', 'Страницы'], 'metronic') !!}
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Страницы
                            <small>
                                initialized from javascript array
                            </small>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <a href="{{ route("admin.pages.create") }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                <span>
                                    <i class="la la-cart-plus"></i>
                                    <span>
                                        Создать страницу
                                    </span>
                                </span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data">
                    <table class="m-datatable__table">
                        <thead class="m-datatable__head">
                            <tr class="m-datatable__row">
                                <th data-field="RecordID" class="m-datatable__cell--center m-datatable__cell m-datatable__cell--sort" style="width:100px;">
                                    <span>#ID</span>
                                </th>
                                <th data-field="OrderID" class="m-datatable__cell m-datatable__cell--sort">
                                    <span>URL</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="m-datatable__body" style="">
                            @php $i=0 @endphp
                            @forelse ($pages as $page)
                                <tr data-row="{{$i}}" class="m-datatable__row @if($i%2 == 0) m-datatable__row--even @endif">
                                    <td data-field="RecordID" class="m-datatable__cell m-datatable__cell--center" style="width:100px;">
                                        <span>{{ $page->id }}</span>
                                    </td>
                                    <td data-field="OrderID" class="m-datatable__cell">
                                        <a href="{{ route("admin.pages.show", ["id" => $page->id]) }}">
                                            {{ $page->slug }}
                                        </a>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
