@extends('layouts.admin')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Редактирование страницы
                </h3>
                {!! Breadcrumbs::render(['Админ панель', 'Страницы', 'Редактирование страницы'], 'metronic') !!}
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                <form action="{{ route("admin.pages.update", ["id" => $page->id]) }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <ul class="nav nav-tabs" role="tablist">
                        @php $i=0; @endphp
                        @foreach ($languages as $language)
                            <li class="nav-item">
                                <a class="nav-link @if($i == 0) active @endif" data-toggle="tab" href="#m_tabs_{{$i}}" aria-expanded="true">
                                    {{$language->code}}
                                </a>
                            </li>
                            @php $i++; @endphp
                        @endforeach
                    </ul>

                    <div class="tab-content">
                        @php $s=0; @endphp
                        @foreach ($languages as $language)
                            @if ($loc = $page->localizations->where('language_id', $language->id)->first())
                                    <div class="tab-pane active" id="m_tabs_{{$s}}">
                                        @include('partials.input', [
                                            'name' => 'locales[' . $language->id . '][title]',
                                            'label' => 'Заголовок',
                                            'value' => $loc->title,
                                        ])

                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <label for="">Краткое описание</label>
                                            <textarea class="summernote" name="locales[{{$language->id}}][description]">{{$loc->description}}</textarea>
                                        </div>
                                        <br><br>

                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <label for="">Полное описание</label>
                                            <textarea class="summernote" name="locales[{{$language->id}}][text]">{{$loc->text}}</textarea>
                                        </div>
                                        <br>


                                        @include('partials.input', [
                                            'name' => 'locales[' . $language->id . '][keywords]',
                                            'label' => 'Ключевые слова',
                                            'value' => $loc->keywords,
                                        ])
                                    </div>
                            @else
                                    <div class="tab-pane" id="m_tabs_{{$s}}">
                                        @include('partials.input', [
                                            'name' => 'locales[' . $language->id . '][title]',
                                            'label' => 'Заголовок',
                                        ])

                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <label for="">Краткое описание</label>
                                            <textarea class="summernote" name="locales[{{$language->id}}][description]"></textarea>
                                        </div>
                                        <br><br>

                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <label for="">Полное описание</label>
                                            <textarea class="summernote" name="locales[{{$language->id}}][text]"></textarea>
                                        </div>
                                        <br>


                                        @include('partials.input', [
                                            'name' => 'locales[' . $language->id . '][keywords]',
                                            'label' => 'Ключевые слова',
                                        ])
                                    </div>
                            @endif
                            @php $s++; @endphp
                        @endforeach
                    </div>

                    <br>
                    <h5>Общее</h5>
                    <hr>
                    <br>

                    @include('partials.input', [
                        'name' => 'slug',
                        'label' => 'URL',
                        'value' => $page->slug,
                    ])
                    @include('partials.input', [
                        'name' => 'active_from',
                        'label' => 'Дата публикации',
                        'class' => 'datepicker',
                        'value' => $page->active_from ? $page->active_from->format('m/d/Y') : '',
                    ])

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                        <a href="{{url('admin/pages')}}" class="btn btn-danger">Отмена</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script type="text/javascript" src="{{asset('assets/demo/default/custom/components/forms/widgets/summernote.js')}}"></script>
@endsection
