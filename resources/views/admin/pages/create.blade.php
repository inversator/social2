@extends('layouts.admin')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Создание новой страницы
                </h3>
                {!! Breadcrumbs::render([
                    '/admin' => 'Админ панель',
                    '/menu' => 'Страницы',
                    '/' => 'Создание новой страницы'
                ], 'metronic') !!}
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                @if(Session::get('message'))
                    <div class="alert alert-success" role="alert">
                        <strong>{{Session::get('message')}}</strong>
                    </div>
                @endif

                <form action="{{ route("admin.pages.store") }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}

                    @include('partials.select', [
                        'name' => 'language_id',
                        'label' => 'Язык',
                        'options' => $languages->pluck('code', 'id')->toArray()
                    ])

                    @include('partials.input', [
                        'name' => 'title',
                        'label' => 'Заголовок',
                    ])

                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <label for="">Краткое описание</label>
                        <textarea class="summernote" name="description"></textarea>
                    </div>
                    <br><br>

                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <label for="">Полное описание</label>
                        <textarea class="summernote" name="text"></textarea>
                    </div>
                    <br>

                    @include('partials.input', [
                        'name' => 'active_from',
                        'label' => 'Дата публикации',
                        'class' => 'datepicker',
                    ])

                    <br>
                        <div class="col-md-12">
                            <h5>SEO содержание</h5>
                        </div>
                    <hr>
                    @include('partials.input', [
                        'name' => 'keywords',
                        'label' => 'Ключевые слова',
                    ])

                    @include('partials.input', [
                        'name' => 'slug',
                        'label' => 'URL',
                    ])

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                        <a href="{{url('admin/pages')}}" class="btn btn-danger">Отмена</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script type="text/javascript" src="{{asset('assets/demo/default/custom/components/forms/widgets/summernote.js')}}"></script>
@endsection
