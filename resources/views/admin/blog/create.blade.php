@extends('layouts.admin')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Создание / Редактирование нового поста
                </h3>
                {!! Breadcrumbs::generate(['Админ панель', 'Блог', 'Создать/Редактировать пост'], 'metronic') !!}
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                @php
                    $link = isset($post) ? url("admin/blog/".$post->id) : url("admin/blog");
                @endphp
                <form action="{{ $link }}" method="POST" class="form-horizontal">

                    {{ csrf_field() }}
                    @if(isset($post))
                        <input type="hidden" name="_method" value="PUT">
                    @endif

                    <div class="col-lg-6 col-md-6 col-sm-9 col-xs-12 form-group">
                        <label for="cat">Выбор категории</label>
                        <select name="category_id" id="cat" class="form-control" required>
                            <option value="" disabled="" selected>Выберите категорию</option>
                            @forelse($categories as $category)
                                <option value="{{$category->id}}" @if(isset($post) && $post->category_id == $category->id) selected @endif>{{$category->title}}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>

                    <div class="col-lg-6 col-md-9 col-sm-12">
                        <label for="">Загаловок</label>
                        <input type="text" class="form-control" name="title" value="{{$post->title or ''}}">
                    </div>
                    <br>

                    <div class="col-lg-6 col-md-9 col-sm-12">
                        <label for="">Краткое описание</label>
                        <textarea class="summernote" name="description">{{$post->description or ''}}</textarea>
                    </div>
                    <br><br>

                    <div class="col-lg-6 col-md-9 col-sm-12">
                        <label for="">Полное описание</label>
                        <textarea class="summernote" name="text">{{$post->text or ''}}</textarea>
                    </div>

                    <br>
                    <div class="col-md-12">
                        <h5>SEO содержание</h5>
                    </div>
                    <hr>

                    <div class="col-md-12">
                        @if(isset($post))
                            @include('admin.blog.seo', ['data' => $post])
                        @else
                            @include('admin.blog.seo')
                        @endif
                    </div>

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                        <a href="{{url('admin/blog')}}" class="btn btn-danger">Отмена</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    <script type="text/javascript" src="{{asset('assets/demo/default/custom/components/forms/widgets/summernote.js')}}"></script>
@endsection