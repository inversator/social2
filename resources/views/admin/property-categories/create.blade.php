@extends('layouts.admin')

@section('content')
    <form action="{{ route("admin.categories.store") }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        @foreach ($languages as $language)
            @include('partials.input', [
                'name' => 'name[' . $language->id . ']',
                'label' => 'Название категории свойств на языке ' . $language->code
            ])
        @endforeach

        @include('partials.submit', [
            'title' => 'ADD'
        ])
    </form>
@endsection
