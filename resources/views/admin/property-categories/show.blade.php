@extends('layouts.admin')

@section('content')
    <form
        action="{{ route("admin.categories.update", ["id" => $category->id]) }}"
        method="POST"
        id="property-form"
        class="form-horizontal col-md-10">
        {{ csrf_field() }}
        {{method_field('PUT')}}

        @foreach ($languages as $language)
            @include('partials.input', [
                'name' => 'name[' . $language->id . ']',
                'value' => $category->getTranslatedProperty($language),
                'label' => 'Название свойства на языке ' . $language->code,
            ])
        @endforeach

        @include('partials.select', [
            'label' => 'Отображение',
            'name' => 'view',
            'options' => $views,
            'selectedItem' => $category->view
        ])


        <br class="clearfix" />

        <button type="submit" class="btn btn-info">
            Сохранить
        </button>

    </form>
@endsection
