@extends('layouts.admin')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Create Permission
                </h3>
                {!! Breadcrumbs::generate(['Dashboard', 'Permissions', 'Create Permission'], 'metronic') !!}
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">

                <form action="{{url('admin/permissions')}}{{ isset($permission) ? '/'.$permission->id : '' }}" method="POST">
            
                    {{csrf_field()}}

                    {{-- Переопределяем метод формы, если редактируется пользовательское разрешение --}}
                    @if (isset($permission))
                        <input type="hidden" name="_method" value="PUT">
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="system_name">System name (NOT CHANGE!!!!)</label>
                                <input class="form-control" id="system_name" type="text" name="name" value="{{$permission->name or ''}}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="display_name">Title</label>
                                <input class="form-control" id="display_name" type="text" name="display_name" value="{{$permission->display_name or ''}}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="description">Description (Optional)</label>
                            <textarea id="description" name="description" class="permission_desc form-control" cols="30" rows="5">{{$permission->description or ''}}</textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 form-group">
                            <button class="btn btn-info" type="submit" name="action">
                                Save
                            </button>
                            <a href="{{url('admin/permissions')}}" class="btn btn-danger" name="action">
                                Cancel
                            </a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

@endsection