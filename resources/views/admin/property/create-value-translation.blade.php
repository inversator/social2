@extends('layouts.admin')

@section('content')
    <div class="alert alert-info">
        {{ Translation::translateKey('LANGUAGE') }} {{ $language->code }}
    </div>

    <form action="{{ route("admin.property.create-value-translation", ["id" => $propertyValue->id, "languageId" => $language->id]) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        @include('partials.input', [
            'name' => 'translation',
            'label' => Translation::translateKey('PROPERTY_VALUE')
        ])

        @include('partials.submit', [
            'title' => 'ADD'
        ])
    </form>
@endsection
