@extends('layouts.admin')

@section('content')
    <form action="{{ route("admin.property.create") }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        @foreach ($languages as $language)
            @include('partials.input', [
                'name' => 'name[' . $language->id . ']',
                'label' => 'Название своства на языке ' . $language->code
            ])
        @endforeach

        @include('partials.select', [
            'label' => 'Категория',
            'name' => 'category',
            'options' => $categories,
            'selectedItem' => null
        ])

        @include('partials.select', [
            'label' => 'Тип поля',
            'name' => 'field_type',
            'options' => $fieldTypes,
            'selectedItem' => null
        ])

        @include('partials.submit', [
            'title' => 'ADD'
        ])
    </form>
@endsection
