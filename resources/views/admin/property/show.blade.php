@extends('layouts.admin')

@section('content')
    <form
        action="{{ route("admin.property.update", ["id" => $property->id]) }}"
        method="POST"
        id="property-form"
        class="form-horizontal col-md-10">
        {{ csrf_field() }}
        {{--{{ method_field('POST') }}--}}
        @foreach ($languages as $language)
            @include('partials.input', [
                'name' => 'name[' . $language->id . ']',
                'value' => $property->getTranslatedProperty($language),
                'label' => 'Название свойства на языке ' . $language->code,
            ])
        @endforeach

        @include('partials.select', [
            'label' => 'Категория',
            'name' => 'category',
            'options' => $categories,
            'selectedItem' => $property->category_id
        ])

        @include('partials.select', [
            'label' => 'Тип поля',
            'name' => 'field_type',
            'options' => $fieldTypes,
            'selectedItem' => $property->field_type
        ])

        @foreach ($languages as $language)
            <div class="card col-md-10">
                <div class="card-block">
                    <div class="card-title">
                        Язык {{ $language->code }}
                    </div>
                </div>
                <div class="card-block">
                    @foreach ($property->values as $value)
                            @php
                                $translation = $value
                                    ->translations
                                    ->where('language_id', $language->id)
                                    ->first();
                            @endphp
                            @if ($translation)
                                <a href="{{ route("admin.property.show-value-translation", ["id" => $translation->id]) }}" class="btn btn-default">
                                    {{ $translation->translation }}
                                </a>
                            @else
                                <a href="{{ route("admin.property.create-value-translation", ["id" => $value->id, "languageId" => $language->id]) }}" class="btn btn-primary">
                                    {{ Translation::translateKey('CREATE') }}
                                </a>
                            @endif
                    @endforeach
                </div>
            </div>
        @endforeach
        <br class="clearfix" />
        <a href="{{ route("admin.property.create-value", ["id" => $property->id]) }}" class="btn btn-info">
            Добавить значение свойства
        </a>
        <input type="submit" class="btn btn-info" value="Сохранить">
            {{--Сохранить--}}
        {{--</input>--}}

    </form>
@endsection
