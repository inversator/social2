@extends('layouts.admin')

@section('content')
    <form action="{{ route("admin.property.create-value", ["id" => $property->id]) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        @foreach ($languages as $language)
            @include('partials.input', [
                'name' => 'value[' . $language->id . ']',
                'label' => 'Значение свойства на языке ' . $language->code,
            ])
        @endforeach

        @include('partials.submit', [
            'title' => 'ADD'
        ])
@endsection
