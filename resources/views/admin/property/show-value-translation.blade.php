@extends('layouts.admin')

@section('content')
    <div class="alert alert-info">
        {{ Translation::translateKey('LANGUAGE') }} {{ $propertyValueTranslation->language->code }}
    </div>

    <form action="{{ route("admin.property.update-value-translation", ["id" => $propertyValueTranslation->id]) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        @include('partials.input', [
            'name' => 'translation',
            'label' => 'Значение свойства',
            'value' => $propertyValueTranslation->translation,
        ])

        @include('partials.submit', [
            'title' => 'SAVE'
        ])
    </form>
@endsection
