@extends('layouts.admin')

@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">
                    Dashboard
                </h3>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-inverse col-md-3">
        <div class="container-fluid">
            <div>
                <ul class="nav navbar-nav">
                    <li><a href="{{ route("admin.property.index") }}">{{ Translation::translateKey('PROPERTIES') }}</a></li>
                    <li><a href="{{ route("admin.categories.index") }}">{{ Translation::translateKey('CATEGORIES') }}</a></li>

                </ul>
            </div>
        </div>
    </nav>

    <div class="row col-md-9">
        @yield('admin-content')
    </div>
@endsection
