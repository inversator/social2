<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid text-center">
    <div class="row content">
        @yield('content')
    </div>
</div>

@yield('footer.js')
<script type="text/javascript">
    (function() {
        $('#language').on('change', function() {
            var code = $(this).val();
            window.location = '/language/' + code;
        });
    })();
</script>

</body>
</html>