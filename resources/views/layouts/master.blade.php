<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Sweet Life</title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
    <meta charset="utf-8"/>
    <!-- For old IEs -->
    <link rel="shortcut icon" href="img/favicon/favicon.ico"/>
    <link rel="icon" type="image/x-icon" sizes="16x16 32x32" href="img/favicon/favicon.ico">
    <link rel="icon" sizes="192x192" href="img/favicon/favicon-192.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="img/favicon/favicon-180.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="img/favicon/favicon-144.png">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <style>
        body {
            opacity: 0;
            overflow-x: hidden;
        }

        html {
            background-color: #fff;
        }

        select {
            display: none;
        }

    </style>
</head>
<body>

<!--  header Start-->
<header class="header">
    <div class="header__top-wrap">
        <div class="container">
            <div class="header__top">
                <a class="logo" href="/"><img src="{{url('/img/logo-clean.png')}}" alt="поиск содержанок и спонсоров"></a>
                <nav class="header__nav">
                    <ul>
                        @if(!Auth::check())
                            <li><a href="{{route('auth.login')}}" title="">Войти</a></li>
                            <li><a class="btn-link" href="{{route('auth.register')}}" title="">Регистрация</a></li>
                        @else
                            <li><a href="{{route('auth.logout')}}" title="">выход <i class="icon icon-out"></i></a></li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="header__bottom-wrap">
        <div class="container">
            <div class="header__bottom">
                <div class="mob-nav visible-xs visible-sm">
                    <a class="fa fa-bars" href="#menu"></a>
                    <nav class="header__menu" id="menu">
                        <ul>
                            <li><a href="" title="">Поиск</a></li>
                            <li><a href="" title="">Кто смотрел</a></li>
                            <li><a href="" title="">Рассылка</a></li>
                            <li><a href="" title="">Сообщения</a></li>
                        </ul>
                    </nav>
                </div>
                <nav class="header__menu hidden-xs hidden-sm" id="menu">
                    <ul>
                        <li><a href="{{ route('search') }}" title="">Поиск</a></li>
                        <li><a href="" title="">Кто смотрел</a></li>
                        <li><a href="" title="">Рассылка</a></li>
                        @if (Auth::user())
                            <li><a href="{{ route("profile.index") }}">Профиль</a></li>
                            <li><a href="{{ route("chat") }}">Сообщения</a></li>
                        @endif
                    </ul>
                    {{--<ul class="nav navbar-nav navbar-right">--}}
                        {{--<select id='language'>--}}
                            {{--@foreach ($languages as $l)--}}
                                {{--<option value="{{ $l->code }}" {{ $language == $l->code ? "selected" : "" }}>{{ $l->code }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--@if (Auth::user())--}}
                            {{--<li>--}}
                                {{--<form method='POST' action='{{ route('auth.logout') }}'>--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--<input type="submit" value="{{ Translation::translateKey('LOGOUT') }}"><span--}}
                                            {{--class="glyphicon glyphicon-log-in"></span> </input>--}}
                                {{--</form>--}}
                            {{--</li>--}}
                        {{--@else--}}
                            {{--<li><a href="{{ route('auth.login') }}"><span--}}
                                            {{--class="glyphicon glyphicon-log-out"></span> {{ Translation::translateKey('LOGIN') }}--}}
                                {{--</a></li>--}}
                        {{--@endif--}}
                    {{--</ul>--}}
                </nav>
                <nav class="header__action-panel">
                    @if(Auth::check())
                    <ul>
                        <li><a href="" title="">Счет: <span>100.00</span> руб.</a></li>
                        <li class="avatar__wrap"><a href="" title="">
							<span class="avatar" style="background-image: url({{Auth::user()->getMainPhoto()}})">
							{{--<img src="{{Auth::user()->getMainPhoto()}}" alt="">--}}
                            </span>
                                <span class="username">{{Auth::user()->name}}</span>
                            </a></li>
                        <li><a class="btn-buy" href="" title="">Купить премиум</a></li>
                    </ul>
                    @endif
                </nav>
            </div>
        </div>
    </div>
</header>
<!--  header   End-->

@yield('content')


<footer class="footer">
    <div class="footer__top-wrap">
        <div class="container">
            <div class="footer__top">
                <div class="footer__cattegory-column footer__logo">
                    <a href="/"><img src="{{url('img/logo-black.png')}}" alt=""></a>
                </div>
                <div class="footer__cattegory-column">
                    <ul class="footer__menu footer__menu-big">
                        <li class="title">Меню</li>
                        <li><a href="#">Анкеты</a></li>
                        <li><a href="#">Пользовательское соглашение</a></li>
                        <li><a href="#">Конфиденциальность</a></li>
                        <li><a href="{{url('feedback')}}">Обратная связь</a></li>
                    </ul>
                </div>
                <div class="footer__cattegory-column hidden-xs hidden-sm">
                    <ul class="footer__menu">
                        <li class="title">О нас</li>
                        <li><a href="#">О проекте</a></li>
                        <li><a href="#">Блог</a></li>
                        <li><a href="#">Правила</a></li>
                    </ul>
                </div>
                <div class="footer__cattegory-column footer__social social">
                    <div class="title">Следите за нами</div>
                    <ul>
                        <li><a class="fa fa-vk" href="#"></a></li>
                        <li><a class="fa fa-facebook" href="#"></a></li>
                        <li><a class="fa fa-twitter" href="#"></a></li>
                        <li><a class="fa fa-instagram" href="#"></a></li>
                        <li><a class="fa fa-instagram" href="#"></a></li>
                        <li><a class="fa fa-instagram" href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom-wrap">
        <div class="container">
            <div class="footer__bottom">
                <span class="copyright">{{date('Y', time())}} (с) Все права защищены</span>
                <span class="develop"><a href="">Glissmedia</a> - разработка и поддержка сайта</span>
            </div>
        </div>
    </div>
</footer>

<link rel="stylesheet" href="/css/main.min.css">
@yield('css')
<script src="/js/scripts.min.js"></script>


@yield('footer.js')
<script type="text/javascript">
    (function () {
        $('#language').on('change', function () {
            var code = $(this).val();
            window.location = '/language/' + code;
        });
    })();
</script>

</body>
</html>
