<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sweet Life</title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
    <meta charset="utf-8" />
    <!-- For old IEs -->
    <link rel="shortcut icon" href="img/favicon/favicon.ico" />
    <link rel="icon" type="image/x-icon" sizes="16x16 32x32" href="img/favicon/favicon.ico">
    <link rel="icon" sizes="192x192" href="img/favicon/favicon-192.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="img/favicon/favicon-180.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="img/favicon/favicon-144.png">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <style>
        body {
            opacity: 0;
            overflow-x: hidden;
        }

        html {
            background-color: #fff;
        }

        select {
            display: none;
        }

    </style>
</head>


<body>
<div class="g-header">
    <div class="container">
        <div class="g-header__top">
            <a href="/"><img src="img/logo.png" alt="поиск содержанок и спонсоров"></a>
            <nav class="g-header__nav">

                <ul>
                    @if(!Auth::check())
                        <li><a href="{{route('auth.login')}}" title="">войти</a></li>
                        <li><a class="btn-link" href="{{route('auth.register')}}" title="">Регистрация</a></li>
                    @else
                        <li><a href="{{route('auth.logout')}}" title="">выход <i class="icon icon-out"></i></a></li>
                    @endif
                </ul>

            </nav>
        </div>
        <div class="g-header__bottom">
            <div class="g-header__bottom-info">
                <h1 class="text-line">Знакомства <br>
                    для успешных мужчин</h1>
                <div class="title">Sweet life – делаем расчет
                    <br> в отношениях выгодным
                </div>
            </div>
            <div class="form-wrap">
                <div class="b-form form">
                    <div class="b-form__bg"></div>
                    <form>
                        <div class="top">
                            <div class="title">Найдите свою
                                <br> вторую половинку</div>
                            <div class="form-group">
                                <label>Кого вы ищете</label>
                                <select class="select-custom" placeholder="Девушку">
                                    <option value="0">Девушку</option>
                                    <option value="1">Парня</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Введите город</label>
                                <input type="text" class="form-control" placeholder="Город">
                            </div>
                            <div class="form-group form-group__age">
                                <label>Выберите возраст</label>
                                <input class="age-slider" type="slider">
                            </div>
                            <div class="btn-wrap">
                                <button type="submit" class="btn-link btn-link__accent">Искать</button>
                            </div>
                        </div>
                        <div class="bottom">
                            <label>
                                <input type="checkbox" checked="">
                                <span class="icon"></span>
                                <span class="checkbox__title">Запомнить мой выбор</span>
                            </label>
                            <a href="#" class="checkbox__help fa fa-question-circle"></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<main>
    @yield('content')
</main>
<footer class="footer">
    <div class="footer__top-wrap">
        <div class="container">
            <div class="footer__top">
                <div class="footer__cattegory-column footer__logo">
                    <a href="/"><img src="/img/logo-black.png" alt=""></a>
                </div>
                <div class="footer__cattegory-column">
                    <ul class="footer__menu footer__menu-big">
                        <li class="title">Меню</li>
                        <li><a href="#">Анкеты</a></li>
                        <li><a href="#">Пользовательское соглашение</a></li>
                        <li><a href="#">Конфиденциальность</a></li>
                        <li><a href="{{url('feedback')}}">Обратная связь</a></li>
                    </ul>
                </div>
                <div class="footer__cattegory-column hidden-xs hidden-sm">
                    <ul class="footer__menu">
                        <li class="title">О нас</li>
                        <li><a href="#">О проекте</a></li>
                        <li><a href="#">Блог</a></li>
                        <li><a href="#">Правила</a></li>
                    </ul>
                </div>
                <div class="footer__cattegory-column footer__social social">
                    <div class="title">Следите за нами</div>
                    <ul>
                        <li><a class="fa fa-vk" href="#"></a></li>
                        <li><a class="fa fa-facebook" href="#"></a></li>
                        <li><a class="fa fa-twitter" href="#"></a></li>
                        <li><a class="fa fa-instagram" href="#"></a></li>
                        <li><a class="fa fa-instagram" href="#"></a></li>
                        <li><a class="fa fa-instagram" href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom-wrap">
        <div class="container">
            <div class="footer__bottom">
                <span class="copyright">{{date('Y', time())}} (с) Все права защищены</span>
                <span class="develop"><a href="">Glissmedia</a> - разработка и поддержка сайта</span>
            </div>
        </div>
    </div>
</footer>

<link rel="stylesheet" href="css/main.min.css">
@yield('css')
<script src="js/scripts.min.js"></script>

@yield('footer.js')
<script type="text/javascript">
    (function () {
        $('#language').on('change', function () {
            var code = $(this).val();
            window.location = '/language/' + code;
        });
    })();
</script>
</body>

</html>
