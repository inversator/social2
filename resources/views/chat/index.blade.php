@extends('layouts.master')

@section('content')
    <!-- По ксс:
     .chat .list .datetime font-style италик не работает скрывает текст
     Линия прокрутки (скроллер) оставлет свободное место справа отс себя
    -->
    <style>
        .chat .list .datetime {
            color: #8c8c8c !important;
            font-style: inherit !important;
        }

        #sSaver {
            text-align: center;
            padding: 130px 0 0;
            color: cadetblue;
        }

        .message .dropdown-menu {
            top: auto;
            left: auto;
            right: 10px;
            min-width: auto;
        }

        .message .dropdown-menu li {
            padding-top: 10px !important;
        }

        /*li:hover #emojiList {*/
        /*display: block;*/
        /*}*/

        #emojiList {
            left: auto;
            top: auto;
        }

        #emojiList li {
            display: block;
        }

        #emojiList .item {
            display: inline-block;
        }

        #emojiList img {
            width: 30px;
            padding: 3px;
            cursor: pointer;
        }

        .btn-file {
            position: relative;
            overflow: hidden;
        }

        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>
    <div class="chat">
        <div class="chat__top">
            <ul>
                <li class="active">
                    <a href="javascript:void(0)">Все</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Непрочитанные</a>
                </li>
                <li>
                    <a href="javascript:void(0)"><i class="fa fa-star"></i>Избранные</a>
                </li>
                <li>
                    <a href="javascript:void(0)">Черный список</a>
                </li>
            </ul>
            <span class="btn-close fa fa-close"></span>
        </div>
        <div class="chat__main">
            <div class="chat-sidebar__head visible-xs">
                <input type="search" placeholder="Поиск">

                <div class="panel-settings">
                    <span>Настройки папки</span>
                    <span class="btn-settings fa fa-cog"></span>
                </div>
            </div>
            <div class="chat__sidebar chat-sidebar">
                <div class="chat-sidebar__head hidden-xs">
                    <input type="search" placeholder="Поиск">

                    <div class="panel-settings">
                        <span>Настройки папки</span>
                        <span class="btn-settings fa fa-cog"></span>
                    </div>
                </div>
                <div class="chat-sidebar__body">
                    <div class="chat-sidebar__contact-list contacts-list">
                        <ul>
                            @forelse($pipes as $pipe)
                                <li class="contact chatPipe"
                                    data-pipe="{{$pipe['pipe']}}"
                                    data-chat-user="{{$pipe['id']}}"
                                    @if($pipe['status'] == 'blacklist')
                                    data-pipe-status="blacklist"
                                    @else
                                    @endif
                                    data-pipe-type="private">
                                    <div class="avatar">
                                        @if($pipe['thumbnail'])
                                            <img alt="" src="{{$pipe['thumbnail']}}">
                                        @else
                                            <img alt=""
                                                 src="https://cdn3.iconfinder.com/data/icons/user-2/100/9-512.png">
                                        @endif
                                    </div>
                                    <div class="info">
                                        <span class="status @if($pipe['is_online']) status-online @else status-ofline @endif"></span>
                                        <span class="title">{{ ucfirst($pipe['first_name']) }} {{$pipe['birthday']}}, Москва</span>
                                        <span class="text">
                                        @if($pipe['count_messages'])
                                                Cообщений - <span>{{$pipe['count_messages']}}</span>
                                            @else
                                                Нет сообщений
                                            @endif
                                            @if(isset($unreadCount[$pipe['pipe']]))
                                                <span class="badge">{{$unreadCount[$pipe['pipe']]->count}}</span>
                                            @else
                                                <span style="display: none" class="badge">0</span>
                                            @endif

                                        </span>
                                    </div>
                                    @if($pipe['status'] == 'favorite')
                                        <span class="fa fa-star checked"></span>
                                    @else
                                        <span class="fa fa-star"></span>
                                    @endif
                                </li>
                            @empty
                                <li> — Пусто —</li>
                            @endforelse

                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <div class="chat__content" id="chatBlock">
                    <ul class="list">
                        <div id="sSaver"> — Выберите собеседника из списка —</div>
                    </ul>
                    <div id="tapStatus"></div>

                    <div class="chat__send-panel hidden-xs">
                        <div class="input-wrap" id="enterMessage">
                            <input type="text" placeholder="Ваше сообщение...">
                            <span class="help-block">(Для отправки нажмите Enter, перенос строки - Shift + Enter)</span>
                        </div>
                        <div class="send-panel">
                            <ul class="">
                                <li><a class="fa fa-gift" href="#"></a></li>
                                <li data-toggle="modal" data-target="#uploadImage"><a onclick="return false;"
                                                                                      class="fa fa-camera" href="#"></a>
                                </li>
                                <li>
                                    <a class="fa fa-smile-o dropdown-toggle" data-toggle="dropdown" aria-expanded="true"
                                       href="#"></a>
                                    <ul id="emojiList" class="dropdown-menu message-edit">
                                        <li>
                                            <div class="item">😁</div>
                                            <div class="item">😂</div>
                                            <div class="item">😃</div>

                                            <div class="item">😄</div>
                                            <div class="item">😅</div>
                                            <div class="item">😆</div>
                                        </li>
                                        <li>
                                            <div class="item">😉</div>
                                            <div class="item">😊</div>
                                            <div class="item">😋</div>

                                            <div class="item">😌</div>
                                            <div class="item">😍</div>
                                            <div class="item">😏</div>
                                        </li>
                                        <li>
                                            <div class="item">😒</div>
                                            <div class="item">😓</div>
                                            <div class="item">😔</div>

                                            <div class="item">😖</div>
                                            <div class="item">😘</div>
                                            <div class="item">😜</div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <button type="submit" class="btn-send">Отправить <span class="fa fa-envelope"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="chat__send-panel visible-xs">
                    <div class="input-wrap">
                        <input type="text" placeholder="Ваше сообщение...">
                        <span class="help-block">(Для отправки нажмите Enter, перенос строки - Shift + Enter)</span>
                    </div>
                    <div class="send-panel">
                        <ul class="">
                            <li><a class="fa fa-gift" href="#"></a></li>
                            <li><a class="fa fa-camera	" href="#"></a></li>
                            <li><a class="fa fa-smile-o" href="#"></a></li>
                        </ul>
                        <button type="submit" class="btn-send">Отправить <span class="fa fa-envelope"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal delete chat message-->
    <div class="modal fade" id="delChatMessage" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">УДАЛИТЬ СООБЩЕНИЕ</h4>
                </div>
                <div class="modal-body">
                    Вы действительно хотите удалить это сообщение?
                    <div id="modalMessage">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена
                    </button>
                    <button id="delButton" type="button" class="btn btn-danger">Удалить</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal upload image-->
    <div class="modal fade" id="uploadImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Загрузка изображений</h4>
                </div>
                <div class="modal-body">

                    <div class="input-group">
                        <label class="input-group-btn">
                    <span class="btn btn-primary">
                        Выбрать файл… <input type="file" style="display: none;" multiple="">
                    </span>
                        </label>
                        <input type="text" class="form-control" readonly="">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primar upload">Загрузить</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer.js')
    <script src="{{asset('js/moment-with-locales.min.js')}}"></script>
    <script type="text/javascript">
        moment.locale('ru');
    </script>
    <!-- Chat scripts -->
    <script type="text/javascript" src="{{asset('js/autobahn.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/chatroom.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/emoji.js')}}"></script>
    <script>
        $('#chatBlock').chatRoom({
            name: '{{$user->first_name}} {{$user->second_name}}',
            userType: 'user',
            userId: '{{$user->id}}'
        });
        localStorage.setItem('userPipes', '<?php echo json_encode($unreadCount) ?>');

        $(document).on('change', ':file', function() {

            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

            input.trigger('fileselect', [numFiles, label]);
            $('#uploadImage input[type=text]').val(label);
        });


    </script>

@endsection