@extends('layouts.master')

@section('content')
    @include('maskcss')

    <link type="text/css" rel="stylesheet" href="{{asset('css/gliss/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/gliss/fonts.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/gliss/font-awesome.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/gliss/ionicons.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/gliss/bootstrap-select.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/gliss/jquery.mCustomScrollbar.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/gliss/bootstrap-datetimepicker.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/gliss/main.css')}}">
    <style>
        .message-content__inner {
            width: 100%
        }

        .b-chats{
            padding-bottom: 55px;
        }

        .b-chats .avatar {
            overflow: hidden;
            z-index: 10;
        }

        .online .avatar:after {
            z-index: 100;
        }

        .b-message-aside .head {
            display: inline-table;
        }

        .head__inner img {
            border-radius: 50px;
        }

        .b-chats .avatar__inner {
            height: 52px;
            width: 52px;
        }

        .message-content__inner {
            background: #fff;
            border-radius: 5px;
        }

        #sSaver {
            text-align: center;
            padding-bottom: 20px;
            display: inherit;
            color: #ced1d4;
            line-height: 74px;
        }

        #tapStatus {
            padding: 10px 25px 25px 25px;
            color: #7a8798;
            position: absolute;
            width: 100%;
            height: 50px;
            bottom: 0;
            z-index: 1000;
            background: rgba(255, 255, 255, 0.61);
            display: none;
            line-hight: 10px;
        }

        .pipeOn {
            background: #fff;
        }

        .b-chats li.out + li.out .avatar__inner, .b-chats li.in + li.in .avatar__inner{display: none;}
        .b-chats li.out + li.out .st-online, .b-chats li.in + li.in .st-online{display: none;}
        .b-chats li.out + li.out .name, .b-chats li.in + li.in .name{display: none;}
        .b-chats li.in:first-child .body, .b-chats li.out:first-child .body{
            border: none;
        }
        .b-chats li.out + li.out .avatar, .b-chats li.in + li.in .avatar{height: 0;}
        .b-chats li.out + li.out , .b-chats li.in + li.in {
            padding-bottom: 0;
            padding-top: 0;
        }

        .b-chats li.out + li.out .message, .b-chats li.in + li.in .message{
            padding-bottom: 0;
        }
        .b-chats li{
            padding-bottom: 0;}
        .b-chats li .body{
            border: none;
        }
        .b-chats .message{
            padding-bottom: 0;
        }
        li.bordered{
            border-top: 1px solid rgba(224, 228, 232, 0.5);
            margin-top: 10px;
        }


    </style>

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="message-wrap">
                    <div class="b-message-aside">

                        @if(count($pipes))
                            <div class="category">
                                <div class="category__inner">
                                    <div class="category-heading">
                                        <a data-toggle="collapse" href="#category-3">Клиенты<i
                                                    class="fa fa-chevron fa-fw"></i>
                                            @if($unreadCount['client'])
                                                <span class="badge">{{$unreadCount['client']}}</span>
                                            @else
                                                <span style="display: none" class="badge">0</span>
                                            @endif
                                        </a>
                                    </div>
                                    <div id="category-3" class="collapse">
                                        <ul class="contacts-list">
                                            @forelse($pipes as $pipe)

                                                <li class="contact chatPipe" data-pipe="{{$pipe->pipe}}"
                                                    data-pipe-type="client">
                                                    <a href="#">
                                                        <div class="head">
                                                            <div class="head__inner">
                                                                @if(FALSE)
                                                                    <div class="st-online"></div>
                                                                @else
                                                                    <div style="display: none;" class="st-online"></div>
                                                                @endif
                                                                <b>G</b>
                                                                @if(isset($unreadCount[$pipe->pipe]))
                                                                    <span class="badge">{{$unreadCount[$pipe->pipe]->count}}</span>
                                                                @else
                                                                    <span style="display: none" class="badge">0</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="info">
                                                            <div class="info-name"
                                                                 title="{{$pipe->pipe}}">{{ (strlen($pipe->pipe) > 20) ? substr($pipe->pipe, 0, 15).'...' : $pipe->pipe}} </div>
                                                            <div class="info-text">{{date('Y-m-d', time())}}</div>
                                                        </div>
                                                    </a>
                                                </li>
                                            @empty
                                                <li> — Пусто —</li>
                                            @endforelse

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if (count($private))
                            <div class="category">
                                <div class="category__inner">
                                    <div class="category-heading">
                                        <a data-toggle="collapse" href="#category-4">Личные сообщения<i
                                                    class="fa fa-chevron fa-fw"></i>
                                            @if($unreadCount['private'])
                                                <span class="badge">{{$unreadCount['private']}}</span>
                                            @else
                                                <span style="display: none" class="badge">0</span>
                                            @endif
                                        </a>
                                    </div>

                                    <div id="category-4" class="collapse">
                                        <ul class="contacts-list">
                                            @foreach($private as $userInfo)
                                                <li class="contact chatPipe"
                                                    data-pipe="{{$userInfo->pipe}}"
                                                    data-chat-user="{{$userInfo->id}}"
                                                    data-pipe-type="private"
                                                        >
                                                    <a href="#">
                                                        <div class="head">
                                                            <div class="head__inner">
                                                                @if(userOnlineOnVisit($userInfo->last_visit))
                                                                    <div class="st-online"></div>
                                                                @else
                                                                    <div style="display: none;" class="st-online"></div>
                                                                @endif
                                                                @if ( isset($userInfo->photo) )
                                                                    <img src="{{ $userInfo->photo }}"
                                                                         alt="{{ $userInfo->second_name }} {{$userInfo->first_name}}"
                                                                         class="circle photo">
                                                                @else
                                                                    <b>G</b>
                                                                @endif
                                                                @if(isset($unreadCount[$userInfo->pipe]))
                                                                    <span class="badge">{{$unreadCount[$userInfo->pipe]->count}}</span>
                                                                @else
                                                                    <span style="display: none" class="badge">0</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="info">
                                                            <div class="info-name">{{ $userInfo->second_name }} {{$userInfo->first_name}}</div>
                                                            <div class="info-text">
                                                                @if ( $userInfo->company )
                                                                    {{--<a href="/company/{{ $userInfo->company_id }}">--}}
                                                                    {{ $userInfo->company }}
                                                                    {{--</a>--}}
                                                                @else
                                                                    Частное лицо
                                                                @endif
                                                                @if(!userOnlineOnVisit($userInfo->last_visit))
                                                                    <span>{{timeAgo($userInfo->last_visit)}}</span>@endif
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="message-content">
                        <div class="message-content__inner">
                            <!-- Dispatch -->
                            <div class="dispatch-wrap" style="display: none">
                                <div class="b-dispatch" id="chatApartments">
                                    <div class="top">

                                    </div>
                                    <ul class="dispatch-list">
                                    </ul>
                                    <a class="btn-collapse-dispatch" href="#">Все <i class="icon"></i></a>
                                </div>
                            </div>
                            <!-- Chat -->
                            <div class="chat-wrap" id="chatBlock">
                                <ul class="b-chats"></ul>
                                <div id="tapStatus"></div>
                            </div>
                            <div id="sSaver"> — Выберите собеседника из списка —</div>
                        </div>

                        <div class="chat-form" id="enterMessage">
                            <input style="display: none" type="text" placeholder="Написать ...">
                        </div>

                        <!-- Modal delete chat message-->
                        <div class="modal fade" id="delChatMessage" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">УДАЛИТЬ СООБЩЕНИЕ</h4>
                                    </div>
                                    <div class="modal-body">
                                        Вы действительно хотите удалить это сообщение?
                                        <div id="modalMessage">

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена
                                        </button>
                                        <button id="delButton" type="button" class="btn btn-danger">Удалить</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
@endsection
@section('js')
    <script src="{{asset('js/gliss/moment-with-locales.min.js')}}"></script>
    <script type="text/javascript">
        moment.locale('ru');
    </script>
    <!-- Chat scripts -->
    <script type="text/javascript" src="{{asset('js/autobahn.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/chatroom.js')}}"></script>
    <script>
        $('#chatBlock').chatRoom({
            name: '{{$user->first_name}} {{$user->second_name}}',
            userType: 'user',
            userId: '{{$user->id}}'
        });
        localStorage.setItem('userPipes', '<?php echo json_encode($unreadCount) ?>');
    </script>
    <!-- Chat scripts end-->
    <script src="../frontend/js/gliss/bootstrap.min.js"></script>


    <script type="text/javascript" src="../frontend/js/gliss/jquery.mCustomScrollbar.min.js"></script>
    <!-- <script type="text/javascript" src="js/jquery.inputmask.bundle.min.js"></script> -->
    <script type="text/javascript">
        $(document).ready(function () {
            // $(".notification-list").mCustomScrollbar();
            $(".notification-list, .b-message-aside, .message-content__inner").mCustomScrollbar({
                scrollButtons: {enable: true, scrollType: "stepped"},
                keyboard: {scrollType: "stepped"},
                mouseWheel: {scrollAmount: 188, normalizeDelta: true},
                theme: "rounded-dark",
                autoExpandScrollbar: true,
                snapAmount: 20,
                snapOffset: 20
            });
        });
    </script>
@endsection
