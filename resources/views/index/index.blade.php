@extends('layouts.index')

@section('content')
<main>
    <div class="el el-1">
        <div class="el__inner">
        </div>
    </div>
    <div class="el el-2">
        <div class="el__inner">
        </div>
    </div>
    <!-- Section catalog Start-->
    <section class="catalog__wrap">
        <div class="container">
            <h2 class="text-line">Новые девушки<span><a class="link" href="#">Больше девушек</a></span></h2> <div class="catalog">
                <div class="catalog__list">
                    @forelse($girls as $girl)
                        <a href="{{url('/profile/'. $girl->id) }}" class="catalog__item">
                            {{--<img src="img/img-1.jpg" alt="">--}}
                            <div class="image" style="background-image: url({{url($girl->getMainPhoto('original'))}}); height:403px; background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                            <div class="info">
                                <div class="title">
                                    @if($girl->isOnline())
                                        <span class="status status-online"></span>
                                    @endif
                                    <span class="name">{{$girl->name}}</span>,
                                    <span class="age">{{$girl->getAge()}}</span>
                                </div>
                                <div class="from">{{$girl->city->title}}, {{$girl->country->title}}</div>
                                <div class="btn-send">Написать <span class="fa fa-envelope-o"></span></div>
                            </div>
                        </a>
                    @empty

                    @endforelse

                </div>
            </div>

        </div>
    </section>
    <!-- Section catalog   End-->
    <!-- Section about Start-->
    <section class="about-wrap">
        <div class="container">
            <div class="about">
                <div class="info">
                    <div class="title">Проект sweet life</div>
                    <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст.</p>
                    <a class="link" href="#">подробнее</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Section about   End-->
    <!-- Section aplication Start-->
    <section class="aplication-wrap">
        <div class="container">
            <div class="aplication">
                <div class="info">
                    <h2 class="text-line"> Сео текст</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod excepturi alias veritatis praesentium, voluptas eius quisquam at tenetur id perferendis error deserunt nulla, hic saepe quaerat incidunt animi tempora sit illum dolor odit. Incidunt ex, tempora, quisquam laudantium unde ullam? Eius, distinctio necessitatibus id? Autem amet ex, obcaecati sit natus.</p>
                </div>
                <div class="image">
                    <div class="el el-3">
                        <div class="el__inner">
                        </div>
                    </div>
                    <img src="img/image-11.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- Section aplication   End-->
    <section class=" g-subscribe-wrap">
        <div class="container">
            <div class=" g-subscribe">
                <div class="info">
                    <h2 class="text-line"> Подпишитесь на актуальные обновления нашего сайта</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod excepturi alias veritatis praesentium, voluptas eius quisquam at tenetur id perferendis</p>
                    <div class="b-subscribe">
                        <form action="" onsubmit="return gliss.index.subscribe()">
                            <div class="form-group">
                                <label>Введите e-mail</label>
                                <input type="email" required="" class="form-control" value="mail@mail.ru">
                            </div>
                            <button type="submit" class="btn-link btn-link__accent">Подписаться</button>
                        </form>
                    </div>
                </div>
                <div class="el el-3-1">
                    <div class="el__inner">
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection

@section('footer.js')
    <script src="js/gliss/gliss.modal.js"></script>
    <script src="js/gliss/gliss.index.js"></script>
@endsection