@extends('layouts.master')

@section('content')

    <form method="POST" action="" class="form-horizontal">
        {{ csrf_field() }}

        <div class="form-group @if ($errors->has('height')) has-error @endif">
            <label for="height">Рост: </label>
            <input class="form-control" type="text" name="height" value="{{ Auth::user()->height }}">
            @if ($errors->has('height')) <p class="help-block">{{ $errors->first('height') }}</p> @endif
        </div>

        <div class="form-group @if ($errors->has('weight')) has-error @endif">
            <label for="weight">Вес: </label>
            <input class="form-control" type="text" name="weight" value="{{ Auth::user()->weight }}">
            @if ($errors->has('weight')) <p class="help-block">{{ $errors->first('weight') }}</p> @endif
        </div>

        @if (Auth::user()->isFemale())
            <div class="form-group @if ($errors->has('breast_size')) has-error @endif">
                <label for="breast_size">Размер груди: </label>
                <input class="form-control" type="text" name="breast_size" value="{{ Auth::user()->breast_size }}">
                @if ($errors->has('breast_size')) <p class="help-block">{{ $errors->first('breast_size') }}</p> @endif
            </div>
        @endif

        @foreach ($properties as $property)
            @php
            $key = "property[$property->id]";
            @endphp
            <div class="form-group @if ($errors->has($key)) has-error @endif">
                <label for="{{ $key }}">{{ Translation::translateProperty($property) }}</label>
                <select class="form-control" name="{{ $key }}">
                    <option value=""></option>
                    @foreach ($property->values as $value)
                        @php
                            $userProperty = isset($userPropertyValues[$property->id]) ? $userPropertyValues[$property->id] : null;
                        @endphp
                        <option value="{{ $value->id }}" {{ $userProperty == $value->id ? "selected" : "" }}>{{ Translation::translatePropertyValue($value) }}</option>
                    @endforeach
                </select>
                @if ($errors->has($key)) <p class="help-block">{{ $errors->first($key) }}</p> @endif
            </div>
        @endforeach
        
        <button type="submit" class="btn btn-default">Сохранить</button>
    </form>
@endsection
