@extends('layouts.master')

@section('content')
    @php
    $user = Auth::user();
    @endphp
    <div class="col-sm-6 col-sm-offset-3">
        Заполните недостающие данные
        <form class="form-horizontal" method="POST" action="{{ route("auth.complete") }}">
            {{ csrf_field() }}

            @if (!$user->phone)
                <div class="form-group @if ($errors->has('phone')) has-error @endif">
                    <label for="phone">Телефон: </label>
                    <input class="form-control" type="text" name="phone" value="{{ old("phone") }}">
                    @if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
                </div>
            @endif

            @if (!$user->name)
                <div class="form-group @if ($errors->has('name')) has-error @endif">
                    <label for="name">Имя: </label>
                    <input class="form-control" type="text" name="name" value="{{ old("name") }}">
                    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                </div>
            @endif

            @if (!$user->country_id)
                <div class="form-group @if ($errors->has('country_id')) has-error @endif">
                    <label for="country_id">Страна: </label>
                    <select name="country_id">
                        <option value=""></option>
                        @foreach ($countries as $country)
                            <option value="{{ $country->id }}" {{ old("country_id") == $country->id ? "selected" : "" }}>{{ $country->title }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('country_id')) <p class="help-block">{{ $errors->first('country_id') }}</p> @endif
                </div>
            @endif

            @if (!$user->city_id)
                <div class="form-group @if ($errors->has('city_id')) has-error @endif">
                    <label for="city_id">Город: </label>
                    <select name="city_id">
                        <option value=""></option>
                        @foreach ($countries as $country)
                            @foreach ($country->cities as $city)
                                <option
                                    data-country-id="{{ $country->id }}"
                                    value="{{ $city->id }}"
                                    {{ old("city_id") == $city->id ? "selected" : "" }}>
                                    {{ $city->title }}
                                </option>
                            @endforeach
                        @endforeach
                    </select>
                    @if ($errors->has('city_id')) <p class="help-block">{{ $errors->first('city_id') }}</p> @endif
                </div>
            @endif

            @if (!$user->birthday)
                <div class="form-group @if ($errors->has('birthday')) has-error @endif">
                    <label for="birthday">День рождения: </label>
                    <input class="form-control" type="text" name="birthday" value="{{ old("birthday") }}" placeholder="dd/mm/yyyy">
                    @if ($errors->has('birthday')) <p class="help-block">{{ $errors->first('birthday') }}</p> @endif
                </div>
            @endif

            @if (!$user->gender)
                <div class="form-group @if ($errors->has('gender')) has-error @endif">
                    <label for="birthday">Пол: </label>
                    М. <input type="radio" name="gender" value="M" {{ old('gender') == 'M' ? 'checked' : '' }}>
                    Ж. <input type="radio" name="gender" value="F" {{ old('gender') == 'F' ? 'checked' : '' }}>
                    @if ($errors->has('gender')) <p class="help-block">{{ $errors->first('gender') }}</p> @endif
                </div>
            @endif

            <input type="submit" class="btn btn-default" value="Сохранить">
        </form>
    </div>
@endsection

@section('footer.js')
    <script type="text/javascript">
    (function() {
        var country = $('select[name=country_id]');
        var city = $('select[name=city_id]');
        var city_id = city.val();

        country.on('change', function() {
            var country_id = $(this).val();
            city.val('');
            city.find('option[data-country-id]').hide();
            city.find('option[data-country-id=' + country_id + ']').show();
        });

        $('select[name=country_id]').trigger('change');
        city.val(city_id);
    })();
    </script>
@endsection
