@extends('layouts.master')

@section('content')
    <main>
        <div class="el el-1">
            <div class="el__inner">
            </div>
        </div>
        <div class="el el-2">
            <div class="el__inner">
            </div>
        </div>
        {{--<div class="p-profile page-main">--}}
            <div class="">
                <div class="col-sm-4 col-sm-offset-4 login">
                    <h2 class="title">Вход</h2>
        <form class="form-horizontal" method="POST" action="{{ route("auth.login") }}">
            {{ csrf_field() }}

            <div class="form-group @if ($errors->has('email')) has-error @endif">

                <input class="form-control" type="text" name="email" value="{{ old("email") }}" placeholder="E-mail">
                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
            </div>

            <div class="form-group @if ($errors->has('password')) has-error @endif">

                <input class="form-control" type="password" name="password" placeholder="Пароль">
                @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
            </div>

            <div class="form-group">
                <input type="checkbox" name="remember" {{ old('remember') ? "checked" : "" }}>
                <label for="password">REMEMBER_ME</label>

            </div>
            <div class="form-group">
            <input type="submit" class="btn btn-send" value="LOGIN">
            </div>
            <div class="form-group">
            <a href="{{ route("auth.register") }}">REGISTRATION</a>
            </div>

            <div class='form-group'>
                <script src="//ulogin.ru/js/ulogin.js"></script>
                @php
                    $authUrl = rawurlencode(route('auth.ulogin'));
                @endphp
                FACEBOOK_LOGIN
                <div class="social" id="uLogin" data-ulogin="display=small;theme=classic;fields=first_name,last_name;providers=facebook;redirect_uri={{ $authUrl }};mobilebuttons=0;"></div>
            </div>
        </form>


        

    </div>
            </div>
        {{--</div>--}}
    </main>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection