@extends('layouts.master')

@section('content')
    @php
    $user = Auth::user();
    @endphp
    <div class="col-sm-6 col-sm-offset-3">
        <form class="form-horizontal" method="POST" action="{{ route("auth.verify_phone") }}">
            {{ csrf_field() }}

            <div class="form-group @if ($errors->has('confirmation_code')) has-error @endif">
                <label for="confirmation_code">Код подтверждения: </label>
                <input class="form-control" type="text" name="confirmation_code" value="{{ old("confirmation_code") }}">
                @if ($errors->has('confirmation_code')) <p class="help-block">{{ $errors->first('confirmation_code') }}</p> @endif
            </div>

            <input type="submit" class="btn btn-default" name="check_code" value="Проверить код">
            <input type="submit" class="btn btn-default" name="request_code" value="Запросить новый код">
        </form>
    </div>
@endsection
