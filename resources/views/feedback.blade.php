@extends('layouts.master')

@section('content')
    <div class="el el-6">
        <div class="el__inner">
        </div>
    </div>
    <div class="el el-7">
        <div class="el__inner">
        </div>
    </div>
    <div class="p-feedback page-main">
        <div class="container">
            <div class="feedback__form feedback-form">
                <div class="feedback-form__top">
                    <div class="title">Обратная связь</div>
                </div>
                <div class="feedback-form__body">
                    <form method="POST" acction="{{url('feedback')}}">
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" class="form-control" placeholder="Имя" name="name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Почта</label>
                                <input type="email" class="form-control" placeholder="e-mail" name="email">
                            </div>
                        </div>
                        <div class="form-group form-group__textarea">
                            <label>Комментарий</label>
                            <textarea placeholder="Выберите причину обращения" name="text"></textarea>
                        </div>
                        <div class="wrap">
                            <div class="info-block">
                                По всем остальным вопросам обращайтесь на почту
                                <a href="mailto:support@sweetlife.ru">support@sweetlife.ru</a>
                            </div>
                            <button type="submit" class="btn-send">Отправить <span class="fa fa-envelope"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection