@php
    $value = isset($value) ? $value : old($name);
    $errorKey = preg_replace('/\[/', '.', $name);
    $errorKey = preg_replace('/\]\./', '.', $errorKey);
    $errorKey = preg_replace('/\]/', '', $errorKey);
    $class = isset($class) ? $class . " form-control" : "form-control";
    $label = isset($label) ? $label : '';
@endphp

<div class="form-group{{ $errors->has($errorKey) ? ' has-error' : '' }}">
    <label for="{{ $name }}" class="col-md-2 control-label">{{ $label }}</label>

    <div class="col-md-6 col-sm-9 col-xs-12">
        <textarea class="{{ $class }}" name="{{ $name }}">{{ $value }}</textarea>
        @if ($errors->has($errorKey))
            <span class="help-block">
                <strong>{{ $errors->first($errorKey) }}</strong>
            </span>
        @endif
    </div>
</div>
