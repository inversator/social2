@php
    $value = isset($value) ? $value : old($name);
    $errorKey = preg_replace('/\[/', '.', $name);
    $errorKey = preg_replace('/\]\./', '.', $errorKey);
    $errorKey = preg_replace('/\]/', '', $errorKey);
    $class = isset($class) ? $class . " form-control" : "form-control";
    $label = isset($label) ? $label : '';
@endphp

<div class="form-group{{ $errors->has($errorKey) ? ' has-error' : '' }}">
    <label for="{{ $name }}" class="control-label">{{ $label }}</label>

    <div class="">
        <input type="text" class="{{ $class }}"
            name="{{ $name }}" value="{{ Auth::user()->getValueProperty($property, $lang) }}" />
        @if ($errors->has($errorKey))
            <span class="help-block">
                <strong>{{ $errors->first($errorKey) }}</strong>
            </span>
        @endif
    </div>
</div>
