@php
    $value = isset($value) ? $value : old($name);
    $errorKey = preg_replace('/\[/', '.', $name);
    $errorKey = preg_replace('/\]\./', '.', $errorKey);
    $errorKey = preg_replace('/\]/', '', $errorKey);
    $class = isset($class) ? $class . " form-control" : "form-control";
    $label = isset($label) ? $label : '';
@endphp

<div class="form-group{{ $errors->has($errorKey) ? ' has-error' : '' }}">
    <label for="{{ $name }}" class=" control-label">{{ $label }}</label>

        <input type="checkbox" class="{{ $class }}"
            name="{{ $name }}" value="1" ,
            @if (Auth::user()->getValueProperty($property, $lang) == 1) checked @endif
        />
        @if ($errors->has($errorKey))
            <span class="help-block">
                <strong>{{ $errors->first($errorKey) }}</strong>
            </span>
        @endif

</div>
