<p>

    @forelse ($properties as $property)

        @php

            $value = Auth::user()->getValueProperty($property, $lang);

        @endphp

        @if($property->field_type == 'checkbox')

            @if($value == 1)
                <span class="text-bold">{{Translation::translateProperty($property)}}</span>
            {{--<span>{{Auth::user()->getValueProperty($property, $lang)}}</span>--}}
            @endif
        @elseif($property->field_type == 'mselect')

                <span class="text-bold">{{Translation::translateProperty($property)}}</span>

                @foreach( Auth::user()->getValueProperties($property, $lang) as $item)
                    <span>{{$item->translation}}</span>
                @endforeach

        @else
            <span class="text-bold">{{Translation::translateProperty($property)}}</span>
            <span>{{Auth::user()->getValueProperty($property, $lang)}}</span>
        @endif

    @empty
    @endforelse

</p>