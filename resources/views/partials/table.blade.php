<table>

    <tbody>
    @forelse ($properties as $property)
        @php

            $value = Auth::user()->getValueProperty($property, $lang);

        @endphp

        @if($property->field_type == 'checkbox')
            @if($value == 1)
                <tr>
                    <td class="text-bold">{{Translation::translateProperty($property)}}</td>
                </tr>
            @endif
        @elseif($property->field_type == 'mselect')
            <tr>
                <td class="text-bold">{{Translation::translateProperty($property)}}</td>

                @foreach( Auth::user()->getValueProperties($property, $lang) as $item)
                    <td>{{$item->translation}}</td>
                @endforeach
            </tr>
        @else
            <tr>
                <td class="text-bold">{{Translation::translateProperty($property)}}</td>
                <td>{{Auth::user()->getValueProperty($property, $lang)}}</td>
            </tr>
        @endif

    @empty
    @endforelse

    </tbody>
</table>