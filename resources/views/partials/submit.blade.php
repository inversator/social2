<div class="form-group">
    <div class="col-md-2"></div>

    <div class="col-md-10">
        <button type="submit" class="btn btn-info">
            {{ Translation::translateKey($title) }}
        </button>
    </div>
</div>
