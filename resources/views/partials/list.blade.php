<ul>

    <tbody>
    @forelse ($properties as $property)

        @php
            $value = Auth::user()->getValueProperty($property, $lang);
        @endphp

        @if($property->field_type == 'checkbox')
            @if($value == 1)
                <li>
                    <span class="text-bold">{{Translation::translateProperty($property)}}</span>
                </li>
            @endif
        @elseif($property->field_type == 'mselect')
            <li>
                <span class="text-bold">{{Translation::translateProperty($property)}}</span>

                @foreach( Auth::user()->getValueProperties($property, $lang) as $item)
                    <span>{{$item->translation}}</span>
                @endforeach
            </li>
        @else
            <li>
                <span class="text-bold">{{Translation::translateProperty($property)}}</span>
                <span>{{Auth::user()->getValueProperty($property, $lang)}}</span>
            </li>
        @endif

    @empty
    @endforelse

    </tbody>
</ul>