@php
    $value = old($name) ? old($name) : (isset($value) ? $value : null);
    $errorKey = preg_replace('/\[/', '.', $name);
    $errorKey = preg_replace('/\]\./', '.', $errorKey);
    $errorKey = preg_replace('/\]/', '', $errorKey);
    $class = "form-control";
    $label = isset($label) ? $label : '';

@endphp


<div class="form-group{{ $errors->has($errorKey) ? ' has-error' : '' }}">
    <label for="{{ $name }}" class="col-md-2 control-label">{{ $label }}</label>

    <div class="col-md-6 col-sm-9 col-xs-12">
        <select name="{{ $name }}[]" class="{{ $class }}" multiple>

            @foreach ($options as $key => $option)
                @php
                    $selected =  old($name) != null ? old($name) : $selectedItem;
                    if (!$selected) {
                        $selected = isset($value) ? $value : null;
                    }
                    $optionId = is_array($options) ? $key : $option->id;
                    $optionValue = is_array($options) ? $option : $option->name;

                        $isSelected = "";
                        if ($selected) {
                            $selected = $selected->pluck('id')->toArray();
                            if (in_array($optionId, $selected)) $isSelected = "selected";
                        }

                @endphp
                <option value="{{ $optionId }}" {{ $isSelected }}>{{ $optionValue }}</option>
            @endforeach
        </select>
        @if ($errors->has($errorKey))
            <span class="help-block">
                <strong>{{ $errors->first($errorKey) }}</strong>
            </span>
        @endif
    </div>
</div>
