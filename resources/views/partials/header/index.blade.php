<div class="g-header">
    <div class="container">
        <div class="g-header__top">
            <a href="index.html"><img src="img/logo.png" alt="поиск содержанок и спонсоров"></a>
            <nav class="g-header__nav">
                <ul>
                    <li><a href="#" title="">войти</a></li>
                    <li><a class="btn-link" href="#" title="">Регистрация</a></li>
                </ul>
            </nav>
        </div>
        <div class="g-header__bottom">
            <div class="g-header__bottom-info">
                <h1 class="text-line">Знакомства <br>
                    для успешных мужчин</h1>
                <div class="title">Sweet life – делаем расчет
                    <br> в отношениях выгодным
                </div>
            </div>
            <div class="form-wrap">
                <div class="b-form form">
                    <div class="b-form__bg"></div>
                    <form>
                        <div class="top">
                            <div class="title">Найдите свою
                                <br> вторую половинку</div>
                            <div class="form-group">
                                <label>Кого вы ищете</label>
                                <select class="select-custom" placeholder="Девушку">
                                    <option value="0">Девушку</option>
                                    <option value="1">Парня</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Введите город</label>
                                <input type="text" class="form-control" placeholder="Город">
                            </div>
                            <div class="form-group form-group__age">
                                <label>Выберите возраст</label>
                                <input class="age-slider" type="slider">
                            </div>
                            <div class="btn-wrap">
                                <button type="submit" class="btn-link btn-link__accent">Искать</button>
                            </div>
                        </div>
                        <div class="bottom">
                            <label>
                                <input type="checkbox" checked="">
                                <span class="icon"></span>
                                <span class="checkbox__title">Запомнить мой выбор</span>
                            </label>
                            <a href="#" class="checkbox__help fa fa-question-circle"></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>