<header class="header">
    <div class="header__top-wrap">
        <div class="container">
            <div class="header__top">
                <a class="logo" href="index.html"><img src="img/logo-clean.png" alt="поиск содержанок и спонсоров"></a>
                <nav class="header__nav">
                    <ul>
                        <li><a href="#" title="">выход <i class="icon icon-out"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="header__bottom-wrap">
        <div class="container">
            <div class="header__bottom">
                <div class="mob-nav visible-xs visible-sm">
                    <a class="fa fa-bars" href="#menu"></a>
                    <nav class="header__menu" id="menu">
                        <ul>
                            <li><a href="" title="">Поиск</a></li>
                            <li><a href="" title="">Кто смотрел</a></li>
                            <li><a href="" title="">Рассылка</a></li>
                            <li><a href="" title="">Сообщения</a></li>
                        </ul>
                    </nav>
                </div>
                <nav class="header__menu hidden-xs hidden-sm" id="menu">
                    <ul>
                        <li><a href="" title="">Поиск</a></li>
                        <li><a href="" title="">Кто смотрел</a></li>
                        <li><a href="" title="">Рассылка</a></li>
                        @if (Auth::user())
                            <li><a href="{{ route("profile.index") }}">Профиль</a></li>
                            <li><a href="{{ route("chat") }}">Сообщения</a></li>
                        @endif
                    </ul>
                    {{--<ul class="nav navbar-nav navbar-right">--}}
                    {{--<select id='language'>--}}
                    {{--@foreach ($languages as $l)--}}
                    {{--<option value="{{ $l->code }}" {{ $language == $l->code ? "selected" : "" }}>{{ $l->code }}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                    {{--@if (Auth::user())--}}
                    {{--<li>--}}
                    {{--<form method='POST' action='{{ route('auth.logout') }}'>--}}
                    {{--{{ csrf_field() }}--}}
                    {{--<input type="submit" value="{{ Translation::translateKey('LOGOUT') }}"><span--}}
                    {{--class="glyphicon glyphicon-log-in"></span> </input>--}}
                    {{--</form>--}}
                    {{--</li>--}}
                    {{--@else--}}
                    {{--<li><a href="{{ route('auth.login') }}"><span--}}
                    {{--class="glyphicon glyphicon-log-out"></span> {{ Translation::translateKey('LOGIN') }}--}}
                    {{--</a></li>--}}
                    {{--@endif--}}
                    {{--</ul>--}}
                </nav>
                <nav class="header__action-panel">
                    @if(Auth::check())
                        <ul>
                            <li><a href="" title="">Счет: <span>100.00</span> руб.</a></li>
                            <li class="avatar__wrap"><a href="" title="">
							<span class="avatar" style="background-image: url({{Auth::user()->getMainPhoto()}})">
							{{--<img src="{{Auth::user()->getMainPhoto()}}" alt="">--}}
                            </span>
                                    <span class="username">{{Auth::user()->name}}</span>
                                </a></li>
                            <li><a class="btn-buy" href="" title="">Купить премиум</a></li>
                        </ul>
                    @endif
                </nav>
            </div>
        </div>
    </div>
</header>