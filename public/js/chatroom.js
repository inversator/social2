(function ($) {
    $.fn.chatRoom = function (arg) {

        var textArea = $('#enterMessage').children('input');
        var chatWindow = this.children('ul');
        var tapStatus = this.children('#tapStatus');

        var channel = $('.chatPipe');
        var sSaver = $('#sSaver');

        var userName = arg.name;
        var userType = arg.userType;
        var userId = arg.userId;

        var authorId;
        var side = 'in';

        var pipeMessageCount = 0;

        var pipe, pipe_type, user_to, pipesUnread;

        function soundOn(type) {

            var audio = new Audio();

            if (type = 'message') {
                audio.src = '/sounds/onmessage.mp3';
            }

            audio.autoplay = true;
        }

        function cropTextMore(text, more) {
            return (text.length > more) ? text.substr(0, more - 4) + '...' : text;
        }

        function getTimeStamp(date) {
            var myDate = new Date(date);
            return myDate.getTime() / 1000.0;
        }

        function getDateFromDB(date) {
            var t = date.split(/[- :]/);
            return new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));
        }

        function sendToServer(data, sess) {
            sess.publish(pipe, data);
        }

        function reconnecting(sess) {
            sess.unsubscribe(pipe);

            sendToServer(
                {
                    'pipe': pipe,
                    'type': 6,
                    'user_id': userId,
                },
                sess
            );

        }

        function liftBlock(response) {

            if (response.type == 0) {
                var chatBlock = $('.contacts-list li[data-pipe="' + response.pipe + '"]');
            } else {
                var chatBlock = $('.contacts-list li[data-pipe="' + response.topic_id + '"]');
            }

            var chatListBlock = chatBlock.parent('ul');
            $(chatBlock[0]).prependTo($(chatListBlock[0]));

        }

        function insertPipeMessages(response) {

            var topicMessages = response.topicMessages;
            var html = '';
            var author = '';
            var bordered = '';
            var online;
            var messageUserId = 0;

            pipeMessageCount = 0;

            if (topicMessages.length) {

                sSaver.hide();

                topicMessages.forEach(function (item, i, topicMessages) {

                    pipeMessageCount++;

                    if (i != 0)
                        if (authorId != item.user_id) {
                            side = (side == 'in') ? 'out' : 'in';
                            bordered = 'bordered';
                        } else {
                            bordered = '';
                        }

                    messageUserId = (item.user_id) ? item.user_id : 0;

                    // if (item.last_visit) online = !(($.now() / 1000) - getTimeStamp(item.last_visit) > 2 * 60);
                    online = item.is_online;
                    var greenLight = ('client' == item.user_type || online) ? '<i class="status status-online"></i>' : '<i style="display: none" class="status status-online"></i>';

                    html += '<li class="' + side + ' ' + bordered + '" data-chat-user="' + messageUserId + '" data-message-id="' + item.id + '">';

                    html += '<div class="avatar">';


                    if (item.thumbnail) {
                        html += '<img alt="" src="' + item.thumbnail + '">';
                    } else {
                        html += '<img alt="" src="https://cdn3.iconfinder.com/data/icons/user-2/100/9-512.png">';
                    }

                    author = item.first_name + ' ' + item.second_name;

                    html += '</div>';

                    html +=
                        '<div class="message">' +
                        '<div class="top">' +
                        '<div class="name">'
                        + greenLight + '' +
                        '<span> ' + author + ' </span>' +
                        '</div>' +
                        '<span class="datetime">' + moment(item.created_at).calendar() + '</span>';
                    if (item.user_id == userId) {
                        html += '<div class="edit dropdown">' +
                            '<a href="#" class="dropdown-toggle fa fa-ellipsis-h" data-toggle="dropdown" aria-expanded="true"></a>' +
                            '<ul class="dropdown-menu message-edit">' +
                            '<li><a ' +
                            'href="#" ' +
                            'data-toggle="modal" ' +
                            'data-target="#delChatMessage">Удалить</a></li>' +
                            '<li><a href="#" onclick="return editChatMessage(' + item.id + ')">Изменить</a></li>' +
                            '</ul>' +
                            '</div>';
                    }

                    html += '</div>';

                    html += '<div class="text"><span class="inner">' + item.message + '</span></div>';


                    html += '</div></li>';


                    authorId = item.user_id;

                });
                if ('client' == userType || 'private' == userType) {
                    chatWindow.show();
                    chatWindow.html(html);
                } else {
                    chatWindow.html(html);
                }

            } else {
                chatWindow.html(html);
                sSaver.text(' — Нет сообщений — ').show();
            }


            if ($(".message-content__inner").length) {
                $(".message-content__inner").mCustomScrollbar("scrollTo", "bottom");
            } else {
                setTimeout(function () {
                    chatWindow.scrollTop(9999);
                }, 300);
            }

            $('ul.list li').Emoji();
        }

        function onMessage(response) {

            var cm = $(channel).find('.text span:first-child');
            var acm = parseInt(cm.text());
            cm.text(++acm);

            pipeMessageCount++;

            var html = '';
            var bordered = '';

            author = response.from;

            if (authorId != response.user_id) {
                side = (side == 'in') ? 'out' : 'in';
                bordered = " bordered";
            } else {
                bordered = '';
            }

            html += '<li class="' + side + bordered + ' online" data-chat-user="' + response.user_id + '" data-message-id="' + response.message_id + '">';
            if (authorId != response.user_id) {
            }
            html += '<div class="avatar">';


            // If isset img for avatar
            if (response.user_img) {
                html += '<img alt="" src="' + response.user_img.thumbnail + '">';
            } else {
                html += '<img alt="" src="https://cdn3.iconfinder.com/data/icons/user-2/100/9-512.png">';
            }

            html += '</div>';

            html +=
                '<div class="message">' +
                '<div class="top">' +
                '<div class="name">' +
                '<i class="status status-online"></i>' +
                '<span>' + author + '</span>' +
                '</div>' +
                '<span class="datetime">' + moment().calendar() + '</span>' +
                '</div>';
            if (response.user_id == userId) {
                html += '<div class="edit dropdown">' +
                    '<a href="#" class="dropdown-toggle  fa fa-ellipsis-h" data-toggle="dropdown" aria-expanded="true"></a>' +
                    '<ul class="dropdown-menu message-edit">' +
                    '<li><a ' +
                    'href="#" ' +
                    'data-toggle="modal" ' +
                    'data-target="#delChatMessage">Удалить</a></li>' +
                    '<li><a href="#" onclick="return editChatMessage(' + response.message_id + ')">Изменить</a></li>' +
                    '</ul></div>';
            }
            html += '<div class="text"><span class="inner">' + response.message + '</span></div>' +
                '</div>';

            html += '</li>';

            if (pipe_type != 'indi' && pipe_type != 'company') liftBlock(response);

            authorId = response.user_id;

            sSaver.hide();
            chatWindow.show();
            chatWindow.append(html);

            if ($(".message-content__inner").length) {
                $(".message-content__inner").mCustomScrollbar("scrollTo", "bottom");
            } else {
                chatWindow.scrollTop(9999);
            }
            $('li[data-message-id=' + response.message_id + ']').Emoji();
            soundOn('message');
        }

        function onTyping(response) {


            var html = '';

            var res = $('p').is('#typ-' + response.id);

            if (!res) {
                html += "<p id='typ-" + response.id + "'>" + response.from + " печатает ...</p>";
                tapStatus.append(html);
                tapStatus.show();
                typeTimer = setTimeout(clearTypingMessage, 1000);
            } else {
                clearTimeout(typeTimer);
                typeTimer = setTimeout(clearTypingMessage, 1000);
            }

            function clearTypingMessage() {
                $('#typ-' + response.id).remove();
                tapStatus.hide();
            }
        }

        function unreadMessages(block) {

            $(block).find('.badge').hide();

            pipesUnread = JSON.parse(localStorage.getItem('userPipes'));

            if (pipesUnread[pipe] !== undefined) {

                var colUnreadCat = 0;
                var unreadPipeType = pipe_type;

                if (unreadPipeType == 'company') {

                    unreadPipeType = 'indi';

                    colUnreadCat = pipesUnread['indi'] - pipesUnread[pipe].count;

                    if (colUnreadCat > 0) {
                        pipesUnread['indi'] = colUnreadCat;
                    } else {
                        pipesUnread['indi'] = 0;
                    }

                } else {
                    colUnreadCat = pipesUnread[unreadPipeType] - pipesUnread[pipe].count;

                    if (colUnreadCat > 0) {
                        pipesUnread[unreadPipeType] = colUnreadCat;
                    } else {
                        pipesUnread[unreadPipeType] = 0;
                    }
                }

                pipesUnread[pipe].count = 0;


                if (pipesUnread[unreadPipeType])
                    $(block).parents('.category__inner').find('.category-heading .badge').text(pipesUnread[unreadPipeType]).show();
                else
                    $(block).parents('.category__inner').find('.category-heading .badge').text(pipesUnread[unreadPipeType]).hide();
                localStorage.setItem('userPipes', JSON.stringify(pipesUnread));
            }

        }

        function addUnread(response) {

            if (response.pipe != pipe && response.user_type != userType) {

                var pipeBlock = $('li[data-pipe="' + response.pipe + '"]');

                if (pipeBlock.length) pipeBlock = pipeBlock[0]

                if (pipeBlock) {

                    if (response.pipe_type == 'company') response.pipe_type = 'indi';

                    pipesUnread = JSON.parse(localStorage.getItem('userPipes'));

                    if (pipesUnread[response.pipe]) {

                        pipesUnread[response.pipe].count = pipesUnread[response.pipe].count + 1;
                        pipesUnread[response.pipe_type]++;

                    } else {
                        pipesUnread[response.pipe] = {count: 1};
                        pipesUnread[response.pipe_type]++;
                    }


                    // Add 1 unread bandge

                    if (pipesUnread[response.pipe].count) {
                        $(pipeBlock).find('.badge').text(pipesUnread[response.pipe].count).show();
                    } else {
                        $(pipeBlock).find('.badge').text(0).hide();
                    }

                    // Chat Category unread

                    var catBadge = $(pipeBlock).parents('.category__inner').find('.category-heading .badge');
                    if (catBadge) catBadge = catBadge[0];

                    if (pipesUnread[response.pipe_type]) {
                        $(catBadge).text(pipesUnread[response.pipe_type]).show();
                    } else {
                        $(catBadge).text(0).hide();
                    }
                    localStorage.setItem('userPipes', JSON.stringify(pipesUnread));

                    soundOn('message');
                }
            }
        }

        function initUser(sess) {

            sess.publish('aW5pdGlhbGlzYXRpb24=', {
                'user_id': userId,
                'type': 0,
                'user_type': userType,
            });

            sess.subscribe('user' + userId + 'pipe', function (topic, response) {

                switch (response.type) {
                    case 0:
                        addUnread(response);
                        if (response.pipe_type != 'indi' && response.pipe_type != 'company') liftBlock(response);
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            });
        }

        var conn = new ab.connect(
            'ws://localhost:8383'
            , function (session) {

                console.log('Connection established.');
                sess = session;

                conn.onerror = function (e) {
                    console.log('WebSocket error: ' + e.data);
                };

                initUser(sess);

                channel.on('click', function () {

                    textArea.show(1000);

                    if (pipe != $(this).attr('data-pipe')) {

                        $('li[data-pipe="' + pipe + '"').removeClass('pipeOn');
                        $(this).addClass('pipeOn');

                        // Reconnecting

                        if (pipe) {
                            reconnecting(sess);
                        }

                        pipe = $(this).attr('data-pipe');
                        user_to = $(this).attr('data-chat-user');
                        pipe_type = $(this).attr('data-pipe-type');

                        if (pipeMessageCount > 0) {
                            sendToServer(
                                {
                                    'pipe': pipe,
                                    'type': 7,
                                    'user_id': userId,
                                    'pipe_type': pipe_type
                                },
                                sess
                            );
                        }

                        console.info('connect to ' + pipe);
                        sess.subscribe(pipe, function (topic, response) {

                            // Event type
                            switch (response.type) {

                                case 0:
                                    //addUnread(response);
                                    break;

                                // send message in pipe (concrentno this pipe)
                                case 1:
                                    if (pipeMessageCount == 0) {
                                        if (pipe_type == 'private') {
                                            sendToServer(
                                                {
                                                    'pipe': pipe,
                                                    'type': 7,
                                                    'user_id': userId,
                                                    'pipe_type': 'private',
                                                    'user_to': user_to
                                                },
                                                sess
                                            );
                                        } else {
                                            sendToServer(
                                                {
                                                    'pipe': pipe,
                                                    'type': 7,
                                                    'user_id': userId,
                                                    'pipe_type': pipe_type
                                                },
                                                sess
                                            );
                                        }

                                    }
                                    onMessage(response);

                                    break;

                                // User typing
                                case 2:

                                    onTyping(response);
                                    break;

                                // Get all pipe message
                                case 3:
                                    insertPipeMessages(response);
                                    break;

                                case 4:
                                    $("[data-chat-user=" + response.user_id + "]").find('.status').removeClass('status-ofline');
                                    $("[data-chat-user=" + response.user_id + "]").find('.status').addClass('status-online');
                                    break;
                                case 5:
                                    $("[data-chat-user=" + response.user_id + "]").find('.status').removeClass('status-online');
                                    $("[data-chat-user=" + response.user_id + "]").find('.status').addClass('status-ofline');
                                    break;
                                default:
                                    console.log('default');
                                    break;
                            }

                        });

                        unreadMessages(this);

                    }
                });
                $('.chat__top li:nth-child(1)').on('click', function () {

                    $('li.chatPipe').show();
                });
                $('.chat__top li:nth-child(2)').on('click', function () {
                    $('li.chatPipe').show();
                    $('li').find('.badge:contains(0)').each(function (index) {
                        $(this).closest('li').hide();
                    });
                });
                $('.chat__top li:nth-child(3)').on('click', function () {
                    $('li.chatPipe').show();
                    $('li').find('span.fa-star:not(.checked)').each(function (index) {
                        $(this).closest('li').hide();
                    });
                });
                $('.chat__top li:nth-child(4)').on('click', function () {
                    $('li.chatPipe').show();
                    $('li.chatPipe:not([data-pipe-status=blacklist])').each(function (index) {
                        $(this).hide();
                    });
                });
                $('span.fa-star').on('click', function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    if ($(this).hasClass('checked')) {
                        $.ajax({
                            data: {
                                status: '0',
                                user_id: userId,
                                pipe: pipe,
                            },
                            url: '/messages/favorite/',
                        });
                    } else {
                        $.ajax({
                            data: {
                                status: 'favorite',
                                user_id: userId,
                                pipe: pipe,
                            },
                            url: '/messages/favorite/',
                        });
                    }

                });


                $('.send-panel button').on('click', function () {
                    sendToServer({
                            'message': $(textArea).val(),
                            'name': userName,
                            'user_id': userId,
                            'type': 1,
                            'user_type': userType,
                            'pipe_type': pipe_type
                        }, sess
                    );
                    textArea.val('');
                })

                $('#uploadImage .upload').on('click', function () {

                    file = $('#uploadImage input[type=file]').prop('files')[0];

                    var fd = new FormData;
                    fd.append('img', file);

                    $.ajax({
                        url: '/messages/upload',
                        type: "POST",
                        data: fd,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            if (pipe) {
                                sendToServer({
                                        'message': "<img src='" + response + "' />",
                                        'name': userName,
                                        'user_id': userId,
                                        'type': 1,
                                        'user_type': userType,
                                        'pipe_type': pipe_type
                                    }, sess
                                );
                            }
                            $('#uploadImage').modal('hide');
                        }
                    });
                });

                textArea.keypress(function (e) {

                    if (e.keyCode == 13 && $(textArea).val()) {
                        sendToServer({
                                'message': $(textArea).val(),
                                'name': userName,
                                'user_id': userId,
                                'type': 1,
                                'user_type': userType,
                                'pipe_type': pipe_type
                            }, sess
                        );
                        textArea.val('');

                        //textArea.blur(); // lost focus
                    } else {

                        sendToServer({
                                'message': '',
                                'name': userName,
                                'type': 2,
                                'user_id': userId,
                                'user_type': userType
                            }, sess
                        );
                    }
                });
            }

            // If connection lost
            , function (code, reason) {

                if (pipe) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '/messages/update/' + userId + '/' + pipe,
                    });
                }

                console.warn('WebSocket connection closed');

                console.info("Code:", code, "Reason:", reason);
            },
            {
                'skipSubprotocolCheck': true,
                'exclude_me': true
            }
        );

    };

})(jQuery);

$('#delChatMessage').on('show.bs.modal', function (e) {
    var li = $(e.relatedTarget).closest('li[data-message-id]')[0];

    var textBlock = $(li).find('.text')[0];
    var idMessage = $(li).attr('data-message-id');

    $('#modalMessage').html('<blockquote class="blockquote-reverse"><p>' + $(textBlock).text() + '</p></blockquote>');
    $('#delButton').on('click', function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/messages/delete',
            data: {'id': idMessage},
            method: 'POST',
            success: function (data) {
                if (data) {
                    $(li).remove();
                }
            }
        });
        $('#delChatMessage').modal('hide');
        $('#delButton').off('click');
    });
});

$(function () {
    $('#emojiList .item').Emoji();
});

$('#emojiList .item').on('click', function () {

    var emoji = $(this).find('img').attr('alt');
    var input = $('#enterMessage input');

    input.val(input.val() + ' ' + emoji + ' ');

})

function editChatMessage(id) {
    var liBlock = $('li[data-message-id=' + id + ']');
    var textBlock = liBlock.find('.message span.inner');

    var oldText = textBlock.html();


    textBlock.html('<textarea contenteditable="true" style="width: 100%">' + oldText.replace(/<img.*?alt="(.*?)".*>/g, '$1'.valueOf()) + '</textarea>' +
        '<p><a id="escEdit" href="#">отмена</a> | <a id="saveEdit" href="#">сохранить</a></p>');

    function editMessage(idMessage, text) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/messages/edit',
            data: {'id': idMessage, text: text},
            method: 'POST',
            success: function (data) {
                if (text) {
                    textBlock.html(text);
                }
            }
        });
    }

    liBlock.find('#escEdit').on('click', function () {
        textBlock.html(oldText);
        return false;
    });
    liBlock.find('#saveEdit').on('click', function () {
        editMessage(id, textBlock.children('textarea').val());
        return false;
    });
    textBlock.children('textarea').on('keypress', function (e) {
        if (e.keyCode == 13) {
            editMessage(id, textBlock.children('textarea').val());


        }
    });

    return false;
}
