var gliss = gliss || {};

gliss.index = (function($){

    this.subscribe = function(){

        console.log('subscribe');

        event = window.event || {};
        event.preventDefault();
        event.stopPropagation();


        gliss.modal.showModal('Подписка успешно оформлена');
        return;


        var form = $(el).closest('form');

        var text = form.find('textarea').val();

        if((!text || text.replace(/^\s+|\s+$/g, '') == '')){return};
        var formData = form.serialize() + '&to='+uid;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/profile/edit',
            method: 'post',
            data: formData,
            dataType : 'json'
        })
            .done(function(response){

                form.find('textarea').val('');
                gliss.profile.closeModal();

                if (response.status == 'success') {

                    alert('Сообщение отправлено!');
                }


                if (response.status == 'fail') {
                    alert('Невозможно отправить сообщение этому пользователю. Попробуйте позже');
                }
            })
            .fail(function(data){

                alert('Невозможно отправить сообщение');

            });

    }

    return this;

}).call(gliss.modal || {}, jQuery);