var gliss = gliss || {};

gliss.profile = (function($){

    this.init = function() {

        $(document).click(function (event) {

            if (!$(event.target).closest(".edit-block-to-show").length) {
                $('.edit-block-to-show').hide();
                $('.edit-block-to-hide').show();
            }

            if ($(event.target).closest(".edit-block").length) {

                $(event.target).closest('.edit-block').find(".edit-block-to-hide").hide();
                $(event.target).closest('.edit-block').find(".edit-block-to-show").show();

                event.stopPropagation();
            }

        });
    };

    this.addFavorite = function(elm, uid){

        var favorite = $(elm).attr('data-favorite');

        console.log(favorite);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            'url': '/profile/edit',
            'method': 'POST',
            'dataType': 'json',
            'data': {
                action: 'change-status',
                status: favorite == 0 ? 'favorite' : 0,
                to: uid
            },
            error: function(jqXHR, textStatus, errorThrown) {
                gliss.modal.showModal('Невозможно выполнить операцию. Попробуйте еще раз');
            },
            success: function(data){

                if(data.status == 'success') {

                    text = '';

                    if (favorite == 'favorite'){
                        text = 'Пользователь удален из избранного';
                    } else {
                        text = 'Пользователь добавлен в избранное';
                    }

                    $('.btn-ignore').removeClass('block');
                    $('.btn-favorite').toggleClass('favorite').attr('data-favorite', favorite == 0 ? 'favorite' : 0);
                    gliss.modal.showModal(text);

                } else {
                    gliss.modal.showModal('Невозможно выполнить операцию. Попробуйте еще раз');
                }
            }
        })

    }


    this.addBlock = function(elm, uid){

        var block = $(elm).attr('data-block');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            'url': '/profile/edit',
            'method': 'POST',
            'dataType': 'json',
            'data': {
                action: 'change-status',
                status: block == 0 ? 'block' : 0,
                to: uid
            },
            error: function(jqXHR, textStatus, errorThrown) {
                gliss.modal.showModal('Невозможно выполнить операцию. Попробуйте еще раз');
            },
            success: function(data){

                if(data.status == 'success') {

                    text = '';

                    if (block == 'block'){
                        text = 'Пользователь разблокирован';
                    } else {
                        text = 'Пользователь заблокирован';
                    }

                    $('.btn-favorite').removeClass('favorite');
                    $('.btn-ignore').toggleClass('block').attr('data-block', block == 0 ? 'block' : 0);
                    gliss.modal.showModal(text);

                } else{
                    gliss.modal.showModal('Невозможно выполнить операцию. Попробуйте еще раз');
                }
            }
        })

    }

    this.quickMessage =  function(uid){

        $('#m_overlay').fadeIn(400,
            function(){
                $('#m_modal_form')
                    .css('display', 'block')
                    .animate({opacity: 1, top: '50%'}, 200);
            });

    };

    this.closeModal = function(){
        $('#m_modal_form')
            .animate({opacity: 0, top: '45%'}, 200,
                function(){
                    $(this).css('display', 'none');
                    $('#m_overlay').fadeOut(400);
                }
            );
    };

    this.sendQuickMessage = function(event, el, uid){


        event = window.event || {};
        event.preventDefault();
        event.stopPropagation();

        var form = $(el).closest('form');

        var text = form.find('textarea').val();

        if((!text || text.replace(/^\s+|\s+$/g, '') == '')){return};
        var formData = form.serialize() + '&to='+uid;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/profile/edit',
            method: 'post',
            data: formData,
            dataType : 'json'
        })
            .done(function(response){

                form.find('textarea').val('');
                gliss.profile.closeModal();

                if (response.status == 'success') {

                    gliss.modal.showModal('Сообщение отправлено!');
                }


                if (response.status == 'fail') {
                    gliss.modal.showModal('Невозможно отправить сообщение этому пользователю. Попробуйте позже');
                }
            })
            .fail(function(data){

                gliss.modal.showModal('Невозможно отправить сообщение');

            });

    };

    /**
     * Редактирование описания
     */
    $('#form-description').submit(function(event){
        event.preventDefault();

        var form = $(this);
        var formData = new FormData(form[0]);

        $.ajax({
            'url': form.attr("action"),
            'method': 'POST',
            'dataType': 'json',
            'data': formData,
            contentType: false,
            processData: false,
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                // showError(form);
            },
            success: function(data){
                console.log(data);

                if(data.status == 'success') {

                    // hideError();

                    // data.moderation == 1 && $('.warn-block').show();
                    // data.moderation == 1 && $('.danger-block').hide();

                    form.closest('.edit-form-hidden').hide();
                    $('.text-about').text(data.description).show();
                    $('.btn-edit').show();

                    // $('.user-description').show();

                } else {
                    console.log(data.error);
                    // showError(form, data.error);
                }


            }
        })

    });

    /**
     * Редактирование описания
     */
    $('#form-description').submit(function(event){
        event.preventDefault();

        var form = $(this);
        var formData = new FormData(form[0]);

        $.ajax({
            'url': form.attr("action"),
            'method': 'POST',
            'dataType': 'json',
            'data': formData,
            contentType: false,
            processData: false,
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                // showError(form);
            },
            success: function(data){
                console.log(data);

                if(data.status == 'success') {

                    // hideError();

                    // data.moderation == 1 && $('.warn-block').show();
                    // data.moderation == 1 && $('.danger-block').hide();

                    form.closest('.edit-form-hidden').hide();
                    $('.text-about').text(data.description).show();
                    $('.btn-edit').show();

                    // $('.user-description').show();

                } else {
                    console.log(data.error);
                    // showError(form, data.error);
                }


            }
        })

    });

    $('.properties-edit').submit(function(event){

        event.preventDefault();

        var form = $(this);
        var formData = new FormData(form[0]);


        var checkboks = form.find('input[type=checkbox]');

        checkboks.each(function(i, el){

            if( !$(el).is(':checked')){
                formData.set($(this).attr('name'), 0);
            }
        })

        $.ajax({
            'url': form.attr("action"),
            'method': 'POST',
            'data': formData,
            contentType: false,
            processData: false,
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                // showError(form);
            },
            success: function(data){

                // hideError();

                form.closest('.edit-form-hidden').hide();
                form.prev('.properties-show').empty().append(data).show();
                $('.btn-edit').show();


            }
        })

    });



    return this;

}).call(gliss.profile || {}, jQuery);
