var gliss = gliss || {};

gliss.modal = (function($){



    this.showModal = function(text){

        console.log('show modal');

        var textMessage = '';

        if (typeof text == 'object'){
            for (var i in text){
                textMessage += '<span>'+text[i]+'</span><br>';
            }
        }else {
            textMessage = text;
        }

        var html = '<div class="popup settings-saved" id="modal_form">';
        // html += '<span onClick="return gliss.modal.closeModal()" id="modal_close">X</span>';
        html += '<div class="quick-message">';
        html += '<div class="quick-message-header"><span></div>';
        html += '<div class="quick-message-body"><div class="user-info">';
        html += '<p>'+textMessage+'</p><button type="submit" class=" btn-send"><span  onclick="return gliss.modal.closeModal()" class="btn__text">OK</span></button></div></div>';

        html += '</div></div>';

        $('#wrap-bg').length == 0 && $('body').append('<div id="wrap-bg">'+html+'<div id="overlay"></div></div>');

        $('#overlay').append(html).fadeIn(400,
            function(){
                $('#modal_form')
                    .css('display', 'block')
                    .animate({opacity: 1, top: '50%'}, 200);
            }).click(function(){return gliss.modal.closeModal()});

    };



    this.closeModal = function(){

        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200,
                function(){
                    $(this).css('display', 'none');
                    $('#wrap-bg').remove();
                }
            );

    };

    return this;

}).call(gliss.modal || {}, jQuery);