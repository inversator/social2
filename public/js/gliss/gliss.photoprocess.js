var gliss = gliss || {};

gliss.photoprocess = (function($){

    //todo убрать и поменять на modal
    showError = function(form, errorTex){

        var error = errorTex || 'Произошла ошибка';

        form.find('.form-row__text-error')
            .text(errorTex)
            .css({'visibility': 'visible', 'opacity': 1});

    },

    makeMain = function(id){

            $.ajax({
                url: '/photos/makemain/'+id,
                type: 'post',
                data: {'_token': $('meta[name="_token"]').attr('content')},
                error: function(){
                    console.log('Произошла ошибка');
                },
                success: function (data) {

                    if (data.status == 'ok') {

                        console.log('success');

                        $('.photo-list__item').removeClass('main');
                        $('.photo-list__main').remove();

                        $('#user-photo-'+id).addClass('main');
                        $('#user-photo-'+id+' .photo-list__item-inner').prepend('<div class="photo-list__main">Основное фото</div>');


                    } else {
                        console.log(data.error || 'Произошла ошибка');
                    }
                }
            })

        };

    this.angle = 0;
    //поворачиваем картинку
    this.rotateImg = function(angle){

        gliss.photoprocess.angle = parseInt(gliss.photoprocess.angle) + parseInt(angle);

        $('#rotate_angle').val(gliss.photoprocess.angle);

        $("#img-preview").css({
            transform: "rotate(" + gliss.photoprocess.angle + "deg)"
        });

    };

    this.edit = function(){

        var form = $('#profile-photo-edit');

        $.ajax({
            url: form.attr("action"),
            method: 'POST',
            dataType: 'json',
            data: form.serialize(),
            error: function (jqXHR, textStatus, errorThrown) {
                // console.log(jqXHR);

                if (jqXHR.status == 422) {

                    var errors = jqXHR.responseJSON;

                    //todo поменять на modal
                    showError(form, errors['photo']);

                }
            },
            success: function (data) {
                console.log('success');
                $('#rotate_angle').val(0);
                gliss.modal.showModal('Фото успешно отредактировано!');
            }
        })



    };

        $('.form-remove-photo').submit(function(e){

            var form = $(this);

            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                data: $(this).serialize(),
                error: function(){
                    showError(form, data.error);
                },
                success: function (data) {

                    if (data.status == 'ok') {
                        $('#user-photo-'+data.id).remove();

                    } else {

                        showError(form, data.error);
                    }
                }
            })


        });

        $('#photo').change(function () {

            var file_data = $('#photo').prop('files')[0];

            var form = $('#profile-photo-upload');
            var formData = new FormData(form[0]);

                $.ajax({
                    url: $('#profile-photo-upload').attr("action"),
                    method: 'POST',
                    dataType: 'json',
                    data: formData,
                    contentType: false,
                    processData: false,
                    error: function(jqXHR, textStatus, errorThrown) {
                        // console.log(jqXHR);

                        if (jqXHR.status == 422) {

                            var errors = jqXHR.responseJSON;

                            showError(form, errors['photo']);

                        }
                    },
                    success: function(data){
                        console.log(data);

                        if(data.status == 'ok') {

                            var str= '<a id="'+data.id+'" data-position="'+data.order+'" class="profile-photo-thumb" href="'+data.name+'" style="background-image: url('+data.name+')"></a>';

                            $('.no-photo').hide();


                            data.main && $('.image').css({
                                'background-image': 'url('+data.main+')',
                            });

                            data.avatar && $('.header__action-panel .avatar').css({
                                'background-image': 'url('+data.avatar+')',
                            })

                            $('.sortable-photos').append(str);

                        } else {
                            alert(data.error);
                        }
                    }
                })



            // });
        })

        this.currentId = null;
        this.startPosition = null;
        this.endPosition = null;

        this.changePriorityPhoto = function(){


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({
                url: '/profile/edit',
                dataType: 'json',
                type: 'post',
                data: {

                    'action': 'change-photo-position',
                    'start': gliss.photoprocess.startPosition,
                    'end': gliss.photoprocess.endPosition,
                    'current': gliss.photoprocess.currentId,
                }
            })
                .done(function(response){

                    console.log(response);
                    if(response.status == 'success') {

                        for(var i in response.order){

                            var id = response.order[i]['id'];
                            var order = response.order[i]['priority'];

                            $('a#'+id).attr('data-position', order);

                        }

                        response.main && $('.image').css({
                            'background-image': 'url('+response.main+')',
                        });

                        response.avatar && $('.header__action-panel .avatar').css({
                            'background-image': 'url('+response.avatar+')',
                        })

                    } else{

                        $('#sortable').sortable( "cancel" );

                    }

                }).fail(function(){
                    $('#sortable').sortable( "cancel" );
                })

        }

        $('#sortable').sortable({

            start: function(event, ui){

                gliss.photoprocess.currentId = $(ui.item[0]).attr('id');

                var prevEl = $(ui.item[0]).prev();

                gliss.photoprocess.startPosition = $(prevEl[0]).attr('data-position');
            },
            stop: function(event, ui){

                var prevEl = $(ui.item[0]).prev();

                gliss.photoprocess.endPosition = $(prevEl[0]).attr('data-position');

                (gliss.photoprocess.startPosition !== gliss.photoprocess.endPosition) && gliss.photoprocess.changePriorityPhoto();
            },

        });



    return this;

}).call(gliss.photoprocess || {}, jQuery);