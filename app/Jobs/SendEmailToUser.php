<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\User;

class SendEmailToUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    private $user;
    private $template;
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, string $template, array $data = [])
    {
        $this->user = $user;
        $this->template = $template;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('emails.' . $this->template, ['user' => $this->user, 'data' => $this->data], function ($message) {
            $message->from(env('FROM_EMAIL', 'dummy@example.com'));
            $message->to($this->user->email);
        });
    }
}
