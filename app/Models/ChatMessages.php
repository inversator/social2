<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Auth, DB;

class ChatMessages extends Model
{

    protected $table = 'chat_messages';

    protected $fillable = ['id', 'user', 'user_type', 'pipe', 'created_at', 'updated_at', 'messages', 'chat_messages'];

    public function getPipeMessages($topic)
    {

        $result = DB::table($this->table)

            ->leftJoin(DB::raw('users user'), 'user.id', '=', 'user')
            ->leftJoin('user_photo', function($join){
                $join->on('user.id', '=', 'user_photo.user_id')->whereNull('user_photo.deleted_at');
            })
            ->select(
                'user.id as user_id',
                'user.first_name',
                'user.second_name',
                'user.last_visit',
                'user.is_online',
                'user_photo.thumbnail as thumbnail',
                'chat_messages.id',
                'chat_messages.user',
                'chat_messages.user_type',
                'chat_messages.pipe',
                'chat_messages.message',
                'chat_messages.created_at',
                'chat_messages.updated_at'
            )
            ->orderBy('chat_messages.created_at')
//            ->groupBy('chat_messages.id')
            ->where('pipe', "{$topic}")->get();

        return $result;
    }



}