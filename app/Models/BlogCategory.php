<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $table = 'posts_categories';

    protected $fillable = ['title', 'description', 'private', 'slug'];

}
