<?php namespace App\Models;

use DB, Auth, Validator;
use App\Models\UsersInteractions;
use App\Models\NameConstruct;
use Jenssegers\Date\Date;
use App\Models\GlobalNotifications;
use App\Models\City;
use SendGrid;
use App\User;
use App\Models\LaboratoryModel;

class Partners
{

    public static function randomUsers($limit)
    {
        $limit = intval($limit);
        $user_id = Auth::user()->id;
        $users = DB::select("SELECT users.id, users.first_name, users.second_name, users.middle_name, users.rating, users.last_visit,
				partners.favorite, partners.partner,
				confirm.partner AS confirm,
				( SELECT photo.thumbnail FROM user_photo photo WHERE photo.deleted_at IS NULL AND photo.user_id = users.id ) AS photo,
				company.id AS company_id, company.title AS company_name,
				( SELECT COUNT(apartments.id) FROM apartments WHERE apartments.user_id = users.id AND apartments.deleted_at IS NULL ) AS apartments
			FROM user
				LEFT JOIN user_interaction AS partners ON partners.user_id = '{$user_id}' AND partners.user_to = users.id
				LEFT JOIN user_interaction AS confirm ON confirm.user_id = users.id AND confirm.user_to = '{$user_id}'
				LEFT JOIN companies_members member ON member.member_id = users.id AND member.deleted_at IS NULL
				LEFT JOIN companies company ON company.id = member.company_id AND member.deleted_at IS NULL
			WHERE users.id != '{$user_id}'
			  AND users.deleted_at IS NULL
			ORDER BY users.id DESC
			LIMIT $limit
		");
        foreach ($users as $key => $user) {
            $displayName = NameConstruct::displayName($user->first_name, $user->middle_name, $user->second_name);
            $timeNow = new Date("now", 'Europe/Moscow');
            $now = strtotime($timeNow);
            $last_visit = strtotime($user->last_visit);
            $online = $now - $last_visit > 2 * 60 ? false : true;
            $users[$key]->last_visit = Date::parse($user->last_visit, 'Europe/Moscow')->ago();
            $users[$key]->online = $online;
            $users[$key]->displayName = $displayName;
        }
        return $users;
    }

    public static function searchPartners($data)
    {
        $user_id = Auth::user()->id;
        if ($data['value'] || $data['city']) {
            $whereCity = '';
            $whereName = '';
            if (isset($data['city'])) {
                $city = City::where('title', '=', $data['city'])
                    ->first();
                if ($city->id) $whereCity = " AND users.city = '{$data['city']}' ";
            }
            if ($data['value']) $whereName = " AND ( users.first_name LIKE '%{$data['value']}%' OR users.second_name LIKE '%{$data['value']}%' ) ";
            $users = DB::select("SELECT users.id, users.first_name, users.second_name, users.middle_name, users.rating, users.last_visit,
					partners.favorite, partners.partner,
					confirm.partner AS confirm,
					( SELECT photo.thumbnail FROM user_photo photo WHERE photo.deleted_at IS NULL AND photo.user_id = users.id ) AS photo,
					company.id AS company_id, company.title AS company_name,
					( SELECT COUNT(apartments.id) FROM apartments WHERE apartments.user_id = users.id AND apartments.deleted_at IS NULL ) AS apartments
				FROM user
					LEFT JOIN user_interaction AS partners ON partners.user_id = '{$user_id}' AND partners.user_to = users.id
					LEFT JOIN user_interaction AS confirm ON confirm.user_id = users.id AND confirm.user_to = '{$user_id}'
					LEFT JOIN companies_members member ON member.member_id = users.id AND member.deleted_at IS NULL
					LEFT JOIN companies company ON company.id = member.company_id AND member.deleted_at IS NULL
				WHERE users.id != '{$user_id}'
			      AND users.deleted_at IS NULL
			      $whereName
			      $whereCity
			      ORDER BY users.rating ASC
			");
            foreach ($users as $key => $user) {
                $displayName = NameConstruct::displayName($user->first_name, $user->middle_name, $user->second_name);
                $timeNow = new Date("now", 'Europe/Moscow');
                $now = strtotime($timeNow);
                $last_visit = strtotime($user->last_visit);
                $online = $now - $last_visit > 2 * 60 ? false : true;
                $users[$key]->last_visit = Date::parse($user->last_visit, 'Europe/Moscow')->ago();
                $users[$key]->online = $online;
                $users[$key]->displayName = $displayName;
            }
            return ['users' => $users];
        } else return ['users' => []];
    }

    public static function favorite($data)
    {
        if ($data['user_to']) {
            $user_id = Auth::user()->id;
            if ($data['user_to'] == $user_id) return ['error' => true, 'message' => 'Вы не можете добавить в избранное самого себя'];
            $favoriteState = UsersInteractions::where('user_id', $user_id)
                ->where('user_to', $data['user_to'])
                ->get()
                ->toArray();
            if (count($favoriteState) > 0) {
                // update
                $fav = $favoriteState[0]['favorite'] == 1 ? 0 : 1;
                UsersInteractions::where('id', $favoriteState[0]['id'])->update(['favorite' => $fav]);
                return ['success' => true, 'favorite' => $fav];
            } else {
                UsersInteractions::firstOrCreate([
                    'user_id' => $user_id,
                    'user_to' => $data['user_to'],
                    'favorite' => 1,
                ]);
                return ['success' => true, 'favorite' => 1];
            }
        } else return ['error' => true, 'message' => 'Ошибка добавления пользователя в избранное'];
    }

    public static function partner($data)
    {
        if ($data['user_to']) {

            $notification = isset($data['notificate']) ? $data['notificate'] : 0;
            $notif = GlobalNotifications::find($notification);
            $notif_text = "";

            if ($notif) {
                $notif_text = "Вы приняли заявку на партнерство";
                $notif->answer = $notif_text;
                $notif->save();
            }
            $user_id = Auth::user()->id;
            if ($data['user_to'] == $user_id) return ['error' => true, 'message' => 'Вы не можете добавить в партнеры самого себя'];
            $favoriteState = DB::select("SELECT my.id, my.user_id, my.user_to, my.favorite, my.partner,
					partner.partner AS confirm
				FROM user_interaction AS my
					LEFT JOIN user_interaction AS partner
						   ON partner.user_id = my.user_to
						  AND partner.user_to = my.user_id
						  AND partner.deleted_at IS NULL
				WHERE my.user_id = '{$user_id}'
				  AND my.user_to = '{$data['user_to']}'
				  AND my.deleted_at IS NULL
			");
            if (count($favoriteState) > 0) {
                // update
                $partner = $favoriteState[0]->partner == 1 ? 0 : 1;
                UsersInteractions::where('id', $favoriteState[0]->id)->update(['partner' => $partner]);
                if (!$favoriteState[0]->confirm || $partner == 0) Partners::removeAccess($data['user_to']);
                elseif ($favoriteState[0]->confirm && $partner == 1) {
                    $message = 'Пользователь <a href="/user/' . $user_id . '" target="_blank">' . NameConstruct::displayName(Auth::user()->first_name, Auth::user()->middle_name, Auth::user()->second_name) . '<a/> принял Вашу заявку на партнерство';
                    GlobalNotifications::firstOrCreate([
                        'user_id' => $data['user_to'],
                        'sender_id' => $user_id,
                        'optional_id' => 0,
                        'type' => 'partner_accepted',
                        'message' => $message,
                    ]);
                }
                if (!$favoriteState[0]->confirm && $partner == 1) {
                    $message = 'Вам пришел запрос на партнерство от <a href="/user/' . $user_id . '" target="_blank">' . NameConstruct::displayName(Auth::user()->first_name, Auth::user()->middle_name, Auth::user()->second_name) . '<a/><input type="hidden" value="' . $user_id . '" />';
                    GlobalNotifications::firstOrCreate([
                        'user_id' => $data['user_to'],
                        'sender_id' => $user_id,
                        'optional_id' => $user_id,
                        'type' => 'partners',
                        'message' => $message,
                    ]);

                    $user = User::find($data['user_to']);

                    // Уведомление на почту
                    if ($user->notificationPartner == 0) {
                        $sendgridAppKey = Setting::getSetting('SENDGRID_APIKEY');
                        $sendgridEmail = Setting::getSetting('SENDGRID_EMAIL');
                        $sendgridTemplateId = Setting::getSetting('SENDGRID_TEMPLATEID');

                        $sendgrid = new SendGrid($sendgridAppKey);
                        $email = new SendGrid\Email();
                        $email->setFrom($sendgridEmail);

                        $sender = User::find($user_id);

                        $body = "<br />";

                        $title = 'Вам пришел запрос на партнерство';
                        $body .= 'Пользователь ' . $sender->first_name . ' отправил Вам запрос на партнерство';

                        $email->setSubject($title);
                        $email->setHtml(LaboratoryModel::test2($sender, $body));
                        $email->setTemplateId($sendgridTemplateId);

                        $email->addSmtpapiTo($user->email);

                        try {
                            $sendgrid->send($email);

                        } catch (\SendGrid\Exception $e) {
                            foreach ($e->getErrors() as $err) {
                                Log::error('SendMail error: ' . $err);
                            }
                        }

                    }
                    // END Уведомление на почту
                }
                GlobalNotifications::where('user_id', '=', $user_id)
                    ->where('type', '=', 'partners')
                    ->where('optional_id', '=', $data['user_to'])
                    ->delete();
                return ['success' => true, 'answer' => $notif_text, 'partner' => $partner, 'confirm' => $favoriteState[0]->confirm == 1 ? 1 : 0];
            } else {
                UsersInteractions::firstOrCreate([
                    'user_id' => $user_id,
                    'user_to' => $data['user_to'],
                    'partner' => 1,
                ]);
                $confirm = UsersInteractions::where('user_id', $data['user_to'])
                    ->where('user_to', $user_id)
                    ->get()
                    ->toArray();
                if (count($confirm) == 0 || !$confirm[0]['partner']) {
                    Partners::removeAccess($data['user_to']);
                    $message = 'Вам пришел запрос на партнерство от <a href="/user/' . $user_id . '" target="_blank">' . NameConstruct::displayName(Auth::user()->first_name, Auth::user()->middle_name, Auth::user()->second_name) . '<a/><input type="hidden" value="' . $user_id . '" />';
                    GlobalNotifications::firstOrCreate([
                        'user_id' => $data['user_to'],
                        'sender_id' => $user_id,
                        'optional_id' => $user_id,
                        'type' => 'partners',
                        'message' => $message,
                    ]);

                } elseif ($confirm[0]['partner']) {
                    $message = 'Пользователь <a href="/user/' . $user_id . '" target="_blank">' . NameConstruct::displayName(Auth::user()->first_name, Auth::user()->middle_name, Auth::user()->second_name) . '<a/> принял Вашу заявку на партнерство';
                    GlobalNotifications::firstOrCreate([
                        'user_id' => $data['user_to'],
                        'sender_id' => $user_id,
                        'optional_id' => 0,
                        'type' => 'partner_accepted',
                        'message' => $message,
                    ]);
                }
                GlobalNotifications::where('user_id', '=', $user_id)
                    ->where('type', '=', 'partners')
                    ->where('optional_id', '=', $data['user_to'])
                    ->delete();
                return ['success' => true, 'answer' => $notif_text, 'partner' => 1, 'confirm' => count($confirm) > 0 && $confirm[0]['partner'] == 1 ? 1 : 0];
            }
        } else return ['error' => true, 'message' => 'Ошибка добавления пользователя в партнеры'];
    }

    public static function getPartnersPipes($user_id)
    {
        return DB::table(DB::raw('`users` u'))
            ->leftJoin(DB::raw('user_interaction partners'), function($join) use ($user_id) {
                $join->on('partners.user_to', '=','u.id')->where('partners.user_id', '=', DB::raw($user_id));
            })
            ->leftJoin(DB::raw('user_interaction confirm'), function ($join) use ($user_id) {
                $join->on('confirm.user_id', '=', 'u.id')->where('confirm.user_to', '=', DB::raw($user_id));
            })
            ->leftJoin(DB::raw('user_photo photo'), 'u.id', '=', 'photo.user_id')
            ->leftJoin(DB::raw('companies_members member'),  function ($join) {
                $join->on('u.id', '=', 'member.member_id')->whereNull('member.deleted_at');
            })
            ->leftJoin(DB::raw('companies company'), 'member.company_id', '=', 'company.id')
            ->leftJoin(DB::raw('user_pipes pipes'), 'pipes.pipe','=', DB::raw('TO_BASE64( CONCAT(' . $user_id . '* u.id - (' . $user_id . ' + u.id) , "partner") )'))
            ->where('u.id', '!=', $user_id)
            ->whereNull('u.deleted_at')
            ->where(function($query){
                $query->where('confirm.partner', '=', '1')->where('partners.partner','=',1);
            })
            ->select(
                'u.id',
                'u.first_name',
                'u.second_name',
                'u.middle_name',
                'u.last_visit',
                'u.last_visit',
                'photo.thumbnail as photo',

                'pipes.last_open',

                'company.id as company_id',
                'company.title as company_name'
            )
            ->groupBy('u.id')
            ->orderBy('pipes.last_open','DESC')
            ->get();
    }

    public static function getPartners()
    {
        $user_id = Auth::user()->id;
        $partners = [];
        $favorites = [];
        $pending = [];
        $getUsers = DB::select("SELECT users.id, users.first_name, users.second_name, users.middle_name, users.rating, users.last_visit,
				partners.favorite, partners.partner, partners.updated_at,
				confirm.partner AS confirm, confirm.updated_at,
				( SELECT photo.thumbnail FROM user_photo photo WHERE photo.deleted_at IS NULL AND photo.user_id = users.id ) AS photo,
				company.id AS company_id, company.title AS company_name,
				( SELECT COUNT(apartments.id) FROM apartments WHERE apartments.user_id = users.id AND apartments.deleted_at IS NULL ) AS apartments
			FROM user
				LEFT JOIN user_interaction AS partners ON partners.user_id = '{$user_id}' AND partners.user_to = users.id
				LEFT JOIN user_interaction AS confirm ON confirm.user_id = users.id AND confirm.user_to = '{$user_id}'
				LEFT JOIN companies_members member ON member.member_id = users.id AND member.deleted_at IS NULL
				LEFT JOIN companies company ON company.id = member.company_id AND member.deleted_at IS NULL
			WHERE users.id != '{$user_id}'
			  AND users.deleted_at IS NULL
			  AND ( confirm.partner = '1' OR partners.partner = 1 )
			ORDER BY users.rating DESC, confirm.updated_at DESC, partners.updated_at DESC
		");

        $me = Auth::user()->id;

        $myCompanyId = DB::select("SELECT *
					FROM companies_members
					WHERE member_id = '{$me}'
					  AND admin = 1
					  AND deleted_at IS NULL
				");

        $members_array = [];

        if (isset($myCompanyId[0])) {

            $members = DB::select("SELECT member_id
						FROM companies_members
						WHERE company_id = '{$myCompanyId[0]->id}'
						  AND deleted_at IS NULL
					");

            if ($members) {
                foreach ($members as $member) {
                    $members_array[] = $member->member_id;
                }
            }

        }

        foreach ($getUsers as $info) {
            $displayName = NameConstruct::displayName($info->first_name, $info->middle_name, $info->second_name);
            $timeNow = new Date("now", 'Europe/Moscow');
            $now = strtotime($timeNow);
            $last_visit = strtotime($info->last_visit);
            $online = $now - $last_visit > 2 * 60 ? false : true;
            $info->last_visit = Date::parse($info->last_visit, 'Europe/Moscow')->ago();
            $info->online = $online;
            $info->displayName = $displayName;
            $info->is_member = in_array($info->id, $members_array) ? 1 : 0;

            if ($info->partner && $info->confirm) $partners[] = $info;
            else if ($info->partner) $pending[] = $info;
            else $favorites[] = $info;
        }
        return ['partners' => $partners, 'pending' => $pending, 'favorites' => $favorites];
    }

    public static function filterPartners($data)
    {
        $user_id = Auth::user()->id;
        $partners = [];
        $pending = [];
        $response = [];
        $dataName = $data['name'] != '' ? " AND ( users.first_name LIKE '%{$data['name']}%' OR users.second_name LIKE '%{$data['name']}%' ) " : "";
        $ascDesc = $data['desc'] ? ' ASC ' : ' DESC ';
        $sort = " ORDER BY users.rating DESC, confirm.updated_at DESC, partners.updated_at DESC ";
        if ($data['sort'] == 1) $sort = " ORDER BY apartments $ascDesc, users.rating DESC, confirm.updated_at DESC, partners.updated_at DESC ";
        elseif ($data['sort'] == 2) $sort = " ORDER BY users.rating $ascDesc, confirm.updated_at DESC, partners.updated_at DESC ";
        elseif ($data['sort'] == 3) {
            if ($ascDesc == ' DESC ') $ascDesc = ' ASC ';
            else $ascDesc = ' DESC ';
            $sort = " ORDER BY confirm.updated_at $ascDesc, partners.updated_at $ascDesc, users.rating DESC ";
        }
        $getUsers = DB::select("SELECT users.id, users.first_name, users.second_name, users.middle_name, users.rating, users.last_visit,
				partners.favorite, partners.partner, partners.updated_at,
				confirm.partner AS confirm, confirm.updated_at,
				( SELECT photo.thumbnail FROM user_photo photo WHERE photo.deleted_at IS NULL AND photo.user_id = users.id ) AS photo,
				company.id AS company_id, company.title AS company_name,
				( SELECT COUNT(apartments.id) FROM apartments WHERE apartments.user_id = users.id AND apartments.deleted_at IS NULL ) AS apartments
			FROM user
				LEFT JOIN user_interaction AS partners ON partners.user_id = '{$user_id}' AND partners.user_to = users.id
				LEFT JOIN user_interaction AS confirm ON confirm.user_id = users.id AND confirm.user_to = '{$user_id}'
				LEFT JOIN companies_members member ON member.member_id = users.id AND member.deleted_at IS NULL
				LEFT JOIN companies company ON company.id = member.company_id AND member.deleted_at IS NULL
			WHERE users.id != '{$user_id}'
			  AND users.deleted_at IS NULL
			  $dataName
			  AND ( confirm.partner = '1' OR partners.partner = 1 )
			$sort
		");
        foreach ($getUsers as $info) {
            $displayName = NameConstruct::displayName($info->first_name, $info->middle_name, $info->second_name);
            $timeNow = new Date("now", 'Europe/Moscow');
            $now = strtotime($timeNow);
            $last_visit = strtotime($info->last_visit);
            $online = $now - $last_visit > 2 * 60 ? false : true;
            $info->last_visit = Date::parse($info->last_visit, 'Europe/Moscow')->ago();
            $info->online = $online;
            $info->displayName = $displayName;
            if (!$data['online'] || ($data['online'] && $online)) {
                if ($info->partner && $info->confirm) $partners[] = $info;
                else if ($info->partner) $pending[] = $info;
                if (($info->partner && $info->confirm) || $info->partner) $response[] = $info;
            }
        }
        if ($data['sort'] == 0) {
            $response = [];
            foreach ($pending as $value) {
                $response[] = $value;
            }
            foreach ($partners as $value) {
                $response[] = $value;
            }
        }
        return ['users' => $response];
    }

    public static function removeAccess($user_id)
    {
        $accesses = DB::select("SELECT access.id
			FROM apartments_usershare access
				LEFT JOIN apartments ON apartments.id = access.apartment_id
			WHERE ( ( access.user_id = '1' AND apartments.user_id = '2') OR ( access.user_id = '2' AND apartments.user_id = '1' ) )
			  AND access.deleted_at IS NULL
		");
        $toDelete = [];
        foreach ($accesses as $value) {
            $toDelete[] = $value->id;
        }
        if (count($toDelete)) {
            $ids = implode(',', $toDelete);
            DB::update("UPDATE apartments_usershare SET deleted_at = NOW() WHERE id IN ($ids)");
        }
        return true;
    }
}
