<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Clients as ClientsModel;
use Carbon\Carbon;

use Auth, DB;

class UserPipes extends Model
{
    public $timestamps = false;

    protected $table = 'user_pipes';

    protected $fillable = ['id', 'user_id', 'pipe', 'last_open', 'status'];

    public static function getAllCountUnread($user_id){

        $res = DB::table(DB::raw('user_pipes p'))
            ->join(DB::raw('chat_messages m'), function ($join){
                $join->on('p.pipe','=','m.pipe');
                $join->on('p.last_open','<','m.created_at');
            })
            ->where('p.user_id', $user_id)
            ->where('m.user', '<>', $user_id)
            ->count()
        ;
        return $res;
    }

    public static function getPipesCountUnread($user_id){

        $res = DB::table(DB::raw('chat_messages m'))
            ->join(DB::raw('user_pipes p'), function ($join){
                $join->on('p.pipe','=','m.pipe');
                $join->on('p.last_open','<','m.created_at');
            })
            ->select(DB::raw('count(m.id) as count, m.pipe, m.pipe_type, m.user'))
            ->where('p.user_id', $user_id)
            ->where('m.user', '<>', $user_id)
            ->groupBy('m.pipe', 'm.pipe_type', 'm.user')
            ->get()
            ->keyBy('pipe')
//            ->toSql()
        ;
        return $res;
    }

    public static function isExistPipe($user1, $user2){
        $pipe = createPipe($user1, $user2, 'private');

        return db::table('user_pipes')->where('user_id', $user1)->where('pipe', $pipe)->first();

    }

    public static function createPipe($user1, $user2){


        $pipe = createPipe($user1, $user2, 'private');

        db::beginTransaction();

        try {

            //создаем канал1
            $userPipe1 = UserPipes::where('user_id', $user1)->where('pipe', $pipe)->first();

            if (!$userPipe1) {

                $userPipe = new UserPipes;
                $userPipe['user_id'] = $user1;
                $userPipe['pipe'] = $pipe;
                $userPipe['last_open'] = Carbon::today();

                $userPipe->save();
            }

            //создаем канал 2
            $userPipe2 = UserPipes::where('user_id', $user2)->where('pipe', $pipe)->first();

            if (!$userPipe2) {

                $userPipe = new UserPipes;
                $userPipe['user_id'] = $user2;
                $userPipe['pipe'] = $pipe;
                $userPipe['last_open'] = Carbon::today();

                $userPipe->save();
            }
        } catch(\Exxceptio $e){

            db::rollback();

            throw $e;
        }

        db::commit();
        return true;

    }


}