<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyValue extends Model
{
    protected $table = 'property_values';

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id', 'id');
    }

    public function translations()
    {
        return $this->hasMany(PropertyValueTranslation::class, 'property_value_id', 'id');
    }

    public function getTranslatedValue(Language $lang)
    {
        $text = $this->translations->where('language_id', $lang->id)->first();
        return $text ? $text->translation : null;
    }
}
