<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageLocalized extends Model
{
    protected $table = 'pages_localized';

    protected $fillable = ['title', 'description', 'keywords', 'text', 'language_id', 'page_id'];
}
