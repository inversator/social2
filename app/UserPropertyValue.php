<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPropertyValue extends Model
{
    protected $table = 'user_property_values';

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id', 'id');
    }

    public function value()
    {
        return $this->belongsTo(PropertyValue::class, 'property_value_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
