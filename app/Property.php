<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';

    protected $fillable = ['active'];

    public function values()
    {
        return $this->hasMany(PropertyValue::class, 'property_id', 'id');
    }

    public function category(){
        return $this->hasOne(PropertyCategory::class, 'id', 'category_id');
    }

    public function translations()
    {
        return $this->hasMany(PropertyTranslation::class, 'property_id', 'id');
    }

    public function getTranslatedProperty(Language $lang)
    {
        $translation = $this->translations->where('language_id', $lang->id)->first();
        return $translation ? $translation->translation : null;
    }

    public function uservalues()
    {
        return $this->hasMany(UserPropertyValue::class, 'property_id', 'id');
    }

}
