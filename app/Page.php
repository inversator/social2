<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    protected $fillable = ['slug', 'active_from'];

    protected $dates = ['active_from'];

    public function localizations()
    {
        return $this->hasMany(PageLocalized::class, 'page_id','id');
    }
}
