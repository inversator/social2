<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslationKey extends Model
{
    protected $table = 'translations_keys';

    public function translations()
    {
        return $this->hasMany(TranslationKeyValue::class, 'translation_key_id', 'id');
    }
}

