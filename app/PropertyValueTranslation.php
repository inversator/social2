<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyValueTranslation extends Model
{
    protected $table = 'property_value_translations';

    protected $fillable = ['translation', 'property_value_id', 'language_id'];

    public function propertyValue()
    {
        return $this->belongsTo(PropertyValue::class, 'property_value_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }
}
