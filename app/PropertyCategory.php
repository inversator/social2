<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyCategory extends Model
{
    protected $table = 'property_categories';

    public function translations()
    {
        return $this->hasMany(PropertyCategoryTranslation::class, 'property_category_id', 'id');
    }

    public function properties()
    {
        return $this->hasMany(Property::class, 'category_id');
    }

    public function getTranslatedProperty(Language $lang)
    {
        $translation = $this->translations->where('language_id', $lang->id)->first();
        return $translation ? $translation->translation : null;
    }
}
