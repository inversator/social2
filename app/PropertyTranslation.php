<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTranslation extends Model
{
    protected $table = 'property_translations';

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }
}
