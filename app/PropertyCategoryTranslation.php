<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyCategoryTranslation extends Model
{
protected $table = 'property_categories_translations';

    public function property()
    {
        return $this->belongsTo(PropertyCategory::class, 'property_category_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }

}
