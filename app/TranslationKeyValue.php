<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslationKeyValue extends Model
{
    protected $table = 'translation_key_values';

    public function translationKey()
    {
        return $this->belongsTo(TranslationKey::class, 'translation_key_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }
}

