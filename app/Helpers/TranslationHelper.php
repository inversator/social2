<?php

namespace App\Helpers;
use App\Property;
use App\PropertyCategory;
use App\PropertyValue;
use App\Language;
use App\TranslationKey;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;

class TranslationHelper
{
    private static $language_id;
    private static $properties;
    private static $propertyValues;
    private static $translationKeys;
    private static $propertycategories;
    private static $preloaded = false;
    private static $cacheKey = 'TRANSLATIONS';

    public static function getCurrentLanguage()
    {
        if (self::$language_id) {
            return self::$language_id;
        }

        $lang = Session::get('language_id');
        if (!$lang) {
            $langCode = Cookie::get('language');
            $exists = Language::where('code', $langCode)->first();
            if (!$exists) {
                $exists = Language::where('code', config('app.locale'))->first();
            }
            if ($exists) {
                Session::put('language_id', $exists->id);
                self::$language_id = $exists->id;
                return self::$language_id;
            }
            self::$language_id = 0;
            return self::$language_id;
        }

        return $lang;
    }

    public static function clearCache()
    {
        self::$preloaded = false;
        Cache::forget(static::$cacheKey);
    }

    private static function preload()
    {
        if (!self::$preloaded) {
            $inCache = Cache::get(static::$cacheKey);
            if (!$inCache) {
                self::$properties = Property::with('translations')->get();
                self::$propertyValues = PropertyValue::with('translations')->get();
                self::$translationKeys = TranslationKey::with('translations')->get();
                self::$propertycategories = PropertyCategory::with('translations')->get();
                Cache::put(static::$cacheKey, [
                    'properties' => self::$properties,
                    'propertyValues' => self::$propertyValues,
                    'translationKeys' => self::$translationKeys,
                    'propertycategories' => self::$propertycategories
                ], 5);
            } else {
                self::$properties = $inCache['properties'];
                self::$propertyValues = $inCache['propertyValues'];
                self::$translationKeys = $inCache['translationKeys'];
                self::$propertycategories = $inCache['propertycategories'];
            }
            self::$preloaded = true;
        }
    }

    public static function translateProperty(Property $prop)
    {
        self::preload();
        $languageId = self::getCurrentLanguage();

        $prop = self::$properties->where('id', $prop->id)->first();
        $trans = $prop->translations->where('language_id', $languageId)->first();

        return $trans ? $trans->translation : null;
    }

    public static function translatePropertyCategory(PropertyCategory $category)
    {
        self::preload();
        $languageId = self::getCurrentLanguage();

        $category = self::$propertycategories->where('id', $category->id)->first();
        $trans = $category->translations->where('language_id', $languageId)->first();

        return $trans ? $trans->translation : null;
    }

    public static function translatePropertyValue(PropertyValue $propValue)
    {
        self::preload();
        $languageId = self::getCurrentLanguage();

        $propValue = self::$propertyValues->where('id', $propValue->id)->first();
        $trans = $propValue->translations->where('language_id', $languageId)->first();

        return $trans ? $trans->translation : null;
    }

    public static function translateKey(string $key)
    {
        self::preload();
        $languageId = self::getCurrentLanguage();

        $value = self::$translationKeys->where('key', $key)->first();
        if (!$value) {
            return 'UNKNOWN_TRANSLATION_KEY';
        }
        $trans = $value->translations->where('language_id', $languageId)->first();
        return $trans ? $trans->translation : null;
    }
}
