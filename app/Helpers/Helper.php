<?php

use Jenssegers\Date\Date;

function createPipe($from, $to, $string)
{
    return base64_encode(($from * $to - ($from + $to)) . $string);
}

function cropChatUserName($userName, $num)
{
    if (strlen($userName) > $num) {
        $result = explode(' ', $userName);

        return $result[0] . ' ' . $result[1];
    } else {
        return $userName;
    }
}

function userOnlineOnVisit($lastVisit)
{
    $timeNow = new Date("now", 'Europe/Moscow');
    $now = strtotime($timeNow);
    $last_visit = strtotime($lastVisit);

    return $now - $last_visit > 2 * 60 ? false : true;
}

function timeAgo($date){
    return Date::parse($date, 'Europe/Moscow')->ago();
}