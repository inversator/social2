<?php
namespace App\Handlers\Events;

use App\User;
use App\Http\Controllers\Pusher as Chat;

use Illuminate\Auth\Events\Logout;
use Illuminate\Auth\Events\Login;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class chatRoom
{
    public function online(Login $event)
    {
        Chat::sentDataToServer([
            'type' => 4,
            'user_id' => $event->user->id
        ]);

        $user = User::find($event->user->id);
        $user->is_online = 1;
        $user->save();
    }

    public function offline(Logout $event)
    {
        Chat::sentDataToServer([
            'type' => 5,
            'user_id' => $event->user->id
        ]);

        $user = User::find($event->user->id);
        $user->is_online = 0;
        $user->save();
    }
}