<?php

namespace App\Http\Requests\Admin;

class PageCreateRequest extends PageRequest
{
    public function rulesOnPost()
    {
        return [
            'slug' => 'required|string|unique:pages,slug',
            'title' => 'required|string|max:1024',
            'keywords' => 'required|string|max:1024',
            'description' => 'required|string|max:1024',
            'text' => 'required|string|max:100000',
            'language_id' => 'required|integer|exists:languages,id',
        ];
    }
}
