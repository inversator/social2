<?php

namespace App\Http\Controllers;

use App\Helpers\TranslationHelper;
use App\Models\UserPhoto;
use App\PropertyCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Property;
use App\UserPropertyValue;
use Illuminate\Support\Facades\Auth;
use App\Language;
use Intervention\Image\Facades\Image;
use DB;

class ProfileController extends Controller
{
    public function index(){

        $user = Auth::user();
        $categories = PropertyCategory::where('active', 1)->get();
        $lang = Language::where('id', TranslationHelper::getCurrentLanguage())->first();
        $photos = $user->getPhotos();


        return view('profile.index', compact('user', 'categories', 'lang', 'photos'));
    }

    public function showOld(Request $request)
    {
        $properties = Property::where('active', true)->get();
        $userPropertyValues = $request->user()
            ->propertyValues()->get()->pluck('property_value_id', 'property_id');
        // we can use $userPropertyValues[$prop_id] in template to verify current value of property


        return view('profile', compact('properties', 'userPropertyValues'));
    }

    public function show(Request $request, $id){

        $user = User::find($id);

        if(!$user){
            return abort(404);
        }

        $categories = PropertyCategory::where('active', 1)->get();
        $lang = Language::where('id', TranslationHelper::getCurrentLanguage())->first();
        $photos = $user->getPhotos();
        $status = Auth::user()->getChatStatus($user->id);

        return view('profile.show', compact('user', 'categories', 'lang', 'photos', 'status'));

    }

    public function update(Request $request)
    {
        $properties = Property::where('active', true)
            ->with('values')->get();
        $userProperties = $request->user()
            ->propertyValues()->get();
        $data = $request->get('property');
        $height = $request->get('height');
        $weight = $request->get('weight');
        $breast_size = $request->get('breast_size');

        $rules = [
            'height' => 'nullable|integer|min:100|max:220',
            'weight' => 'nullable|integer|min:30|max:200',
            'breast_size' => 'nullable|integer|min:0|max:5',
        ];

        $validator = Validator::make(['height' => $height, 'weight' => $weight, 'breast_size' => $breast_size], $rules);
        if ($validator->fails()) {
            return redirect()->route('profile')
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $user = $request->user();
        $user->height = $height;
        $user->weight = $weight;
        $user->breast_size = $breast_size;
        $user->save();

        if (is_array($data)) {
            foreach ($data as $propertyId => $propertyValueId) {
                // we expect only integers in request data
                // but propertyValueId can be empty
                // it means that <select> is empty

                // is it a valid property?
                $property = $properties->where('id', $propertyId)->first();
                if ($property) {
                    if (!$propertyValueId) {
                        // <select> is empty
                        // so we remove property value from user
                        $prop = $userProperties->where('property_id', $propertyId)->first();
                        if ($prop) $prop->delete();
                    } else {
                        // is it valid propertyValueId?
                        $propertyValue = $property->values()->where('id', $propertyValueId)->first();
                        if ($propertyValue) {
                            // if it is already exists, we update it
                            $userPropertyValue = $userProperties->where('property_id', $propertyId)->first();
                            if ($userPropertyValue) {
                                $userPropertyValue->value()->associate($propertyValue);
                                $userPropertyValue->save();
                            } else {
                                // does not exist, create new
                                $userPropertyValue = new UserPropertyValue;
                                $userPropertyValue->property()->associate($property);
                                $userPropertyValue->value()->associate($propertyValue);
                                $request->user()->propertyValues()->save($userPropertyValue);
                            }
                        }
                    }
                }
            }
        }

        return redirect()->route('profile');
    }

    public function edit(Request $request){


        $action = camel_case($request->get('action'));

        if(method_exists($this, $action)){

           return $this->$action($request);
        }

        abort(404);
    }

    public function properties(Request $request){

        $category = null;


        foreach($request->except(['_token', 'action']) as $property_id => $value_id) {

            if ($value_id !== null) {

                $property = Property::where('id', $property_id)->first();

                $category = $property->category;


                if ($property->field_type == 'mselect') {


                    $availableValues = db::table('property_values')->where('property_id', $property_id)->select('id')->get();

                    foreach($availableValues->pluck('id') as $vid){

                        if(!in_array( $vid, $value_id)){

                            db::table('user_property_values')
                                ->where('property_id', $property_id)
                                ->where('property_value_id', $vid)
                                ->where('user_id', Auth::id())
                                ->delete();


                        } else {

                            $value = UserPropertyValue::where('property_id', $property_id)
                                ->where('user_id', Auth::id())
                                ->where('property_value_id', $vid)
                                ->first();

                            if (!$value) {
                                $value = new UserPropertyValue();
                                $value->user_id = Auth::id();
                            }

                            $value->property_id = $property_id;
                            $value->property_value_id = $vid;
                            $value->save();

                        }


                    }


                } else {

                        $value = UserPropertyValue::where('property_id', $property_id)
                            ->where('user_id', Auth::id())->first();

                        if (!$value) {
                            $value = new UserPropertyValue();
                            $value->user_id = Auth::id();
                        }
                        $value->property_id = $property_id;

                        if ($property->field_type == 'select') {
                            $value->property_value_id = $value_id;

                        } else {
                            $value->value = $value_id;
                        }

                        $value->save();
                }
            }
        }


        $lang = Language::where('id', TranslationHelper::getCurrentLanguage())->first();

        $view = $category->view === NULL ? 'table' : $category->view;

        return view('partials.'.$view, ['properties' => $category->properties, 'lang'=>$lang]);

    }

    public function description(Request $request){

        $user = Auth::user();
        $user->description = (($request->get('description')));
        $user->save();

        return response()->json([
            'status' => 'success',
            'description' => $user->description,
        ]);

    }

    public function upload($request)
    {

        $ans = [
            'status' => 'fail',
        ];

        $user = Auth::user();

        // если не найден юзер
        if (!$user) {
            $ans['error'] = 'Произошла ошибка. Попробуйте еще раз.';
            return response()->json($ans);
        };


        try {

            $photo = $request->file('photo');
            $ext = $photo->getClientOriginalExtension();

            Validator::make($request->file(), [
                'photo' => 'required|mimes:jpeg,bmp,png,jpg,gif',
            ])->validate();

            $img = Image::make($photo->getRealPath());

            //массив разрешений изображений, миниатюра, средний и оригинал. Ресайз по высоте
            $sizes = [
                'thumb' => 45,
                'medium' => 420,
                'original' => $img->height()
            ];

            $filename = md5(microtime() . rand(0, 9999)) . '.' . $ext;

            $photoPaths = [];

            foreach ($sizes as $size => $height) {

                $destinationPath = 'user_photos/' . $user->id . '/' . $size;

                if (!file_exists($destinationPath)) {

                    if (!mkdir($destinationPath, 0777, true)) {
                        $ans['error'] = 'Произошла ошибка. Попробуйте еще раз';
                        return response()->json($ans);
                    }
                }

                //ресайзим по высоте
                Image::make($photo->getRealPath())->heighten($height)->save($destinationPath . '/' . $filename);

                $photoPaths[$size] = $destinationPath . '/' . $filename;
            }


            $priority = UserPhoto::where('user_id', $user->id)->max('priority');


            $userPhoto = new UserPhoto();
            $userPhoto->name = $filename;
            $userPhoto->user_id = $user->id;
            $userPhoto->priority = $priority === null ? 0 : $priority + 1;
            $userPhoto->save();


        } catch (\ImageException $e) {

            $ans['error'] = $e->getMessage();
            return response()->json($ans);
        }

        $ans['status'] = 'ok';
        $ans['name'] = url($photoPaths['medium']);
        $ans['id'] = $userPhoto->id;
        $ans['order'] = $priority+1;

        if ($priority === null){
            $ans['main'] = $photoPaths['original'];
            $ans['avatar'] = $photoPaths['thumb'];
        }

        return response()->json($ans);
    }

    public function changeStatus($request){

        $ans = ['status' => 'fail'];

        if (Auth::user()->changeChatStatus($request->input())){
            $ans['status'] = 'success';
        };

        return response()->json($ans);
    }

    public function sendMessage(Request $request){
        $ans = ['status' => 'fail'];

        if (Auth::user()->sendMessage($request->input('to'), $request->input('text'))){
            $ans['status'] = 'success';
        };

        return response()->json($ans);

    }

    public function changePhotoPosition(Request $request){
//        dd($request->all());

        $ans = ['status' => 'fail'];

        $start = (int)$request->get('start', -1);
        $end = (int)$request->get('end', -1);

        db::beginTransaction();


        try {

            if($start > $end) {
;
                db::table('user_photo')
                    ->where('user_id',Auth::id())
                    ->where('priority','>', $end)
                    ->where('priority', '<=', $start)
                    ->increment('priority');

                db::table('user_photo')->where('id', $request->get('current'))->update(['priority' => ($end+1)]);

            } else {

                db::table('user_photo')
                    ->where('user_id',Auth::id())
                    ->where('priority', '>', $start)
                    ->where('priority', '<=', $end)
                    ->decrement('priority');

                db::table('user_photo')->where('id', $request->get('current'))->update(['priority' => $end]);
            }

        } catch(\Exception $e) {
throw new \Exception($e->getMessage());
            db::rollback();
            $ans['status'] = 'fail';
            return response()->json($ans);
        }

        db::commit();

        $photoPositions = db::table('user_photo')->where('user_id', Auth::id())->select('id', 'priority')->get()->toArray();

        $ans['status'] = 'success';
        $ans['order'] = $photoPositions;
        if ($end === -1 or $start === -1){
            $ans['main'] = Auth::user()->getMainPhoto('original');
            $ans['avatar'] = Auth::user()->getMainPhoto('thumb');
        }
        return response()->json($ans);

    }

}
