<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Feedback;

class FeedbackController extends Controller
{
    public function index(){
        return view('feedback');
    }

    public function send(Request $request){
//        dd($request->all());

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'text' => 'required',
        ]);

        $from = $request->get('email');
        $text = $request->get('text');

        $data = [
            'from' => htmlspecialchars($from),
            'text' => trim(htmlspecialchars($text))
        ];

        try {

            Mail::to(['support@sweetlife.ru'])->send(new Feedback($data));

        } catch(\Exception $e){
            throw new \Exception($e->getMessage());

            return redirect()->back()->withErrors('Невозможно отправить сообщение.');
        }

        return redirect('feedback')->with('status', 'Сообщение отправлено.');
    }
}
