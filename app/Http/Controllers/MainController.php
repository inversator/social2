<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class MainController extends Controller
{
    public function index(Request $request)
    {

        $girls = User::where('gender', 'F')->orderBy('created_at', 'desc')->limit(8)->get();

        return view('index.index', compact('girls'));
    }
}
