<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Cookie;
use App\Language;
use Illuminate\Contracts\Encryption\DecryptException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $languages = Language::all();
        View::share('languages', $languages);
        try {
            $language = Crypt::decrypt(Cookie::get('language'));
        } catch (DecryptException $e) {
            $language = null;
        }
        if ($languages->where('code', $language)->first()) {
            View::share('language', $language);
        } else {
            if ($lang = $languages->get(0)) {
                View::share('language', $lang);
            }
        }
    }
}
