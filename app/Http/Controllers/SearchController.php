<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Property;
use App\UserPropertyValue;
use App\User;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $properties = Property::where('active', true)->get();

        $selectedProperties = $request->get('property');
        $page = (int)$request->get('page');
        if ($page <= 0) $page = 1;

        $filteredUsers = User::where('active', true)
            ->with('propertyValues');

        $gender = $request->get('gender');
        if ($gender) {
            $filteredUsers->where('gender', $gender);
        }
        $attrs = ['height', 'weight', 'breast_size'];
        foreach ($attrs as $attr) {
            $from = $attr . '_from';
            $to = $attr . '_to';
            $from = (int)$request->get($from);
            $to = (int)$request->get($to);
            if ($from > 0) {
                $filteredUsers->where($attr, '>=', $from);
            }
            if ($to > 0) {
                $filteredUsers->where($attr, '<=', $to);
            }
        }

        if (is_array($selectedProperties) && $request->user()->hasPremiumAccount()) {
            foreach ($selectedProperties as $propertyId => $propertyValueIds) {
                if (is_array($propertyValueIds) && !empty($propertyValueIds)) {
                    $filteredUsers->where(function($query) use($propertyId, $propertyValueIds) {
                        $subQuery = UserPropertyValue::where('property_id', $propertyId)
                            ->whereIn('property_value_id', $propertyValueIds)
                            ->selectRaw('1');
                        $query->whereRaw(DB::raw( '(' . $subQuery->toSql() . ') = 1'));
                        $query->mergeBindings($subQuery->getQuery());
                    });
                }
            }
        }

        $users = $filteredUsers->paginate(30);

        return view('search', compact('properties', 'selectedProperties', 'users'));
    }
}
