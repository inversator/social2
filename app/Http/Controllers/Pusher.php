<?php
/**
 * Created by PhpStorm.
 * User: maa
 * Date: 15.08.2017
 * Time: 10:04
 */

namespace App\Http\Controllers;

use App\Models\Basket;
use App\Models\ChatMessages;
use App\Models\UserPipes;
use App\Models\UserPhoto;
use ZMQContext;


use Carbon\Carbon;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

class Pusher implements WampServerInterface
{
    protected $subscribedTopics = [];
    protected $listenerTopics = [];

    protected $model, $basket, $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->model = new ChatMessages();
//        $this->basket = new Basket();
    }

    public function getSubscribedTopics()
    {
        return $this->subscribedTopics;
    }

    public function addSubscribedTopic($topic)
    {
        $this->subscribedTopics[$topic->getId()] = $topic;
    }

    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        $this->addSubscribedTopic($topic);
        $topicMessages = $this->model->getPipeMessages("{$topic}");

        $topicMessages = $topicMessages->toArray();

        $conn->event($topic, [
            'type' => 3,
            'topicMessages' => $topicMessages,
        ]);
    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {

    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        echo "New connection ({$conn->resourceId})\n";
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected \n";
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    /*
     * send messages
     event type:
        type = 0 : init
        type = 1 : message
        type = 2 : user typing
        type = 3 : get all topic messages
        type = 4 : user online
        type = 5 : user offline
        type = 6 : user pipe off
        type = 7 : user pipe on
    */
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        // If user typing

        switch ($event['type']) {

            case(0):

                $pipes = UserPipes::where('user_id', $event['user_id'])->select('pipe')->get();

                if ($pipes) {
                    foreach ($pipes as $pipe) {

                        if (!isset($this->listenerTopics[$pipe['pipe']])) {
                            $this->listenerTopics[$pipe['pipe']] = [];
                        }

                        $this->listenerTopics[$pipe['pipe']][$event['user_id']] = 'user' . $event['user_id'] . 'pipe';
                    }
                }

                break;
            case(1):

                // Send message

                $data = [
                    'id' => $conn->resourceId,
                    'topic_id' => "{$topic}",
                    'message' => isset($event['message']) ? $event['message'] : ' � ��������� �� ���������� ',
                    'from' => isset($event['name']) ? $event['name'] : '������������',
                    'type' => isset($event['type']) ? $event['type'] : '1',
                    'user_type' => $event['user_type'],
                    'user_id' => isset($event['user_id']) ? $event['user_id'] : $event['name'],
                    'user_img' => ($event['user_type'] != 'client') ? '' : '', //UserPhoto::select('path as thumbnail')->where('user_id', $event['user_id'])->first()
                    'time' => time(),
                    'pipe_type' => isset($event['pipe_type']) ? $event['pipe_type'] : 'user',
                ];

                $message = MessagesController::addMessage($topic, $data);
                $data['message_id'] = $message->id;

                $this->sentDataToServer($data);


                // Add unread messages to listener

                if (isset($this->listenerTopics["{$topic}"]))

                    foreach ($this->listenerTopics["{$topic}"] as $user => $pipe) {
                        $this->sentDataToServer([
                            'topic_id' => $pipe,
                            'user_id' => $user,
                            'pipe' => "{$topic}",
                            'type' => 0,
                            'pipe_type' => isset($event['pipe_type']) ? $event['pipe_type'] : 'client',
                        ]);
                    }


                break;

            case(2):


                foreach ($this->clients as $client) {

                    if ($client != $conn && $topic->has($client)) {
                        $client->event($topic, [
                            'id' => $conn->resourceId,
                            'type' => 2,
                            'from' => isset($event['name']) ? $event['name'] : '',
                        ]);
                    }
                }
                break;
            case(4):
                break;
            case(5):
                break;
            case(6):

                MessagesController::setPipeTime($event['user_id'], $event['pipe']);
                break;
            case(7):
                if ($event['pipe_type'] == 'private' && false){
                    MessagesController::setPipeTime($event['user_id'], $event['pipe']);
                    MessagesController::setUserPipe($event['user_to'], $event['pipe'], Carbon::today());
                } else {
                    MessagesController::setPipeTime($event['user_id'], $event['pipe']);
                }
                break;
            default:
                break;

        }

    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occured: {$e->getMessage()} \n";
        $conn->close();
    }

    static function sentDataToServer(array $data)
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');

        $socket->connect('tcp://127.0.0.1:5555');

        $data = json_encode($data);
        $socket->send($data);

    }

    public function broadcast($jsonDataToSend)
    {
        $aDataToSend = json_decode($jsonDataToSend, true);
        $subscribedTopics = $this->getSubscribedTopics();

        if (isset($aDataToSend['topic_id']) && isset($subscribedTopics[$aDataToSend['topic_id']])) {

            $topic = $subscribedTopics[$aDataToSend['topic_id']];
            $topic->broadcast($aDataToSend);

        } else {

            if ($aDataToSend['type'] == 4 || $aDataToSend['type'] == 5) {

                foreach ($subscribedTopics as $topic) {
                    $topic->broadcast($aDataToSend);
                }
            }
        }

    }
}