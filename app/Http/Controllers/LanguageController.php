<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Language;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function setLanguage(Request $request, $code)
    {
        $language = Language::where('code', $code)->first();
        if ($language) {
            Cookie::queue('language', $language->code, 86400 * 1000);
            Session::put('language_id', $language->id);
        }

        return redirect()->back();
    }
}
