<?php

namespace App\Http\Controllers;

use App\Models\Basket;
use App\Models\Companies;
use Carbon\Carbon;
use App\Models\ChatMessages;
use App\Models\UserPipes;

use DB;

use App\Users as User;
use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;
use View, Auth, Input;

use App\Models\Administrator;
use App\Models\Maid;

class MessagesController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->middleware('auth');
        $this->model = new ChatMessages();
    }


    public function pipes()
    {
        $user = Auth::user();

        $pipes = $this->getPrivate($user->id);

        $this->setUserPipes($pipes, $user->id, 'private');

        // Set unread messages
        $pipesUnreadCount = UserPipes::getPipesCountUnread($user->id);
//        dd($pipesUnreadCount);
        $clientUnread = $userUnread = $techUnread = $companyUnread = $indiUnread = $privateUnread = 0;
        foreach ($pipesUnreadCount as $value) {

            switch ($value->pipe_type) {
                case 'client':
                    $clientUnread += $value->count;
                    break;
                case 'tech':
                    $techUnread += $value->count;
                    break;
                case 'user':
                    $userUnread += $value->count;
                    break;
                case 'indi':
                    $indiUnread += $value->count;
                    break;
                case 'company':
                    $companyUnread += $value->count;
                    break;
                case 'private':
                    $privateUnread += $value->count;
            }
        }

        $pipesUnreadCount['client'] = $clientUnread;
        $pipesUnreadCount['tech'] = $techUnread;
        $pipesUnreadCount['user'] = $userUnread;
        $pipesUnreadCount['indi'] = $indiUnread + $companyUnread;
        $pipesUnreadCount['company'] = $companyUnread;
        $pipesUnreadCount['private'] = $privateUnread;

        $pipes = $pipes->toArray();

        foreach ($pipes as $key => $pipe) {
            Carbon::setLocale('ru');
            $pipes[$key]['birthday'] = Carbon::parse($pipe['birthday'])->diffForHumans(null, true);
        }

        return view('chat.index', [
            'pipes' => $pipes,
            'user' => $user,
            'unreadCount' => $pipesUnreadCount,

            'privateUnread' => $privateUnread,
        ]);
    }

    public static function addMessage($topic, $data)
    {
        $message = new ChatMessages();

        $message['user'] = $data['user_id'];
        $message['user_type'] = $data['user_type'];
        $message['pipe_type'] = $data['pipe_type'];
        $message['pipe'] = $topic;
        $message['message'] = $data['message'];

        $message->save();

        return $message;
    }

    public function getPrivate($user_id)
    {
        return UserPipes::join('chat_messages', 'chat_messages.pipe', '=', 'user_pipes.pipe')
            ->join(DB::raw('user_pipes as pipes'), function ($join) use ($user_id) {
                $join->on('pipes.pipe', '=', 'chat_messages.pipe')->where('pipes.user_id', '!=', $user_id);
            })
            ->join('users', 'users.id', '=', 'pipes.user_id')
            ->leftJoin('user_photo', 'users.id', '=', 'user_photo.user_id')
            ->where('chat_messages.pipe_type', '=', 'private')
            ->where('user_pipes.user_id', '=', $user_id)
            ->select(
                'user_pipes.pipe',
                'user_pipes.status',
                'users.id',
                'users.name',
                'users.is_online',
                'users.first_name',
                'users.second_name',
                'users.birthday',
                'user_photo.thumbnail',
                DB::raw('count(chat_messages.id) as count_messages')
            )
            ->groupBy('user_pipes.pipe', 'user_pipes.status', 'users.id', 'user_photo.thumbnail')
            ->get();
    }

    public function setUserPipes($arr, $user_id, $type = 'private')
    {
        // Перебираем каналы пользователя
        foreach ($arr as $userInfo) {


            $issetPipe = UserPipes::where('user_id', $user_id)->where('pipe', createPipe($userInfo->id, $user_id, $type))->first();

            if (!$issetPipe) {
                self::setPipeTime($user_id, createPipe($userInfo->id, $user_id, $type));
            }

        }

    }

    public static function setPipeTime($user_id, $pipe)
    {

        $userPipe = UserPipes::where('user_id', $user_id)->where('pipe', $pipe)->first();

        if (!$userPipe) {
            $userPipe = new UserPipes;
            $userPipe['user_id'] = $user_id;
            $userPipe['pipe'] = $pipe;
            $userPipe['last_open'] = date('Y-m-d H:i:s');
        } else {
            $userPipe['last_open'] = date('Y-m-d H:i:s');
        }

        $userPipe->save();
    }

    public static function setUserPipe($user_id, $pipe, $time)
    {
        $userPipe = UserPipes::where('user_id', $user_id)->where('pipe', $pipe)->first();

        if (!$userPipe) {

            echo "enter";

            $userPipe = new UserPipes;
            $userPipe['user_id'] = $user_id;
            $userPipe['pipe'] = $pipe;
            $userPipe['last_open'] = $time;

            $userPipe->save();
        }
    }

    public function status(Request $request)
    {
        $pipe = UserPipes::where('user_id', $request->input('user_id'))->where('pipe', $request->input('pipe'))->first();
        $pipe->status = $request->input('status');
        return (string)$pipe->save();
    }

    public function delMessage(Request $request)
    {
        $message = ChatMessages::find($request->input('id'));
        return (string)$message->delete();
    }

    public function editMessage(Request $request)
    {
        $message = ChatMessages::find($request->input('id'));
        $message->message = $request->input('text');
        $message->save();

        return $message->message;
    }

    public function upload(Request $request)
    {


        $file = $request->file('img');

        $filename = str_random(20) . '.' . $file->getClientOriginalExtension() ?: 'png';
        $path = public_path("/img/chat");
        $img = Image::make($file);
        $img->resize(null, 400, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save($path . "/" . $filename);

        return "/img/chat/" . $filename;
    }
}