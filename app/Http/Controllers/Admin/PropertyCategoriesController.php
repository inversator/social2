<?php

namespace App\Http\Controllers\Admin;

use App\PropertyCategory;
use App\PropertyCategoryTranslation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
use App\Helpers\TranslationHelper;

class PropertyCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = PropertyCategory::all();
        return view('admin.property-categories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.property-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());

        if ($request->method() === 'GET') {
            return view('admin.property.create');
        }

        $category = new PropertyCategory();
        $category->active = 1;
        $category->save();
        foreach ($request->get('name') as $languageId => $value) {
            if ($lang = Language::find($languageId)) {
                $translation = new PropertyCategoryTranslation();
                $translation->translation = $value;
                $translation->language()->associate($lang);
                $category->translations()->save($translation);
            }
        }

        TranslationHelper::clearCache();
        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = PropertyCategory::findOrFail($id);

        $views = [
            'paragraph' => 'Параграф',
            'list' => 'Список',
            'table' => 'Таблица'
        ];

        return view('admin.property-categories.show', compact('category', 'views'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());

        $category= PropertyCategory::findOrFail($id);

        foreach ($request->get('name') as $languageId => $value) {
            if ($lang = Language::find($languageId)) {
                $translation = $category
                    ->translations
                    ->where('language_id', $lang->id)->first();
                if ($translation) {
                    $translation->translation = $value;
                    $translation->save();
                } else {
                    $translation = new PropertyCategoryTranslation;
                    $translation->translation = $value;
                    $translation->language()->associate($lang);
                    $category->translations()->save($translation);
                }
            }
        }

        $category->view = $request->get('view') ;
        $category->save();

        TranslationHelper::clearCache();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
