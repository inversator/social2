<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Property;
use App\PropertyCategory;
use App\PropertyValue;
use App\PropertyTranslation;
use App\PropertyValueTranslation;
use App\Language;
use App\Http\Requests\Admin\PropertyRequest;
use App\Http\Requests\Admin\PropertyValueRequest;
use App\Http\Requests\Admin\PropertyValueTranslationRequest;
use App\Helpers\TranslationHelper;

class PropertiesController extends Controller
{
    public function index()
    {
        $properties = Property::all();
        return view('admin.property.index', compact('properties'));
    }

    public function show($id)
    {

        $property = Property::findOrFail($id);
        $categories = [];

        $rLang = Language::where('code', 'ru')->first();

        foreach(PropertyCategory::all() as $category){
            $categories[$category->id] = $category->getTranslatedProperty($rLang);
        }

        $fieldTypes = [
            'input' => 'input',
            'checkbox' => 'checkbox',
            'select' => 'список',
            'textarea' => 'Текст',
            'radio' => 'radio',
            'mselect' => 'Множественный выбор'

        ];

        return view('admin.property.show', compact('property', 'categories', 'fieldTypes'));
    }

    public function update(PropertyRequest $request, $id)
    {
//        dd($request->all());
        $property = Property::findOrFail($id);

        foreach ($request->get('name') as $languageId => $value) {
            if ($lang = Language::find($languageId)) {
                $translation = $property
                    ->translations
                    ->where('language_id', $lang->id)->first();
                if ($translation) {
                    $translation->translation = $value;
                    $translation->save();
                } else {
                    $translation = new PropertyTranslation;
                    $translation->translation = $value;
                    $translation->language()->associate($lang);
                    $property->translations()->save($translation);
                }
            }
        }

        $property->category_id = $request->get('category') != NULL ? $request->get('category') : 0;
        $property->field_type = $request->get('field_type') != NULL ? $request->get('field_type') : 'input';
        $property->save();

        TranslationHelper::clearCache(); 
        return redirect()->back();
    }

    public function create(PropertyRequest $request)
    {
        if ($request->method() === 'GET') {

            $categories = [];

            $rLang = Language::where('code', 'ru')->first();

            foreach(PropertyCategory::all() as $category){
                $categories[$category->id] = $category->getTranslatedProperty($rLang);
            }

            $fieldTypes = [
                'input' => 'input',
                'checkbox' => 'checkbox',
                'select' => 'список',
                'textarea' => 'Текст',
                'radio' => 'radio',
                'mselect' => 'Множественный выбор'
            ];
            return view('admin.property.create', compact('categories', 'fieldTypes'));
        }

        $property = new Property;
        $property->active = true;
        $property->category_id = $request->get('category') != NULL ? $request->get('category') : 0;
        $property->field_type = $request->get('field_type') != NULL ? $request->get('field_type') : 'input';
        $property->save();
        foreach ($request->get('name') as $languageId => $value) {
            if ($lang = Language::find($languageId)) {
                $translation = new PropertyTranslation;
                $translation->translation = $value;
                $translation->language()->associate($lang);
                $property->translations()->save($translation);
            }
        }

        TranslationHelper::clearCache(); 
        return redirect()->route('admin.property.show', ['id' => $property->id]);
    }

    public function updateValue(PropertyValueRequest $request, $id)
    {
        $propertyValue = PropertyValue::findOrFail($id);

        foreach ($request->get('value') as $languageId => $value) {
            if ($lang = Language::find($languageId)) {
                $translation = $propertyValue->translations->where('language_id', $lang->id)->first();
                if ($translation) {
                    $translation->translation = $value;
                    $translation->save();
                } else {
                    $translation = new PropertyValueTranslation;
                    $translation->translation = $value;
                    $translation->language()->associate($lang);
                    $propValue->translations()->save($translation);
                }
            }
        }

        TranslationHelper::clearCache(); 
        return redirect()->back();
    }

    public function createValue(PropertyValueRequest $request, $propertyId)
    {
        $property = Property::findOrFail($propertyId);

        if ($request->method() === 'GET') {
            return view('admin.property.create-value', compact('property'));
        }

        $propertyValue = new PropertyValue;
        $propertyValue->property()->associate($property);
        $propertyValue->save();

        foreach ($request->get('value') as $languageId => $value) {
            if ($lang = Language::find($languageId)) {
                $translation = new PropertyValueTranslation;
                $translation->translation = $value;
                $translation->language()->associate($lang);
                $propertyValue->translations()->save($translation);
            }
        }

        TranslationHelper::clearCache(); 
        return redirect()->route('admin.property.show', ['id' => $property->id]);
    }

    public function showValueTranslation($id)
    {
        $propertyValueTranslation = PropertyValueTranslation::findOrFail($id);
        return view('admin.property.show-value-translation', compact('propertyValueTranslation'));
    }

    public function updateValueTranslation(PropertyValueTranslationRequest $request, $id)
    {
        $propertyValueTranslation = PropertyValueTranslation::findOrFail($id);
        $propertyValueTranslation->fill($request->all());
        $propertyValueTranslation->save();
        TranslationHelper::clearCache(); 
        return redirect()->route('admin.property.show', ['id' => $propertyValueTranslation->propertyValue->property->id]);
    }

    public function createValueTranslation(PropertyValueTranslationRequest $request, $id, $languageId)
    {
        $propertyValue = PropertyValue::findOrFail($id);
        $language = Language::findOrFail($languageId);

        if ($request->method() === 'GET') {
            return view('admin.property.create-value-translation', compact('propertyValue', 'language'));
        }

        $propertyValueTranslation = PropertyValueTranslation::create([
            'translation' => $request->get('translation'),
            'property_value_id' => $propertyValue->id,
            'language_id' => $language->id,
        ]);
        TranslationHelper::clearCache(); 
        return redirect()->route('admin.property.show', ['id' => $propertyValue->property->id]);
    }
}
