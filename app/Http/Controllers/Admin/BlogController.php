<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Facades\BlogFacade as Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Blog::getPosts();
        return view('admin.blog.index', [
            'posts' => json_encode($posts)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Blog::getCategories();
        return view('admin.blog.create', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Blog::create($request->all());
        return redirect('admin/blog')
                ->with('message', 'Пост "'.$request->get('title').'"успешно создан');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if(!is_numeric($id)){
            $action = camel_case($id);
            if(method_exists($this, $action)){
                return $this->$action($request);
            }
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Blog::find($id);
        $categories = Blog::getCategories();
        return view('admin.blog.create', [
            'categories' => $categories,
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Blog::updatePost($request->all(), $id);
        return redirect('admin/blog')
                ->with('message', 'Пост "'.$request->get('title').'" успешно обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function categories(){

        $categories = Blog::getCategories();
        return view('admin.blog.category_list', [
            'categories' => json_encode($categories)
        ]);

    }


    public function categoriesCreate(){

        return view('admin.blog.category_form', [

        ]);

    }


    public function saveCategory(Request $request){

        Blog::saveCategory($request->all());
        return redirect('admin/blog/categories')
                ->with('message', 'Категория "'.$request->get('title').'"успешно создана');

    }

    public function editCategory($id){

        $category = Blog::getCategory($id);
        return view('admin.blog.category_form', [
            'category' => $category
        ]);

    }


    public function updateCategory(Request $request, $id){

        Blog::updateCategory($request->all(), $id);
        return redirect('admin/blog/categories')
            ->with('message', 'Категория "'.$request->get('title').'"успешно обновлена');

    }

    public function deleteCategory(Request $request, $id){

        Blog::deleteCategory($id);
        return redirect('admin/blog/categories')
            ->with('message', 'Категория "'.$request->get('title').'"успешно удалена');

    }


    public function delete($request){

        Blog::deletePost($request->get('id'));

        return redirect('admin/blog')
            ->with('message', 'Пост успешно удален');
    }

}
