<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use App\User;
use App\Country;
use Carbon\Carbon;
use App\Jobs\SendEmailToUser;
use App\Jobs\SendSmsToUser;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if ($request->method() === 'POST') {
            $rules = [
                'email' => 'required|email',
                'password' => 'required'
            ];
            $data = $request->all();

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->route('auth.login')
                    ->withInput($request->except('password'))
                    ->withErrors($validator);
            }

            $remember = $request->has('remember');
            $email = $request->get('email');
            $password = $request->get('password');

            $user = User::where('email', $email)->first();
            if ($user) {
                if (Hash::check($password, $user->password)) {
                    if (!$user->active) {
                        $error = new MessageBag;
                        $error->add('email', 'Учетная запись неактивна');
                        return redirect()->route('auth.login')
                            ->withInput($request->except('password'))
                            ->withErrors($errors)
                            ;
                    }
                }
            }

            if (Auth::attempt([
                'email' => $email,
                'password' => $password
            ], $remember)) {
                return redirect()->route('index');
            }

            $error = new MessageBag;
            $error->add('email', 'Неправильный email или пароль');
            return redirect()->route('auth.login')
                    ->withInput($request->except('password'))
                    ->withErrors($error);
        }

        return view('auth.login');
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }
        return redirect()->route('index');
    }

    public function register(Request $request)
    {
//        dd($request->all());

        if ($request->method() === 'POST') {
            $birthdayLimit = Carbon::now()->addYears(-16)->format('Y-m-d');
            $rules = [
                'name' => 'required|string|max:256',
                'email' => 'required|email|max:256|unique:users,email',
                'password' => 'required|string|min:6',
                'country_id' => 'required|exists:countries,id',
                'city_id' => 'required|exists:cities,id',
                'birthday' => 'required|date_format:d/m/Y|before:' . $birthdayLimit,
                'gender' => 'required|in:M,F'
            ];
            $data = $request->all();

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->route('auth.register')
                    ->withInput($request->except('password'))
                    ->withErrors($validator);
            }

            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = $data['password'];
            $user->country()->associate($data['country_id']);
            $user->city()->associate($data['city_id']);
            $user->birthday = Carbon::createFromFormat('d/m/Y', $data['birthday']);
            $user->gender = $data['gender'];
            $user->confirmation_code = Str::random(128);
            $user->save();

            $this->dispatch(new SendEmailToUser($user, 'confirmation', ['code' => $user->confirmation_code]));
            Session::flash('success', 'Проверьте почтовый ящик');
            return redirect()->route('index');
        }

        $countries = Country::with('cities')->get();
        return view('auth.register', compact('countries'));
    }

    public function ulogin(Request $request)
    {
        $token = $request->input('token');
        $host = $request->getHttpHost();
        $url = sprintf('https://ulogin.ru/token.php?token=%s&host=%s', $token, $host);
        $response = file_get_contents($url);
        $response = json_decode($response, true);

        if (isset($response['network']) && isset($response['uid'])) {
            $user = User::where('social_provider', $response['network'])
                ->where('social_id', $response['uid'])
                ->first();

            if (!$user) {
                $user = new User;
                $user->social_provider = $response['network'];
                $user->social_id = $response['uid'];
                $user->name = isset($response['first_name']) ? Str::limit($response['first_name'], 256, '') : '';
                $user->save();
            }

            Auth::loginUsingId($user->id);
            return redirect()->route('index');
        }
    }

    public function confirmEmail(Request $request, $userId, $code)
    {
        $user = User::find($userId);
        if (!$user || !$user->email || $user->confirmation_code != $code) {
            Session::flash('error', 'Некорректный код активации');
            return redirect()->route('index');
        }

        $user->confirmation_code = null;
        $user->save();
        Session::flash('success', 'Email подвержден');
        return redirect()->route('index');
    }

    public function completeRegistration(Request $request)
    {
        if (Auth::user() && Auth::user()->isUserCompleted()) {
            return redirect()->route('index');
        }

        if ($request->method() === 'POST') {
            $birthdayLimit = Carbon::now()->addYears(-16)->format('Y-m-d');
            $rules = [];
            $user = Auth::user();
            if (!$user->name) {
                $rules['name'] = 'required|string|max:256';
            }
            if (!$user->phone) {
                $rules['phone'] = 'required|string|digits:11';
            }
            if (!$user->country_id) {
                $rules['country_id'] = 'required|exists:countries,id';
            }
            if (!$user->city_id) {
                $rules['city_id'] = 'required|exists:cities,id';
            }
            if (!$user->birthday) {
                $rules['birthday'] = 'required|date_format:d/m/Y|before:' . $birthdayLimit;
            }
            if (!$user->gender) {
                $rules['gender'] = 'required|in:M,F';
            }
            $data = $request->all();

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->route('auth.complete')
                    ->withInput($request->all())
                    ->withErrors($validator);
            }

            if (isset($data['name'])) {
                $user->name = $data['name'];
            }
            if (isset($data['country_id'])) {
                $user->country()->associate($data['country_id']);
            }
            if (isset($data['city_id'])) {
                $user->city()->associate($data['city_id']);
            }
            if (isset($data['birthday'])) {
                $user->birthday = Carbon::createFromFormat('d/m/Y', $data['birthday']);
            }
            if (isset($data['gender'])) {
                $user->gender = $data['gender'];
            }
            if (isset($data['phone'])) {
                $user->phone = $data['phone'];
            }
            $user->save();

            return redirect()->route('index');
        }

        $countries = Country::with('cities')->get();
        return view('auth.complete', compact('countries'));
    }
}
