<?php

namespace App;

use App\Mail\NewUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use DB;
use App\Models\UserPhoto;
use Illuminate\Support\Facades\Auth;
use App\Models\UserPipes;


class User extends Authenticatable
{
    use EntrustUserTrait; // add this trait to your user model
    use Notifiable;

    protected $dates = ['birthday'];

    protected $table = 'users';
    protected $fillable = ['name', 'email', 'birthday', 'phone', 'country_id', 'city_id', 'gender', 'height', 'weight', 'group'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function propertyValues()
    {
        return $this->hasMany(UserPropertyValue::class, 'user_id', 'id');
    }

    public function photos(){
        return $this->hasMany(UserPhoto::class, 'user_id', 'id');
    }

    public function isUserCompleted()
    {
        if (!$this->phone) return false;
        if (!$this->name) return false;
        if (!$this->birthday) return false;
        if (!$this->gender) return false;
        if (!$this->country_id) return false;
        if (!$this->city_id) return false;
        return true;
    }

    public function hasPremiumAccount()
    {
        // TODO actually verify user's account
        return true;
    }

    public function getAgeAttribute()
    {
        if ($this->birthday) {
            return $this->birthday->diff(Carbon::now())->format('%y');
        }
    }

    public function isFemale()
    {
        return $this->gender == 'F';
    }


    public function getUsers() {
        return $this->paginate(20);
    }

    public function getUser($id) {
        return $this->findOrFail($id);
    }

    public function updateUser($data, $id) {
        $user = $this->findOrFail($id);
        unset($data['_token']);
        unset($data['_method']);
        if(!empty($data['password'])){
            $data['password'] = bcrypt($data['password']);
        }
        $user->update($data);
    }

    public function deleteUser($id) {
        $user = User::findOrFail($id);
        $user->delete();
    }

    public function saveUser($data) {
        $passw = mt_rand(100000, 999999);
        $data['password'] = bcrypt($passw);

        $user = User::create($data);

        \Mail::to($user)->send(new NewUser($data, $passw));

        return $user;
    }


    public static function getUsersForType($type_name){

        return DB::table('roles')
            ->where('roles.name', $type_name)
            ->leftJoin('role_user', 'roles.id', '=', 'role_user.role_id')
            ->leftJoin('users', 'role_user.user_id', '=', 'users.id')
            ->select('users.*')
            ->orderBy('name', 'ASC')
            ->get();

    }

    public function getValueProperty(Property $property, Language $lang){

        if($property->field_type == 'select' or $property->field_type == 'mselect') {

            $res = db::table('user_property_values as upv')
                ->join('property_values as pv', 'upv.property_value_id', '=', 'pv.id')
                ->join('property_value_translations as t', 'pv.id', '=', 't.property_value_id')
                ->where('upv.user_id', $this->id)
                ->where('t.language_id', $lang->id)
                ->where('upv.property_id', $property->id)
                ->first();

            return $res == null ? 'не указано' : $res->translation;

        } else {

            $res = db::table('user_property_values')->where('user_id', $this->id)
                ->where('property_id', $property->id)
                ->first();

            return $res == null ? 'не указано' : $res->value;
        }
    }

//todo refactor - интегрировать с функцией выше
    public function getValueProperties(Property $property, Language $lang){



            $res = db::table('user_property_values as upv')
                ->join('property_values as pv', 'upv.property_value_id', '=', 'pv.id')
                ->join('property_value_translations as t', 'pv.id', '=', 't.property_value_id')
                ->where('upv.user_id', $this->id)
                ->where('t.language_id', $lang->id)
                ->where('upv.property_id', $property->id)
                ->get();

            return $res == null ? 'не указано' : $res;

    }

    public function getPhotos(){

        return db::table('user_photo')
            ->where('user_id', $this->id)
            ->orderBy('priority', 'asc')
            ->get();
    }

    public function getMainPhoto($size = 'thumb'){

         $photos = $this->getPhotos();


         if($photos->isNotEmpty() and file_exists("user_photos/$this->id/$size/{$photos[0]->name}")) {

             return "user_photos/$this->id/$size/{$photos[0]->name}";
         } else {
             if($this->gender == 'F') {
                 return "img/female.jpg";
             }

             return 'img/male.jpg';
         }
    }

    public function getAge(){

        return Carbon::now()->diffInYears(Carbon::parse($this->birthday));

    }


    //todo сделать перевод
    public function getZodiac(){

        $bd = Carbon::parse($this->birthday);

        $day = $bd->day;

        $month = $bd->month;
//        dd($month);
        $signs = array("Козерог", "Водолей", "Рыбы", "Овен", "Телец", "Близнецы", "Рак", "Лев", "Девы", "Весы", "Скорпион", "Стрелец");
        $signsstart = array(1=>21, 2=>20, 3=>20, 4=>20, 5=>20, 6=>20, 7=>21, 8=>22, 9=>23, 10=>23, 11=>23, 12=>23);
        return $day < $signsstart[$month] ? $signs[$month - 1] : $signs[$month % 12];

    }

    public function changeChatStatus($data){

        $to = $data['to'];

        $pipe = createPipe(Auth::id(), $to, 'private');

        try {

            if (!UserPipes::isExistPipe(Auth::id(), $to)) {
                UserPipes::createPipe(Auth::id(), $to);
            }

            db::table('user_pipes')->where('user_id', $to)->where('pipe', $pipe)->update(['status' => $data['status']]);


        } catch(\Exception $e){
            return false;
        }

        return true;

    }



    public function getChatStatus($uid){

        $pipe = createPipe(Auth::id(), $uid, 'private');

        return db::table('user_pipes')->where('pipe', $pipe)->where('user_id', $uid)->value('status');

    }

    public function isOnline(){

        return NULL !== db::table('sessions')->where('user_id', $this->id)->first();
    }

    public function sendMessage($to, $text){


        $pipe = createPipe(Auth::id(), $to, 'private');

        try {

            if (!UserPipes::isExistPipe(Auth::id(), $to)) {
                UserPipes::createPipe(Auth::id(), $to);
            }

            db::table('chat_messages')->insert([
                'user' => $to,
                'user_type' => 'user',
                'pipe' => $pipe,
                'message' => $text,
                'pipe_type' => 'private']);


        } catch(\Exception $e){
            return false;
        }

        return true;

    }
}
