<?php
/**
 * Created by PhpStorm.
 * User: maa
 * Date: 15.08.2017
 * Time: 10:32
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use App\Http\Controllers\Pusher;
use React\EventLoop\Factory as ReactLoop;
use React\ZMQ\Context as ReactContex;
use React\Socket\Server as ReactServer;

use Ratchet\Wamp\WampServer;

class PushServer extends Command
{
    protected $signature = 'socketpush:serve';

    protected $description = '��� ������';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $loop = ReactLoop::create();
        $pusher = new Pusher();
        $context = new ReactContex($loop);

        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind('tcp://127.0.0.1:5555');
        $pull->on('message', array($pusher, 'broadcast') );

        $webSock = new ReactServer($loop);
        $webSock->listen(8383, '0.0.0.0');
        $webServer = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $pusher
                    )
                )
            ),
            $webSock
        );

        $this->info('Server pusher go');
        $loop->run();

    }
}