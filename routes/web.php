<?php


Route::group(['middleware' => 'check_user_profile'], function() {
    Route::get('/', 'MainController@index')->name('index');

    Route::get('/feedback', 'FeedbackController@index');
    Route::post('/feedback', 'FeedbackController@send');

    Route::group(['middleware' => 'auth'], function() {

        Route::post('profile/edit', 'ProfileController@edit');
        Route::resource('profile', 'ProfileController');
//        Route::get('profile/{id}', 'ProfileController@show')->name('profile');

//        Route::post('profile', 'ProfileController@update');

        Route::get('search', 'SearchController@search')->name('search');
    });
});

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function() {
    Route::get('login', 'AuthController@login')->name('login');
    Route::post('login', 'AuthController@login');

    Route::get('register', 'AuthController@register')->name('register')->middleware('guest');
    Route::post('register', 'AuthController@register');

    Route::get('logout', 'AuthController@logout')
        ->name('logout')->middleware('auth');

    Route::post('ulogin', 'AuthController@ulogin')
        ->name('ulogin');

    Route::get('confirm_email/{user_id}/{code}', 'AuthController@confirmEmail')
        ->name('confirm_email');

    Route::group(['middleware' => 'auth'], function() {
        Route::get('complete', 'AuthController@completeRegistration')
            ->name('complete');
        Route::post('complete', 'AuthController@completeRegistration');
    });

});

Route::get('language/{code}', 'LanguageController@setLanguage')->name('language');

// TODO make it available to admins only

Route::middleware('isAdmin')->group(function(){

    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {

        Route::get('blog/categories/{id}/delete', 'Admin\BlogController@deleteCategory');
        Route::post('blog/categories/{id}/update', 'Admin\BlogController@updateCategory');
        Route::get('blog/categories/{id}/edit', 'Admin\BlogController@editCategory');
        Route::post('blog/categories', 'Admin\BlogController@saveCategory');
        Route::resource('blog', 'Admin\BlogController');
        Route::resource('permissions', 'Admin\PermissionsController');
        Route::resource('roles', 'Admin\RolesController');
        Route::resource('users', 'Admin\UsersController');

        Route::post('menu/item-delete', 'Admin\MenuController@deleteItem');
        Route::post('menu/update-item', 'Admin\MenuController@updateItem');
        Route::post('menu/save-item', 'Admin\MenuController@saveItem');
        Route::resource('menu', 'Admin\MenuController');

        Route::resource('pages', 'Admin\\PagesController');

        Route::get('', 'Admin\\HomeController@index')->name('index');

        Route::resource('categories', 'Admin\PropertyCategoriesController');

        Route::group(['prefix' => 'properties', 'as' => 'property.'], function() {
            Route::get('', 'Admin\\PropertiesController@index')->name('index');
            Route::match(['get', 'post'], 'create', 'Admin\\PropertiesController@create')->name('create');
            Route::get('{id}', 'Admin\\PropertiesController@show')->name('show');
            Route::post('{id}', 'Admin\\PropertiesController@update')->name('update');

            Route::post('value/{id}', 'Admin\\PropertiesController@updateValue')->name('update-value');
            Route::match(['get', 'post'], '{id}/value', 'Admin\\PropertiesController@createValue')->name('create-value');

            Route::get('value-translation/{id}', 'Admin\\PropertiesController@showValueTranslation')->name('show-value-translation');
            Route::post('value-translation/{id}', 'Admin\\PropertiesController@updateValueTranslation')->name('update-value-translation');
            Route::match(['get', 'post'], '{id}/value-translation/{languageId}', 'Admin\\PropertiesController@createValueTranslation')->name('create-value-translation');
        });

    });

});

Route::any('/messages', 'MessagesController@pipes')->name('chat');
Route::any('/messages/delete', 'MessagesController@delMessage')->name('deleteChatMessage');
Route::any('/messages/edit', 'MessagesController@editMessage')->name('editChatMessage');

Route::any('/messages/favorite/', 'MessagesController@status')->name('favMessage');
//Route::any('/messages/blacklist', 'MessagesController@status')->name('blMessage');
Route::any('/messages/upload', 'MessagesController@upload')->name('chatImage');

Route::get('/messages/update/{user}/{pipe}', function($user, $pipe){
    \App\Http\Controllers\MessagesController::setPipeTime($user,$pipe);
});
